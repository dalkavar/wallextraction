set "opencvDir=dlls"
set "myDir=D:/wallextraction"

del /F /Q "%myDir%/wallgui/walls/bin"
robocopy "%myDir%/dlls/" "%myDir%/wallgui/walls/bin" libopencv_core400.dll libopencv_highgui400.dll libopencv_imgproc400.dll libopencv_imgcodecs400.dll
robocopy "%myDir%/cmake-build-debug/" "%myDir%/wallgui/walls/bin" walls.exe rooms.exe