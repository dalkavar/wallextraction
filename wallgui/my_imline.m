classdef my_imline < imline
  methods
	function obj = my_imline(hAxis, x, y)
		obj = obj@imline(hAxis, x, y);
		obj.setContextMenu([]);
		obj@imline
	end

	function buttonUpEventFcn(varargin)
		buttonUpEventFcn@imline(varargin);
	end
  end
end