classdef LineDrag < handle
	properties
		hLine = gobjects(0);
		oldButtonUpCbk = @emptyFcn;
		dragStop_Callback = @emptyFcn;
		Parent = gobjects(0);
	end
	
	methods
		function this = LineDrag(ax, x, y, lineUpdated_Callback)
			this.hLine = line(ax, x, y, 'Marker', 's', 'LineWidth', 2.5, 'MarkerSize', 7, 'LineJoin', 'miter', 'MarkerFaceColor', [0.2 0.4 1], ...
				'ButtonDownFcn', @this.ButtonDown_Cbk);
			this.dragStop_Callback = lineUpdated_Callback;
			this.Parent = ax;
		end
		
		% Callback for clicking the line
		function ButtonDown_Cbk(this, hLine, event)
			fprintf('clicked the line\n');
			% only react if the click was near one of the endpoints
			x0 = hLine.XData(1); x1 = hLine.XData(2);
			y0 = hLine.YData(1); y1 = hLine.YData(2);
			ex = event.IntersectionPoint(1); ey = event.IntersectionPoint(2);
			d0 = sqrt((x0 - ex)^2 + (y0 - ey)^2);
			d1 = sqrt((x1 - ex)^2 + (y1 - ey)^2);

			% which endpoint was clicked? (if any)
			if (d0 < 0.01), endpoint = 0;
			elseif (d1 < 0.01), endpoint = 1;
			else, endpoint = -1;
			end
			
			% if an endpoint was clicked, start dragging
			if (endpoint >= 0)
				hAx = hLine.Parent;
				hFig = hAx.Parent;
				set(hFig,'WindowButtonMotionFcn', {@this.draggingFcn, hAx, endpoint});
				this.oldButtonUpCbk = hFig.WindowButtonUpFcn;
				set(hFig,'WindowButtonUpFcn', @this.dragStopFcn);
				setptr(hFig, 'hand');
			end
		end
		
		% runs while an endpoint is being dragged
		function draggingFcn(this, hFig, event, hAx, endpoint)
			currPt = get(hAx,'CurrentPoint');
			if (endpoint == 0)
				this.hLine.XData(1) = currPt(1, 1);
				this.hLine.YData(1) = currPt(1, 2);
			else
				this.hLine.XData(2) = currPt(1, 1);
				this.hLine.YData(2) = currPt(1, 2);
			end
		end
		
		% Stop dragging, reset Figure callbacks
		function dragStopFcn(this, hFig, event)
			this.dragStop_Callback(this);
			set(hFig,'WindowButtonMotionFcn', []);
			set(hFig,'WindowButtonUpFcn', this.oldButtonUpCbk);
			setptr(hFig, 'arrow');
		end
		
		% returns the coordinates for the line
		function [x0, y0, x1, y1] = getLineCoords(this)
			x0 = this.hLine.XData(1);
			y0 = this.hLine.YData(1);
			x1 = this.hLine.XData(2);
			y1 = this.hLine.YData(2);
		end
		
		% destructor
		function delete(obj)
			delete(obj.hLine);
		end
		
		% Helper empty function, runs if there's no mouse buttondown/up callback configured by the user
		function emptyFcn(~, ~)
		end
	end
end
