classdef LinePatch < handle
	properties
		hPatch = gobjects(0);   % patch polygon
		hDrag = gobjects(0);    % LineDrag for editing
		Parent = gobjects(0);
		thickness = 0.0;        % transversal thickness
		sliceThickness = 0.0;
		normal = [0.0, 0.0];    % normal direction vector
		minParallelDist = 0.0;  % min parallel distance detected
		p0x, p0y, p1x, p1y;     % line endpoint coordinates
		selected = false;       % if this linePatch is selected
		cfg;				    % reference to Config object
	end
	
	methods
		function I = LinePatch(ax, p0x, p0y, p1x, p1y, thickness, sliceThickness, normal, minParallelDist, cfg)
			% store properties
			I.thickness = thickness;
			I.p0x = p0x; I.p0y = p0y;
			I.p1x = p1x; I.p1y = p1y;
			I.Parent = ax;
			I.cfg = cfg;
			I.sliceThickness = sliceThickness;
			I.normal = normal;
			I.minParallelDist = minParallelDist;
			
			[X, Y] = I.patchCoordsForLine();
			I.hPatch = patch(X, Y, cfg.polyColorNormal, 'Tag', 'line', 'EdgeColor', 'none', 'ButtonDownFcn', @I.patch_ButtonDownCallback);
			I.hPatch.UserData.Creator = I;
		end
		
		% returns the patch coordinates for the current line endpoint coordinates
		function [X, Y] = patchCoordsForLine(I)
			r = I.thickness / 2.0;
			w = abs(I.p0x - I.p1x);
			h = abs(I.p0y - I.p1y);

			if (I.p0x == I.p1x)
				% vertical
				X = [I.p0x-r, I.p0x-r, I.p0x+r, I.p0x+r];
				Y = [I.p0y, I.p1y, I.p1y, I.p0y];
			elseif (I.p0y == I.p1y)
				% horizontal
				X = [I.p0x, I.p0x, I.p1x, I.p1x];
				Y = [I.p0y+r, I.p0y-r, I.p0y-r, I.p0y+r];
			else
				% diagonal
				if (w > h)
					X = [I.p0x, I.p0x, I.p1x, I.p1x];
					Y = [I.p0y+r, I.p0y-r, I.p1y-r, I.p1y+r];
				else
					X = [I.p0x+r, I.p0x-r, I.p1x-r, I.p1x+r];
					Y = [I.p0y, I.p0y, I.p1y, I.p1y];
				end
			end
		end
		
		% on left click, select and highlight line
		function patch_ButtonDownCallback(I, hPatch, event)
			if (event.Button == 1)
				if (I.selected)
				else
					I.select();
				end
			end
		end
	
		% highlights patch and shows LineDrag
		function select(I)
			% deselect any other LinePatch
			if ~isempty(I.cfg.hSelectedPoly)
				I.cfg.hSelectedPoly.deselect();
			end
			
			I.cfg.hSelectedPoly = I;
			I.hPatch.FaceColor = I.cfg.polyColorSelected;
			I.hPatch.FaceAlpha = 0.5;

			% Re-create the draggable line object
			lx = [I.p0x, I.p1x];
			ly = [I.p0y, I.p1y];
			I.hDrag = LineDrag(I.Parent, lx, ly, @I.lineUpdated_Callback);
		end
		
		% deselect this element, remove highlights
		function deselect(I)
			I.selected = false;
			I.hPatch.FaceColor = I.cfg.polyColorNormal;
			I.hPatch.FaceAlpha = 1.0;
			delete(I.hDrag);
		end
		
		% After the line endpoints have been modified
		function lineUpdated_Callback(I, hDrag)
			[I.p0x, I.p0y, I.p1x, I.p1y] = I.hDrag.getLineCoords();
			[X, Y] = I.patchCoordsForLine();
			I.hPatch.XData = X;
			I.hPatch.YData = Y;
		end
		
		% Returns a serialized version of this LinePatch for storage
		function line = serialize(I)
			of = 1.0; % Matlab offset
			arg = [I.p0x - of, I.p0y - of, I.p1x - of, I.p1y - of, I.thickness, I.sliceThickness, I.normal(1), I.normal(2), I.minParallelDist];
			line = sprintf('%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f', arg(1), arg(2), arg(3), arg(4), arg(5), arg(6), arg(7), arg(8), arg(9));
		end
		
		%destructor
		function delete(I)
			delete(I.hDrag);
			delete(I.hPatch);
		end
	end
		
end

