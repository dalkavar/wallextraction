classdef Config < handle
    properties
        loaded = false;
		imgPath = '';
		imgFromArgument = false; % true if we got the image path from a Matlab argument
		imgWidth = 0;
		imgHeight = 0;
		hSelectedPoly = gobjects(0);  % selected polygon representing the sliceGroup
		
		countDeleted = 0;
		countDragged = 0;
		
		polyColorNormal = [1.0 0.0 0.0]; % Color of unselected slice group polygon
		polyColorSelected = [0.4 0.4 1.0]; % Color of selected slice group polygon
		waitBar = []; % Waitbar object
    end
end