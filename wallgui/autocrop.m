% autocrops binary [0,1] 1 ch image with black background 0 and white foreground 1
% leaves padding pixels in all directions
function [out, xMin, yMin, xMax, yMax] = autocrop(img, padding)
    [rows, cols] = size(img);
    xMin = 1; yMin = 1;
    xMax = cols; yMin = rows;
    
    % get top
    for y = 1 : rows
        if (nnz(img(y,:)) > 0)
            yMin = y;
            break;
        end
    end
    
    % get bottom
    for y = rows : -1 : 1
        if (nnz(img(y,:)) > 0)
            yMax = y;
            break;
        end
    end

    % get left
    for x = 1 : cols
        if (nnz(img(:,x)) > 0)
            xMin = x;
            break;
        end
    end
    
    % get right
    for x = cols : -1 : 1
        if (nnz(img(:,x)) > 0)
            xMax = x;
            break;
        end
    end
    
    % apply padding
    yMin = yMin - padding;
    yMax = yMax + padding;
    xMin = xMin - padding;
    xMax = xMax + padding;
    
    % guard against invalid coords
    if (yMin < 1),		yMin = 1; end
    if (yMax > rows),	yMax = rows; end
    if (xMin < 1),		xMin = 1; end
    if (xMax > cols),	xMax = cols; end
    
    out = img(yMin : yMax, xMin : xMax);
end
