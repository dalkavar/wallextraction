clc;

%imgOriginal = imread('wall-input.png');
%imgBin = uint8(imbinarize(imgOriginal));

% for this example, load and binarize {0,1} a sample image, and add padding to test autocrop
imgOriginal = rgb2gray(imread('../test-images/learning/Triumph10/img/1.png'));
imgBin = uint8(imbinarize(imgOriginal));
imgBin = 1 - imgBin;
imgBin = padarray(imgBin, [20 20], 0, 'both');
%imgBin now is binary {0, 1} image with black for empty pixels
%figure, imshow(imgBin, []);

% ALWAYS: autocrop and write input image to file
[imgBin, x0, y0, ~, ~] = autocrop(imgBin, 1);
imwrite(imgBin, 'walls/assets/wall-input.png');

% If a path is provided WallExtraction GUI will load the input file and run full method
WallExtraction('walls/assets/wall-input.png');

% If no path, WallExtraction GUI will load the last data used
%WallExtraction();