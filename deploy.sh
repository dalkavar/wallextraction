#!/bin/bash
# Deploy script for linux

# delete files in /bin
rm wallgui/walls/bin/walls
rm wallgui/walls/bin/rooms

# copy linux executables
cp "cmake-build-debug/walls" "wallgui/walls/bin/walls"
cp "cmake-build-debug/rooms" "wallgui/walls/bin/rooms"