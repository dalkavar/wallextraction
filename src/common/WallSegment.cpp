#include "WallSegment.h"

WallSegment::WallSegment(Point2f &p0, Point2f &p1, Point2f &normal) {
	this->p0 = p0;
	this->p1 = p1;
	this->normal = normal;
	this->update();
}

/*WallSegment::WallSegment(SliceGroup &g) {
	// properties
	p0 = g.seg.p0;
	p1 = g.seg.p1;
	normal = g.normal;
	thickness = g.seg.thickness;
	sliceThickness  = g.seg.sliceThickness;
	minParallelDist = g.minParallelDist;

	// connections
	//if g.p1ToEndpoint

	update(false);
}*/

void WallSegment::print() {
	Segment::print();
}

// Given a line from a text file, mine data and initialize object
void WallSegment::serialize(char line[]) {
	sprintf(line, "%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n", id, p0.x, p0.y, p1.x, p1.y, thickness, sliceThickness, normal.x, normal.y, minParallelDist);
}

// registers a connection to other WallSegment at my endpoint
void WallSegment::connect(WallSegment *other, char endpoint, bool toEndpoint) {
	/*if (endpoint == '0') {

	}
	(endpoint == '0') ? p0Conn.push_back(other) : p1Conn.push_back(other);*/
}

void WallSegment::serializeConnections(char *line) {

}

