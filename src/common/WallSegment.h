#ifndef PROJECT_WALLSEGMENT_H
#define PROJECT_WALLSEGMENT_H

#include <util/segment.h>

class WallSegment;

// Represents a connection
struct WallConnection {
	WallSegment* other; // points to the other WallSegment
	uchar type; // enum ConnType
};


/**
 * A special class of segment that is considered part of a wall
 */
class WallSegment : public Segment {
public:
	enum class ConnType: uchar { TO_ENDPOINT, TO_LINE_BODY };

	ushort id = 0; // unique id
	uchar type = 0; // 0: normal, 1: virtual
	Point2f normal; // normal pointing outside of the wall

	vector<WallConnection> p0Conn; // Connected to p0
	vector<WallConnection> p1Conn; // Connected to p1

	WallSegment* parallelWall = nullptr; // parallel wall segment
	float minParallelDist = 0.f;

	WallSegment() = default;
	WallSegment(Point2f &p0, Point2f &p1, Point2f &normal);

	void connect(WallSegment* other, char endpoint, bool toEndpoint);

	void print();

	void serialize(char line[]);
	void serializeConnections(char line[]);
};


#endif //PROJECT_WALLSEGMENT_H
