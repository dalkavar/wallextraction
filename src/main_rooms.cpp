#include <stdio.h>
#include <room/WallPainter.h>
#include <extractor/groupmanager.h>
#include <extractor/channeldivider.h>
#include <textseparation/tsregion.h>
#include "util/segment.h"
#include "room/wallprojector.h"
#include "room/ParticleEgn.h"
#include "external/cxxopts.hpp"
#include <util/profiler.h>
#include <room/WallSegmentManager.h>

using namespace std;

int main(int argc, const char *argv[]) {
	printf("\n** Performing rooms segmentation **\n\n");
	//Profiler pf;

	cxxopts::Options options("Room segmentation", "A command line computer vision program for segmenting rooms in floor plan images.");
	options.add_options()
			("out", "Output directory path", cxxopts::value<string>()->default_value("assets/"));
	auto result = options.parse(argc, argv);
	string outPath = result["out"].as<string>();

	// load initial image
	Mat imgInitial = imread(outPath + "wall-input.png", IMREAD_GRAYSCALE);
	if (imgInitial.empty()) {
		printf("FATAL: Initial image 'wall-input.png' not found.\n");
		exit(1);
	}

	// Create output binary image with wall mask
	Mat imgWalls;
	WallPainter wpaint(&imgInitial);
	wpaint.drawLineSegments(mgr.groups, 1);
	wpaint.projectWalls(mgr.groups, 2);
	wpaint.getWallsMask(imgWalls);

	mgr.virtualConnectParallels(imgWalls); //Connect colinear parallels using virtual walls

	imwrite(outPath + "wall-mask.png", imgWalls); // Filled walls binary image

	// load wall segments
	GroupManager mgr(&imgInitial);
	mgr.outPath = outPath;
	mgr.readSegmentsFromFile();

	//for (SliceGroup &g : mgr.groups) g.seg.print();

	// Get the walls mask
	Mat imgWalls;
	WallPainter wpaint(&imgInitial);
	wpaint.drawLineSegments(mgr.groups, 1);

	//disp::showColormap(wpaint.imgLines, "Lines", "wall_lines.png");

	wpaint.projectWalls(mgr.groups, 2);
	wpaint.getWallsMask(imgWalls);
	for (WallSegment &w : mgr.virtuals) {
		line(imgWalls, w.p0, w.p1, Scalar(1), 2); // add virtual walls
	}
	//imwrite(outPath + "wall-mask.png", imgWalls);

	// find the convex hull
	/*vector<Point> points, hull;
	for (SliceGroup &g : mgr.groups) {
		points.push_back(geo::integerPoint(g.seg.p0));
		points.push_back(geo::integerPoint(g.seg.p1));
	}
	cv::convexHull(points, hull);
	vector<vector<Point>> conts;
	conts.push_back(hull);
	drawContours(imgWalls, conts, -1, Scalar(1), 2);
	//for(Point &p : hull) cv::circle(imgWalls, p, 3, Scalar(4), -1);
	//disp::showColormap(imgWalls, "hull");
	 */

	// Get connected components for rooms
	cv::floodFill(imgWalls, Point(0, 0), Scalar(1));
	Point pLast = Point(imgWalls.cols - 1, imgWalls.rows - 1);
	if (imgWalls.at<uchar>(pLast) == 0)	cv::floodFill(imgWalls, pLast, Scalar(1));
	imgWalls = 1 - imgWalls;
	//imwrite("filled.png", imgWalls);

	Mat labels, stats, centroids;
	std::mutex mtx;
	int numcc = cv::connectedComponentsWithStats(imgWalls, labels, stats, centroids, 4, CV_16U);

	// Decide on thresholds for rooms from stats
	float a, avgArea = 0.f;
	vector<float> areas;
	for (ushort i = 0 ; i < numcc ; i++) {
		a = stats.at<int>(i, CC_STAT_AREA);
		if (a > 100) {
			areas.push_back(a);
			avgArea += a;
		}
	}

	if (areas.empty()) {
		printf("ERROR: No meaningful rooms spaces found. Terminating...\n");
		exit(1);
	}

	avgArea /= areas.size();

	// Interquartile range method (IQR)
	float q1, q2, q3, boxLength, minThresh, maxThresh;
	vector<float> quant = stats::Quantile<float>(areas, { 0.25, 0.5, 0.75 });
	q1 = quant[0];
	q2 = quant[1]; // median
	q3 = quant[2];

	boxLength = abs(q3 - q1);
	minThresh = 0.05f * avgArea;
	maxThresh = max<float>(q3 + 1.f * boxLength, 2 * q3);
	//printf("Thresholds: %.2f, %.2f\n", minThresh, maxThresh);

	//disp::showColormap(labels, "Labels init", "labels0.png");
	// Detect the average parallel line distance
	double avgParallelDistance = 0.f, avgThickness = 0.f;
	int n = 0;
	for (SliceGroup &g : mgr.groups) {
		avgThickness += g.seg.thickness;
		if (g.minParallelDist > g.seg.thickness) {
			avgParallelDistance += g.minParallelDist;
			n++;
		}
	}
	avgParallelDistance /= n;
	avgThickness /= mgr.groups.size();
	float minAreaByThickness = pow((avgParallelDistance + 2 * avgThickness), 2);
	printf("Average parallel line distance: %f, avg thickness: %f\n", avgParallelDistance, avgThickness);

	int minParallel = int(avgParallelDistance * 2);
	//printf("Min dimensions: %.2f\n", avgParallelDistance);
	labels.forEach<ushort>([&mtx, &stats, &imgWalls, minAreaByThickness, minParallel, minThresh, maxThresh](ushort &label, const int* pos) -> void {
		if (label > 0) {
			int x = pos[1], y = pos[0];
			int count = stats.at<int>(label, CC_STAT_AREA);

			// filter against a measure of the parallel line distance
			if (count < minAreaByThickness) {
				lock_guard<mutex> guard(mtx);
				label = 0;
				imgWalls.at<uchar>(pos) = 0;
			}

			/*if (count < minThresh
				|| (stats.at<int>(label, CC_STAT_WIDTH) < minParallel)
				|| (stats.at<int>(label, CC_STAT_HEIGHT) < minParallel)) {
				lock_guard<mutex> guard(mtx);
				imgWalls.at<uchar>(pos) = 0;
			}*/
		}
	});

	disp::showColormap(labels, "Labels", "labels.png");

	//disp::showColormap(imgWalls, "WaLLS filtered");

	//Find the contours. Use the contourOutput Mat so the original image doesn't get overwritten
	vector<vector<Point> > contours;
	cv::Mat contourOutput = imgWalls.clone();
	cv::findContours(contourOutput, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

	// Filter the contours
	float minEdge = 2*avgParallelDistance + 2 * avgThickness;
	vector<vector<Point> > validContours;
	Point p0, p1;
	bool valid;

	for (size_t idx = 0; idx < contours.size(); idx++) {
		valid = true;
		if (contours.at(idx).size() > 4) {
			RotatedRect rect = fitEllipseDirect(contours.at(idx));
			if (rect.size.height <= minEdge || rect.size.width <= minEdge) valid = false;
		}
		else {
			for (int i = 1; i < contours.at(idx).size(); i++) {
				p0 = contours.at(idx).at(i - 1);
				p1 = contours.at(idx).at(i);
				if (geo::euclideanDistance(p0, p1) < minEdge) {
					valid = false;
				}
			}
		}

		if (valid) validContours.push_back(contours.at(idx));
	}

	// Debug:: Draw the contours
	//Mat contourImage;
	//cvtColor(imgWalls * 0, contourImage, cv::COLOR_GRAY2BGR);

	//for (size_t idx = 0; idx < validContours.size(); idx++) {
		//cv::drawContours(contourImage, validContours, idx, px::shinyColor(idx), 2);

		// Draw ellipses
		/*if (contours.at(idx).size() > 4) {
			RotatedRect rect = fitEllipseDirect(contours.at(idx));
			if (rect.size.height <= minEdge || rect.size.width <= minEdge) {
				ellipse(contourImage, rect, Vec3b(55,55,255), 2);
			} else {
				ellipse(contourImage, rect, Vec3b(55,255,55), 2);
			}
		}*/
		//for (Point &p : contours.at(idx)) {contourImage.at<Vec3b>(p) = Vec3b(255,255,255);}
	//}
	//for (size_t idx = 0; idx < validContours.size(); idx++) cv::drawContours(contourImage, validContours, idx, Vec3b(0, 123, 200));
	//imwrite(outPath + "contours.png", contourImage);


	// load image and segments, initialize emitters
	//ParticleEgn egn = ParticleEgn(inputImgPath, inputSegPath);
	//egn.run();
	//pf.finish();

	// write contours and wall contours to csv file
	mgr.writeRoomsToFile(validContours);


	//waitKey(0);
	printf("Process finished.\n");
}
