#ifndef TSREGION_H
#define TSREGION_H

#include "opencv2/core.hpp"

using namespace cv;

// Represents a connected components region
class TSRegion {
public:
	int label; // unique label from the connected components Matrix
	int x0, y0, x1, y1; // coords for the bounding box
	ushort count; // number of pixels
	int w = 0, h = 0; // width and height
	ushort area = 0;
	float density = 0;
	bool isCharacter = false; // true if this element looks like a character
	bool isSmallElongated = false; // looks like a character but is very elongated

	TSRegion() = default;
	TSRegion(int label, Mat &stats);
	void update(); // updates properties w,h,area,density,etc after counting pixels
	void print();
};

#endif // TSREGION_H
