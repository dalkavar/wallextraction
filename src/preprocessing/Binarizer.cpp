#include <utility>

#include "Binarizer.h"

/**
 * Constructor, stores parameters
 * @param imgPath Path to image
 * @param binThresh Binarization threshold
 */
Binarizer::Binarizer(string imgPath, uchar binThresh) {
	this->imgPath = std::move(imgPath);
	BLACK_THRESH = binThresh;
}

/**
 * Loads and parses color image at imgPath, binarizes with intensity threshold binThresh
 * @return true if image could be loaded
 */
bool Binarizer::load() {
	// try to load file
	Mat imgCol = imread(imgPath, IMREAD_COLOR);
	if (imgCol.empty()) return false;

	imgCodes = Mat(imgCol.size(), CV_8U, Scalar(0));

	// gather black and non-black pixels
	std::mutex mtx;
	imgCol.forEach<Vec3b>([&mtx, this](Vec3b &color, const int pos[]) -> void {
		float avg = float(color[0] + color[1] + color[2]) / 3.f;

		if (avg < WHITE_AVG_THRESH) {           // Some tolerance on the white level
			uchar code = 1;
			if (avg > BLACK_THRESH) {
				float stdev = 0.3f * (fabs(avg - color[0]) + fabs(avg - color[1]) + fabs(avg - color[2]));
				if (stdev < GRAY_THRESH) {
					code = 2;    // Gray pixel
				} else {
					code = 3;    // Color pixel
				}
			}

			lock_guard<mutex> guard(mtx);
			imgCodes.at<uchar>(pos) = code;
		}
	});

	return true;
}

/**
 * Initializes and fills all black pixels into imgOut as '1's
 * @param imgOut CV_8U Binary {0,1} where 0 is empty
 */
void Binarizer::getBlackPixels(Mat &imgOut) {
	imgOut = Mat(imgCodes.size(), CV_8U, Scalar(0));

	std::mutex mtx;
	imgCodes.forEach<uchar>([&mtx, &imgOut](uchar &code, const int pos[]) -> void {
		if (code == 1) {
			lock_guard<mutex> guard(mtx);
			imgOut.at<uchar>(pos) = 1;
		}
	});
}
