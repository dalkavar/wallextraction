#include <opencv2/imgproc.hpp>
#include "tsregion.h"

TSRegion::TSRegion(int label, Mat &stats) {
	x0 = stats.at<int>(label, CC_STAT_LEFT);
	y0 = stats.at<int>(label, CC_STAT_TOP);
	w = stats.at<int>(label, CC_STAT_WIDTH);
	h = stats.at<int>(label, CC_STAT_HEIGHT);
	count = stats.at<int>(label, CC_STAT_AREA);
	x1 = x0 + w - 1;
	y1 = y0 + h - 1;
	area = w * h;
	density = float(count) / area;
	this->label = label;
}


void TSRegion::print() {
	printf("TSRegion %d { (%d,%d)-(%d,%d) | count %d | w %d | h %d | area %d | %s }\n",
		  label, x0, y0, x1, y1, count, w, h, area, (isCharacter) ? ((isSmallElongated) ? "small-long" : "character") : "nothing");
}

void TSRegion::update() {
	w = x1 - x0 + 1;
	h = y1 - y0 + 1;
	area = w * h;
	density = float(count) / area;
}

