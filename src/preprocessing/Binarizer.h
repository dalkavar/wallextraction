#ifndef PROJECT_BINARIZER_H
#define PROJECT_BINARIZER_H

#include <mutex>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "../util/display.h"
#include "../util/px.h"

using namespace cv;
using namespace std;

class Binarizer {
    string imgPath;
    uchar BLACK_THRESH = 20; // Binarization threshold: Lower gray levels are considered black
    float WHITE_AVG_THRESH = 250.f; // A gray level higher than this is considered white
	float GRAY_THRESH = 10.f; // Stdev between RGB values les than this means it's gray

public:
    Mat imgCodes; // CV_8U coded representation of original image (0: empty, 1: black, 2: gray, 3: color)

    Binarizer(string imgPath, uchar binThresh);
    bool load();
    void getBlackPixels(Mat &imgOut);
};

#endif //PROJECT_BINARIZER_H
