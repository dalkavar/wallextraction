#ifndef TEXTSEPARATION_H
#define TEXTSEPARATION_H

#include <mutex>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "../util/display.h"
#include "../util/px.h"
#include "../util/plothist.h"
#include "tsregion.h"

using namespace cv;
using std::vector;

// Implements Text/Graphics Separation Revisited
class TextSeparation {
	Mat* img; // CV_8U binarized image {0, 1}
	vector<TSRegion> regions; // regions detected

public:
	double avgArea; // average connected components area

	const float factorTa = 5; // factor for building the area threshold value
	const float MAX_RATIO_THRESH = 20; // threshold on the aspect ratio for both w/h and h/w
	float MAX_AREA_THRESH = 0; // max threshold on area for being considered text character
	float MAX_AREA_HIST_THRESH = 0; // threshold on area histogram for being considered text character

	const float T_density = 0.5; // density threshold
	const float T_ratioDash = 2; // elongation ratio between characters and dashed lines

	Mat imgText; // CV_8U with the text only

	TextSeparation();
	void separate(Mat &img);
	void getAreaThreshold();
	bool validateRatio(float w, float h, float threshold);
	void removeTextFromImage(Mat &imgInput, Mat &imgOutput);
};

#endif // TEXTSEPARATION_H
