#include "textseparation.h"

TextSeparation::TextSeparation() {

}

// separates text from img into imgText
void TextSeparation::separate(Mat &img) {
	std::mutex mtx;

	MAX_AREA_THRESH = 0.25f * (img.cols * img.rows); // A fraction of yhe image dimensions

	// get connected components with stats and store them as regions
	Mat labels, stats, centroids;
	int n = cv::connectedComponentsWithStats(img, labels, stats, centroids, 8, CV_16U);
	for (ushort i = 1 ; i < n ; i++) {
		regions.emplace_back(TSRegion(i, stats));
	}
	getAreaThreshold(); // calculate Ta
	if (MAX_AREA_HIST_THRESH == 0) return; // Triggered if getAreaThreshold fails

	// apply thresholds to regions
	int maxDim = (int)round(sqrt(MAX_AREA_THRESH));
	for (TSRegion &r : regions) {
		// looks like a character?
		r.isCharacter = (
					validateRatio(r.w, r.h, MAX_RATIO_THRESH)
					&& (r.area <= MAX_AREA_HIST_THRESH)
					&& (r.w <= maxDim) && (r.h <= maxDim));

		// looks like an elongated character?
		if (r.isCharacter) {
			r.isSmallElongated = (
						(r.density >= T_density)
						&& (!validateRatio(r.w, r.h, T_ratioDash)));
		}
	}

	// store BW text image
	imgText = Mat(labels.rows, labels.cols, CV_8U, Scalar(0));
	labels.forEach<ushort>([&mtx, this](ushort &label, const int* pos) -> void {
		if (label > 0) {
			TSRegion &reg = this->regions.at(ulong(label) - 1);
			if (reg.isCharacter) {
				lock_guard<mutex> guard(mtx);
				this->imgText.at<uchar>(pos) = 1;
			}
		}
	});
}

// perform histogram analysis to obtain an area threshold from the regions
void TextSeparation::getAreaThreshold() {
	int n = (int)regions.size();
	Mat areas = Mat(n, 1, CV_16U);
	ushort a, minArea = (ushort)99999, maxArea = 0;

	// Prepare areas mat and gather avg area
	for (ulong i = 0 ; i < n ; i++) {
		a = regions.at(i).area;
		areas.at<ushort>(i, 0) = a;
		if (maxArea < a) maxArea = a;
		if (minArea > a) minArea = a;
	}

	// Guard against images without text
	Scalar meanMat, stdMat;
	cv::meanStdDev(areas, meanMat, stdMat);
	float avgArea = (float)meanMat[0], devArea = (float)stdMat[0];
	float rg = (maxArea - minArea);
	printf("mean: %.2f stddev: %.2f range: %.2f, %.2f%%\n", avgArea, devArea, rg, devArea*100/rg);
	if ((devArea / (maxArea - minArea)) > 0.2f) {
		printf("TextSeparation:: Canceled. Too much variance on the data.\n");
		MAX_AREA_HIST_THRESH = 0;
		return;
	}

	printf("TextSeparation:: regions: %d, avgArea %.2f, maxArea %d\n", n, avgArea, maxArea);

	// Calculate histogram
	Mat hist;
	int histSize = 64;
	float range[] = { 0, float(maxArea + 1) }; // lower inclusive, upper exclusive
	const float* histRange = { range };
	calcHist(&areas, 1, nullptr, Mat(), hist, 1, &histSize, &histRange, true, false);

	// find most popular area
	float count, maxCount = 0, Amp;
	int posMaxCount = 0;

	for (int i = 0 ; i < histSize ; i++) {
		count = hist.at<float>(i, 0);
		//printf("hist[%d] %d counted %.2f\n", i, i * maxArea / histSize, count);
		if (count > maxCount) {
			maxCount = count;
			posMaxCount = i;
		}
	}
	Amp = (float(maxArea) / histSize) * (posMaxCount + 1);

	// set area threshold value
	MAX_AREA_HIST_THRESH = factorTa * float((avgArea > Amp) ? avgArea : Amp);

	PlotHist ph = PlotHist();
	ph.plot<float>(hist, histSize, range, 1000, 400);
	ph.drawVerticalLine(avgArea, Vec3b(0,0,255));
	ph.drawVerticalLine(Amp, Vec3b(0,200,40));
	ph.drawVerticalLine(MAX_AREA_HIST_THRESH, Vec3b(200,25,200));
	imwrite("histogram-area.png", ph.imgPlot);


	/*int hist_w = 512; int hist_h = 400;
	int bin_w = cvRound( (double) hist_w/histSize );
	Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0,0,0));


	for(int i = 0 ; i < histSize ; i++) {
		line(histImage, Point( bin_w*(i), hist_h ) ,
							   Point( bin_w*(i), hist_h - cvRound(hist.at<float>(i)) ),
							   Scalar(255,0,0), 2, 8, 0  );
		//printf("%d\t%.1f", i, hist.at<float>(i));
	}
	*/
}

// returns true if the aspect ratios from w and h (w/h and h/w) does not pass a threshold
bool TextSeparation::validateRatio(float w, float h, float threshold) {
	if ((h / w) > threshold) return false;
	if ((h / w) < (1 / threshold)) return false;
	return true;
}

/**
 * @brief Modifies image imgOutput to remove text
 * @param imgInput CV_8U {0, 1} input
 * @param imgOutput CV_8U {0, 1} out
 */
void TextSeparation::removeTextFromImage(Mat &imgInput, Mat &imgOutput) {
	separate(imgInput);
	imgOutput = imgInput - imgText;
}
