#ifndef MBR_H
#define MBR_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <vector>

using namespace cv;
using namespace std;

class MBR {
public:
	Point p0;
	Point p1;
	int w, h;

	MBR();
	MBR(Point p);
	MBR(vector<Point> region);
	void countPoint(Point p);
	void print();
	void reset();
};

#endif // MBR_H
