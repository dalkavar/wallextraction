#ifndef PLOTHIST_H
#define PLOTHIST_H

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <mutex>
#include <algorithm>
#include <vector>

using namespace cv;
using std::string;

// plots histogram
class PlotHist {
	// position and size of plot drawing area (between axis)
	int w = 0, h = 0;
	int x0, y0, xf, yf;
	float maxBarCount = 0; // max hist count
	float valueMin = 0, valueMax = 0; // min and max values in histogram range
	int barWidth; // bar pixel width, computed
	int numBars; // num bars
	int maxBarHeight;

	Mat imgText; // for text related operations
	void drawHorizontalText(Mat &img, string text, Point p);
	void drawVerticalText(Mat &img, string text, Point p); // draws vertical text at point p

public:
	Mat imgPlot; // output

	// global image configuration
	int paddingTop = 15; // full image padding
	int paddingLeft = 45;
	int paddingRight = 15;
	int paddingBottom = 45;
	Vec3b imgBackgroundColor = Vec3b(255,255,255);

	// axis configuration
	float axisArrowHeight = 10; // height of the arrow tip
	Vec3b axisColor = Vec3b(128,128,128); // color of the axis lines
	Vec3b axisTextColor = Vec3b(128,128,128); // color of the axis text marks
	int axisFontFace = FONT_HERSHEY_PLAIN;
	double axisFontScale = 0.5;
	int axisTextThickness = 1;

	// bar configuration
	Vec3b barBackgroundColor = Vec3b(20,20,18);
	int barMargin = 1; // pixel margin between consecutive bars



	PlotHist();
	void calcPlot(Mat &img, int numBins, float vMin, float vMax, int plotWidth, int plotHeight);
	template <class T> void plot(Mat &data, int numBins, float range[2], int imgWidth, int imgHeight);
	void drawHorizontalLine(double count, Vec3b color);
	void drawVerticalLine(double value, Vec3b color);
};

#endif // PLOTHIST_H
