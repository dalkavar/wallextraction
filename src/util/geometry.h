#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <cmath>
#include <algorithm>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include "display.h"
#include <string>

using namespace cv;
using namespace std;

namespace geo {
	const float DEGREES_PER_RAD = 180.0f / M_PI;

	float toDegrees(double radians);
	float toRadians(double degrees);
	Point integerPoint(Point2f p);
	int integerLength(double l);

	float getSlopeY(Point2f p0, Point2f p1);
	float getSlopeX(Point2f p0, Point2f p1);
	float getSlopeXY(Point2f p0, Point2f p1, bool horizontal);

	float xForYinRect(double m, double b, double y);
	float yForXinRect(double m, double b, double x);

	// line equations
	bool points2LineEquation(Point2f p0, Point2f p1, double &m, double &b);
	Vec2f pointSlope2LineEquation(const Point2f &p, float m);
	double linearRegression(vector<Point2f> &points, double &m, double &b);

	// angles
	float radiansBetweenPoints(Point2f p0, Point2f p1);
	float radiansBetweenSegments(Point2f &p0, Point2f &p1, Point2f &q0, Point2f &q1);
	float angleDifferenceDeg(float a, float b);
	float angleDifferenceRad(float a, float b);
	float lineDirectionDiference(float a, float b);
	float angleSimilarity(float a, float b);
	float normalizeAngle(float a);
	float hypotenuse(float c1, float c2);
	float angleFromUnitVector(float x, float y);
	
	// direction unitary vectors
	Point2f directionBetweenPoints(Point2f &p0, Point2f &p1);
	Point2f directionBetweenSegments(Point2f &p0, Point2f &p1, Point2f &q0, Point2f &q1);
	Point2f oppositeDirection(Point2f &dir);
	Point2f vectorDirection(Point2f &vect);
	double vectorMagnitude(Point2f &vect);
	bool isZero(Point2f &vect);
	

	// intersections
	float rangeIntersection(float a0, float a1, float b0, float b1);
	char getLineIntersection(Point2f a0, Point2f a1, Point2f b0, Point2f b1, Point2f &cross, Point2f &f);
	Point2f rectCrosspoint(double m1, double b1, double m2, double b2);
	bool linesCrosspoint(Point2f p0, Point2f p1, Point2f q0, Point2f q1, Point2f &cross);
	Point2f projectPoint2Rect(const Point2f &p, float m, float b);
	Point2f avgPoint(const Point2f &p0, const Point2f &p1);

	// 2D distances
	float euclideanDistance(const Point2f &p0, const Point2f &p1);
	float distPointToRect(const Point2f &p, const Point2f &endpointA, const Point2f &endpointB);
	float distPointToLineProjection(Point2f &p, const Point2f &endpointA, const Point2f &endpointB);
	float distBetweenRects(double m, double b1, double m2);
	float distBetweenRects(float m1, float b1, const Point2f &p1, float m2, float b2);
	float distanceBetweenParallelRects(Point2f a0, Point2f a1, Point2f b0, Point2f b1, float &distance, Point2f &direction);

	// Areas
	float triangleArea(float a, float b, float c);
	float triangleHeightFromEdges(float base, float s1, float s2);
}
#endif // GEOMETRY_H
