#ifndef PROFILER_H
#define PROFILER_H

#include <chrono>
#include <string>

using std::chrono::milliseconds;
using std::string;

// Abstracts a time profiler with checkpoints and debug-friendlyness
class Profiler {
private:
	milliseconds creation;
	milliseconds lastCheckpoint;
	int checkCount = 0; // checkpoints so far

	milliseconds now();
	void print(milliseconds d, int type);

public:
	milliseconds lastDiff;
	Profiler();
	void checkpoint(bool printMsg);
	void checkpoint();
	void finish(bool printMsg);
	void finish();
};

#endif // PROFILER_H
