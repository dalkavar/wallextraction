#include "px.h"

namespace px {
	// array of non-contiguous shiny colors for debugging
	const int _numShinyColors = 20;
	Vec3b _shinyColors[_numShinyColors] = {
		Vec3b(75, 25, 230),
		Vec3b(75, 180, 60),
		Vec3b(25, 225, 255),
		Vec3b(200, 130, 0),
		Vec3b(48, 130, 245),
		Vec3b(180, 30, 145),
		Vec3b(240, 240, 70),
		Vec3b(230, 50, 240),
		Vec3b(60, 245, 210),
		Vec3b(190, 190, 250),
		Vec3b(128, 128, 0),
		Vec3b(255, 190, 230),
		Vec3b(40, 110, 170),
		Vec3b(200, 250, 255),
		Vec3b(0, 0, 128),
		Vec3b(195, 255, 170),
		Vec3b(0, 128, 128),
		Vec3b(180, 215, 255),
		Vec3b(128, 0, 0),
		Vec3b(50, 50, 255)
	};


	// Reliably converts a Point2f to a Point.
	// Returns the coordinates of the pixel where the sub-pixel point is contained
	// Only works with positive coordinates
	Point integerPoint(Point2f p) {
		return Point(floor(p.x), floor(p.y));
	}

	// True if coordinates represent a valid point inside the Mat
	bool pointInsideMat(Point2f p, Mat& img) {
		return (p.x > 0 && p.y > 0 && p.x < img.cols && p.y < img.rows);
	}

	// Colors a pixel in a CV_U8C3
	void setColor3(Mat& img, Point p, Vec3b color) {
		img.at<Vec3b>(p) = color;
	}
	void setColor3(Mat& img, Point2f p, Vec3b color) {
		img.at<Vec3b>(integerPoint(p)) = color;
	}

	// Set a pixel white in a CV_8U
	void setWhite(Mat &img, Point p) {
		img.at<uchar>(p) = 255;
	}
	void setWhite(Mat &img, int x, int y) {
		img.at<uchar>(y, x) = 255;
	}

	// Set a pixel black in a CV_8U
	void setBlack(Mat& img, Point p) {
		img.at<uchar>(p) = 0;
	}
	void setBlack(Mat& img, Point2f p) {
		img.at<uchar>(integerPoint(p)) = 0;
	}
	void setBlack(Mat& img, int x, int y) {
		img.at<uchar>(y, x) = 0;
	}

	// true if pixel is black (0,0,0) in a CV_8UC3
	bool isBlack3(Mat& img, int x, int y) {
		return (img.at<Vec3b>(y, x) == Vec3b(0,0,0));
	}
	// true if pixel is black (0) in a CV_8U
	bool isBlack(Mat& img, int x, int y) {
		return (img.at<uchar>(y, x) == 0);
	}
	bool isBlack(Mat& img, Point p) {
		return (img.at<uchar>(p) == 0);
	}
	bool isBlack(Mat& img, Point2f p) {
		return (img.at<uchar>(integerPoint(p)) == 0);
	}
	// will return depending on threshold
	bool looksBlack(Vec3b color, uchar threshold) {
		float avg = (color[0] + color[1] + color[2]) / 3;
		return (avg < threshold);
	}

	// True if color is not any gray (white and black included) in a CV_8U3C
	bool isColor3(Vec3b& color) {
		return !(color[0] == color[1] && color[1] == color[2]);
	}
	// True if color is white
	bool isWhite3(Vec3b& color) {
		return (color[0] == 255 && color[1] == 255 && color[2] == 255);
	}
	// True if color is any gray in a CV_8U3C
	// The 3 channels must have equal value and be in interval (diff, 255 - diff)
	bool isGray3(Vec3b& color, uchar diff) {
		uchar c1 = color[1];
		if (color[0] == c1 && c1 == color[2]) {
			return (c1 > diff && c1 < 255 - diff);
		}
		return false;
	}


	// True if pixel is white (255) in a CV_8U
	bool isWhite(Mat& img, int x, int y) {
		return (img.at<uchar>(y, x) == 255);
	}
	bool isWhite(Mat& img, Point p) {
		return (img.at<uchar>(p) == 255);
	}

	// true if point is not white (255) in a CV_8U
	bool notWhite(Mat& img, Point p) {
		return (img.at<uchar>(p) != 0);
	}
	bool notWhite(Mat& img, Point2f p) {
		return notWhite(img, integerPoint(p));
	}


	// Returns the nearest black pixel coordinates (current position included)
	// Looks for a black pixel in the 8 main directions, up to 4 pixels far
	// Throws the exception "runtime_error" if no such pixel was found
	Point nearestBlack(Mat& img, Point p) {
		unsigned char i, l;
		Point p2;

		if (!isBlack(img, p)) {
			for (l = 1 ; l < 4 ; l++) {
				for (i = 0 ; i < 8 ; i++) {
					try {
						p2 = displacePoint(p, i, l);
					}
					catch (...) { throw runtime_error("Couldn't get nearest black."); }

					if (isBlack(img, p2)) return p2;
				}
			}

			throw runtime_error("No near black found.");
		}
		return p;
	}

	// Returns Point p displaced by "l" pixels, in the direction i [0, 7], clock-wise from the top
	// Doesn't modify p, but returns a modified copy instead
	// Throws runtime_error if the obtained coordinates are not positive
	Point displacePoint(Point p, char direction, char length) {
		switch(direction) {
			case 0: p.y -= length; break;
			case 1: p.y -= length; p.x += length; break;
			case 2: p.x += length; break;
			case 3: p.y += length; p.x += length; break;
			case 4: p.y += length; break;
			case 5: p.y += length; p.x -= length; break;
			case 6: p.x -= length; break;
			case 7: p.y -= length; p.x -= length; break;
		}

		if (p.x < 0 || p.y < 0) throw runtime_error("Impossible to displace Point.");
		return p;
	}

	// Returns a vector of the Points directly connected to Point p, that match "gray" value
	vector<Point> getNeighbors(Mat& img, Point p, uchar gray) {
		char i;
		Point p2;
		vector<Point> neighs;

		for (i = 0 ; i < 8 ; i++) {
			p2 = displacePoint(p, i, 1);
			if (img.at<uchar>(p2) == gray) {
				neighs.push_back(p2);
			}
		}

		return neighs;
	}

	// Returns black (0) neighbors only
	vector<Point> blackNeighbors(Mat& img, Point p) {
		return getNeighbors(img, p, 0);
	}

	// Returns the black neighbors count in a point of a CV_8U Mat
	uchar countBlackNeighbors(Mat &img, int x, int y) {
		int w = img.cols, h = img.rows;
		uchar count = 0;

		if (y > 0) {
			if (img.at<uchar>(y-1, x) == 0) count++;
			if (x > 0) {
				if (img.at<uchar>(y-1, x-1) == 0) count++;
			}
			if (x < w) {
				if (img.at<uchar>(y-1, x+1) == 0) count++;
			}
		}
		if (x < w) {
			if (img.at<uchar>(y, x+1) == 0) count++;
		}
		if (x > 0) {
			if (img.at<uchar>(y, x-1) == 0) count++;
		}

		if (y < h) {
			if (img.at<uchar>(y+1, x) == 0) count++;
			if (x < w) {
				if (img.at<uchar>(y+1, x+1) == 0) count++;
			}
			if (x > 0) {
				if (img.at<uchar>(y+1, x-1) == 0) count++;
			}
		}

		return count;
	}

	// Colors all pixels in Point vector v with "color"
	void colorRegion3(Mat& img, vector<Point>& points, Vec3b color) {
		for (Point &p : points) {
			img.at<Vec3b>(p) = color;
		}
	}

	// Colors a 1px line going from p0 to p1 in black
	// if it overlaps other pixels, only alter the color
	// @TODO: Implement diagonal
	void semicolorLine(Mat& img, Point p0, Point p1) {
		if (p0.x == p1.x) { // vertical
			do {
				alterColor3(img, p0);
				p0.y++;
			} while (p0.y <= p1.y);
		}
		else if (p0.y == p1.y) { // horizontal
			do {
				alterColor3(img, p0);
				p0.x++;
			} while (p0.x <= p1.x);
		}
	}

	// Modifies the color: white becomes yellow, black becomes gray, colors lighten or darken
	void alterColor3(Mat& img, Point p) {
		Vec3b color = img.at<Vec3b>(p);

		if (color[0] == 255 && color[1] == 255 && color[2] == 255) {
			color[0] = 0; color[1] = 200;
		} else if (color[0] == 0 && color[1] == 0 && color[2] == 0) {
			color[2] = 100; color[1] = 70;
		} else {
			color[2] += 100; color[1] += 100;
		}

		img.at<Vec3b>(p) = color;
	}

	// Paints a 1px inner border with color to the CV_8U image
	void addBorder(Mat &img, uchar color) {
		unsigned int x, y, w = img.cols, h = img.rows;

		for (x = 0 ; x < w ; x++) { img.at<uchar>(0, x) = color; }
		for (y = 0 ; y < h ; y++) { img.at<uchar>(y, 0) = color; }
		y = h - 1;
		for (x = 0 ; x < w ; x++) { img.at<uchar>(y, x) = color; }
		x = w - 1;
		for (y = 0 ; y < h ; y++) { img.at<uchar>(y, x) = color; }
	}

	// Returns a color corresponding to the index n which is considered "shiny"
	// Shiny colors are easy to spot in B/W images and different from each other
	Vec3b  shinyColor(ushort n) {
		int max = _numShinyColors - 1; // number of colors in our array

		// normalize
		if (n > max) {
			n = (n % max);
		}

		return _shinyColors[n];
	}

	// Returns the greenlee1985 code for the CV_8U image coords
	// works even in the image border
	uchar gl_getCode(const Mat& img, int x, int y) {
		const uchar *row, *rowPrev, *rowNext;
		int yMax = img.rows - 1;
		uchar code = 0;
		row = img.ptr<uchar>(y);

		if (y > 0) {
			rowPrev = img.ptr<uchar>(y - 1);
			code += 128 * (rowPrev[x] != 255);
		}

		if (y < yMax) {
			rowNext = img.ptr<uchar>(y + 1);
			code += 8 * (rowNext[x] != 255);
		}

		if (x > 0) {
			if (y > 0) code += 64 * (rowPrev[x-1] != 255);
			if (y < yMax) code += 16 * (rowNext[x-1] != 255);
			code += 32 * (row[x-1] != 255);
		}
		if (x < img.cols - 1) {
			if (y > 0) code += 1 * (rowPrev[x+1] != 255);
			if (y < yMax) code += 4 * (rowNext[x+1] != 255);
			code += 2 * (row[x+1] != 255);
		}
		//printf("gl_getCode(%d,%d) = %d", x, y, code);

		return code;
	}

	// displace p towards the neighbor described by the greenlee dirCode
	void displaceEndpoint(Point &p, uchar dirCode) {
		switch(dirCode) {
			case 1: p.x++; p.y--; break;
			case 2: p.x++; break;
			case 4: p.x++; p.y++; break;
			case 8: p.y++; break;
			case 16: p.x--; p.y++; break;
			case 32: p.x--; break;
			case 64: p.x--; p.y--; break;
			case 128: p.y--; break;
		}
	}

	// true if greenlee code is an endpoint
	bool gl_isEndpoint(uchar c) {
		return (c==1 || c==2 || c==4 || c==8 || c==16 || c==32 || c==64 || c==128);
	}

	// true if endpoint code c1 is c2, or the direction immediately to the left or right
	bool gl_similarDirection(uchar c1, uchar c2) {
		if (c1 == c2) return true;

		switch(c1) {
			case 1: return (c2 == 2 || c2 == 128);
			case 2: return (c2 == 1 || c2 == 4);
			case 4: return (c2 == 2 || c2 == 8);
			case 8: return (c2 == 4 || c2 == 16);
			case 16: return (c2 == 8 || c2 == 32);
			case 32: return (c2 == 16 || c2 == 64);
			case 64: return (c2 == 32 || c2 == 128);
			case 128: return (c2 == 64 || c2 == 1);
		}
		return false;
	}

	// Receives a GreenLee code for an endpoint; Returns the code for the endpoint in the opposite direction
	uchar gl_oppositeEndpoint(uchar dir) {
		switch (dir) {
			case 1: return 16;
			case 2: return 32;
			case 4: return 64;
			case 8: return 128;
			case 16: return 1;
			case 32: return 2;
			case 64: return 4;
			case 128: return 8;
		}
		return 0;
	}

	// Returns an endpoint GreenLee code for one of 8 directions possible between from and to
	uchar gl_directionCode(Point &from, Point &to) {
		int dir = 0;
		int ux = to.x - from.x;
		int uy = to.y - from.y;

		if (ux > 0) {
			if (uy < 0) dir = 1;
			else if (uy == 0) dir = 2;
			else dir = 4;
		} else if (ux == 0) {
			if (uy < 0) dir = 128;
			else dir = 8;
		} else {
			if (uy < 0) dir = 64;
			else if (uy == 0) dir = 32;
			else dir = 16;
		}

		return dir;
	}

	// Deletes the neighbor (or neighbors) the greenlee code points to in the CV_8U image
	// if code < 0 we delete the center as well
	// code == 0 deletes only the center
	void gl_deleteNeighbors(Mat &img, int x, int y, short code, uchar bgColor) {
		if (code <= 0)		{ img.at<uchar>(y, x) = bgColor;			code = -code; }
		if (code >= 128)	{ img.at<uchar>(y - 1, x) = bgColor;		code -= 128; }
		if (code >= 64)		{ img.at<uchar>(y - 1, x - 1) = bgColor;	code -= 64; }
		if (code >= 32)		{ img.at<uchar>(y, x - 1) = bgColor;		code -= 32; }
		if (code >= 16)		{ img.at<uchar>(y + 1, x - 1) = bgColor;	code -= 16; }
		if (code >= 8)		{ img.at<uchar>(y + 1, x) = bgColor;		code -= 8; }
		if (code >= 4)		{ img.at<uchar>(y + 1, x + 1) = bgColor;	code -= 4; }
		if (code >= 2)		{ img.at<uchar>(y, x + 1) = bgColor;		code -= 2; }
		if (code >= 1)		{ img.at<uchar>(y - 1, x + 1) = bgColor; }
	}

	// Copies non-white pixels from imgForeground to CV_8UC3 imgBackground
	// bgColor is background color in imgForeground
	void overlapImages(Mat &imgBackground, Mat &imgForeground, Scalar bgColor) {
		std::mutex mtx;

		// imgBackground must be CV_8UC3
		if (imgBackground.type() != CV_8UC3) {
			cv::cvtColor(imgBackground, imgBackground, COLOR_GRAY2BGR);
		}

		switch (imgForeground.type()) {
		case CV_8U:
			imgForeground.forEach<uchar>([&imgBackground, &bgColor, &mtx](uchar &px, const int pos[]) -> void {
				if (px != bgColor[0]) {
					lock_guard<mutex> guard(mtx);
					imgBackground.at<Vec3b>(pos[0], pos[1]) = Vec3b(px, px, px);
				}
			});
			break;
		case CV_16U:
			imgForeground.forEach<ushort>([&imgBackground, &bgColor, &mtx](ushort &px, const int pos[]) -> void {
				if (px != bgColor[0]) {
					lock_guard<mutex> guard(mtx);
					imgBackground.at<Vec3b>(pos[0], pos[1]) = Vec3b(px, px, px);
				}
			});
			break;
		case CV_8UC3:
			Vec3b bg = Vec3b(bgColor[0], bgColor[1], bgColor[2]);
			imgForeground.forEach<Vec3b>([&imgBackground, bg, &mtx](Vec3b &px, const int pos[]) -> void {
				if (px != bg) {
					lock_guard<mutex> guard(mtx);
					imgBackground.at<Vec3b>(pos[0], pos[1]) = px;
				}
			});
			break;
		}
	}

	// copies all non-empty pixels from input into shiny colors in the output
	// Input should be CV_8U or CV_16U
	// pixels with value bgColor are ignored
	// useful for coloring cv::connectedComponents()
	void shinyColormapImage(Mat &input, Mat &output, ushort bgColor) {
		std::mutex mtx;

		// make sure output is an initialized color image
		if (output.empty() || output.type() != CV_8UC3) {
			output = Mat(input.rows, input.cols, CV_8UC3, Scalar(0,0,0));
		}

		// guard against unsupported mats
		int type = input.type();
		if (type != CV_8U && type != CV_16U) {
			printf("shinyColormapImage:: ERROR: unsuported image type (must be CV_8U or CV_16U).");
			return;
		}

		if (type == CV_8U) {
			input.forEach<uchar>([&mtx, &output, bgColor](uchar &val, const int* pos) -> void {
				if (val != bgColor) {
					lock_guard<mutex> guard(mtx);
					output.at<Vec3b>(pos) = px::shinyColor(val);
				}
			});
		}
		else if (type == CV_16U) {
			input.forEach<ushort>([&mtx, &output, bgColor](ushort &val, const int* pos) -> void {
				if (val != bgColor) {
					lock_guard<mutex> guard(mtx);
					output.at<Vec3b>(pos) = px::shinyColor(val);
				}
			});
		}
	}

	// Returns in nhood the 8 neighborhood pixel values for CV_8U img, starting at top-left, clock-wise
	void get8Neighbors(Mat &img, int x, int y, uchar nhood[]) {
		const uchar *row, *rowPrev, *rowNext;
		int yMax = img.rows - 1, xMax = img.cols - 1;
		row = img.ptr<uchar>(y);
		if (y > 0) rowPrev = img.ptr<uchar>(y - 1);
		if (y < yMax) rowNext = img.ptr<uchar>(y + 1);

		nhood[0] = (y > 0 && x < xMax) ? rowPrev[x + 1] : 0;
		nhood[1] = (x < xMax) ? row[x + 1] : 0;
		nhood[2] = (y < yMax && x < xMax) ?rowNext[x + 1] : 0;
		nhood[3] = (y < yMax) ? rowNext[x] : 0;
		nhood[4] = (y < yMax && x > 0) ? rowNext[x - 1] : 0;
		nhood[5] = (x > 0) ? row[x - 1] : 0;
		nhood[6] = (y > 0 && x > 0) ? rowPrev[x - 1] : 0;
		nhood[7] = (y > 0) ? rowPrev[x] : 0;
	}
	// for CV_32F image
	void get8Neighbors(Mat &img, int x, int y, float nhood[]) {
		const float *row, *rowPrev, *rowNext;
		int yMax = img.rows - 1, xMax = img.cols - 1;
		row = img.ptr<float>(y);
		if (y > 0) rowPrev = img.ptr<float>(y - 1);
		if (y < yMax) rowNext = img.ptr<float>(y + 1);

		nhood[0] = (y > 0 && x < xMax) ? rowPrev[x + 1] : 0;
		nhood[1] = (x < xMax) ? row[x + 1] : 0;
		nhood[2] = (y < yMax && x < xMax) ?rowNext[x + 1] : 0;
		nhood[3] = (y < yMax) ? rowNext[x] : 0;
		nhood[4] = (y < yMax && x > 0) ? rowNext[x - 1] : 0;
		nhood[5] = (x > 0) ? row[x - 1] : 0;
		nhood[6] = (y > 0 && x > 0) ? rowPrev[x - 1] : 0;
		nhood[7] = (y > 0) ? rowPrev[x] : 0;
	}


	// Counts the number of times the neighborhood pixels go from a value to an empty value and backwards
	// nhood is an array of 8 positions as the one returned by get8Neighbors
	uchar nhood8Jumps(uchar nhood[], uchar bgColor) {
		uchar jumps = 0;
		for (uchar i = 0 ; i < 7 ; i++) {
			if (nhood[i] == bgColor && nhood[i + 1] != bgColor) jumps++;
			else if (nhood[i] != bgColor && nhood[i + 1] == bgColor) jumps++;
		}
		// last pixel
		if (nhood[7] == bgColor && nhood[0] != bgColor) jumps++;
		else if (nhood[7] != bgColor && nhood[0] == bgColor) jumps++;

		return jumps;
	}


	// draws an arrow in img(x,y) pointing at angle
	void drawDirection(Mat &img, int x, int y, float angle) {
		cv::Vec3b color(0,200,0);
		float length = 10;
		Point origin(x, y);
		Point dest(x + length * cos(angle), y - length * sin(angle));
		cv::arrowedLine(img, origin, dest, color, 1.2, cv::LINE_AA, 0, 0.5);
	}

	// Return number of pixels of color in img
	int countPixels(Mat &img, const uchar color) {
		int count = 0;
		std::mutex mtx;
		img.forEach<uchar>([&mtx, &count, color](uchar &val, const int* pos) -> void {
			if (val == color) {
				lock_guard<mutex> guard(mtx);
				count++;
			}
		});
		return count;
	}

	/**
	 * Returns in p0 and p1 the MBR min and max corners after cropping image
	 * Tries to leave a 1px border if possible
	 * @param img CV_8U
	 * @param bgColor color of empty pixels
	 * @param p0 top-left
	 * @param p1 bottom-right
	 * @return true if we managed to crop
	 */
	bool autocrop(Mat &img, uchar bgColor, Point &p0, Point &p1) {
		int x = 0, y = 0, w = img.cols, h = img.rows;
		bool loop = true;
		uchar *row;
		p0 = Point(0, 0);
		p1 = Point(w - 1, h - 1);

		for (y = 0 ; (y < h && loop) ; y++) { // find top
			row = img.ptr(y);
			for (x = 0 ; x < w ; x++) {
				if (row[x] != bgColor) { p0.y = y; loop = false; break; }
			}
		}

		loop = true; // find bottom
		for (y = h - 1 ; (y > 0 && loop) ; y--) {
			row = img.ptr(y);
			for (x = 0 ; x < w ; x++) {
				if (row[x] != bgColor) { p1.y = y; loop = false; break; }
			}
		}

		loop = true; // find left
		for (x = 0 ; (x < w && loop) ; x++) {
			for (y = 0 ; y < h ; y++) {
				if (img.at<uchar>(y, x) != bgColor) { p0.x = x; loop = false; break; }
			}
		}

		loop = true; // find right
		for (x = w - 1 ; (x > 0 && loop) ; x--) {
			for (y = 0 ; y < h ; y++) {
				if (img.at<uchar>(y, x) != bgColor) { p1.x = x; loop = false; break; }
			}
		}

		// Leave 1px border if possible
		if (p0.x > 0) p0.x--;
		if (p0.y > 0) p0.y--;
		if (p1.x < (w - 1)) p1.x++;
		if (p1.y < (y - 1)) p1.y++;

		return (p0.x > 0 || p0.y > 0 || p1.x < (w - 1) || p1.y < (h - 1));
	}

	// True if coordinates are valid inside img
	bool isPixelInsideMat(int x, int y, Mat &img) {
		if (x < 0 || y < 0 || x >= img.cols || y >= img.rows || img.empty()) return false;
		return true;
	}

	// Draws a line using slices, from p0 to p1, with transversal thickness, on img CV_8U
	void drawLine(Mat &img, Point2f p0, Point2f p1, float thickness, uchar color) {
		//printf("Drawing line from (%.2f,%.2f) to (%.2f,%.2f)...", p0.x, p0.y, p1.x, p1.y);
		Point2f pc;
		double m, b;
		int numSlices;
		float w, h, st;
		double dx = 0, dy = 0;

		// organize by x coordinate
		if (p1.x < p0.x) cv::swap<Point2f>(p1, p0);

		w = fabs(p0.x - p1.x);
		h = fabs(p0.y - p1.y);
		bool verticalSlices = ((h / w) < 1.f);
		geo::points2LineEquation(p0, p1, m, b);

		// find slice thickness
		bool perfect = (p0.x == p1.x || p0.y == p1.y);
		if (perfect) {
			st = thickness;
		} else {
			st = float((verticalSlices) ? thickness / fabs(cos(atan(m))) : thickness / fabs(sin(atan(m))));
		}
		float r =  st / 2.f;

		if (verticalSlices) {
			// Vertical slices, horizontal shaped line
			numSlices = geo::integerLength(w);
			pc.x = floor(p0.x) + 0.5f;
			pc.y = p0.y;
			dx = (p0.x < p1.x) ? 1.f : -1.f;
			if (!perfect) {
				dy = (dx == 1) ? m : -m;
			}
		}
		else {
			// Horizontal slices, vertical shaped line
			numSlices = geo::integerLength(h);
			pc.y = floor(p0.y) + 0.5f;
			pc.x = p0.x;
			dy = (p0.y < p1.y) ? 1.f : -1.f;
			if (!perfect) {
				dx = (dy == 1) ? 1/m : -1/m;
			}
		}

		// draw all slices
		while (numSlices > 0) {
			drawSlice(img, pc, r, verticalSlices, color);
			pc.x += dx;
			pc.y += dy;
			numSlices--;
		}
	}

	// draws a slice centered at c
	void drawSlice(Mat &img, Point2f c, float radius, bool verticalSlice, uchar color) {
		int sx0, sy0, sx1, sy1; // slice coords
		float x0 = c.x, y0 = c.y, x1 = c.x, y1 = c.y;

		if (verticalSlice) {
			y0 -= radius;
			y1 += radius;
			sx0 = (int)floor(x0);
			sy0 = ((y0 - int(y0)) > 0.5) ? (int)ceil(y0) : (int)floor(y0);
			sy1 = ((y1 - int(y1)) > 0.5) ? (int)floor(y1) : (int)floor(y1) - 1;

			while(sy0 <= sy1) {
				if (!px::isPixelInsideMat(sx0, sy0, img)) return;
				img.at<uchar>(sy0, sx0) = color;
				sy0++;
			}
		}
		else {
			x0 -= radius;
			x1 += radius;
			sx0 = ((x0 - int(x0)) > 0.5) ? (int)ceil(x0) : (int)floor(x0);
			sy0 = (int)floor(y0);
			sx1 = ((x1 - int(x1)) > 0.5) ? (int)floor(x1) : (int)floor(x1) - 1;

			while(sx0 <= sx1) {
				if (!px::isPixelInsideMat(sx0, sy0, img)) return;
				img.at<uchar>(sy0, sx0) = color;
				sx0++;
			}
		}
	}
}
