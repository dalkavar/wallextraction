#include <cmath>

#include "plothist.h"

// draws horizontal text at p right-aligned horizontally, center aligned vertically on p
void PlotHist::drawHorizontalText(Mat &img, std::string text, Point p) {
	int baseline;
	Size textSize = getTextSize(text.c_str(), axisFontFace, axisFontScale, axisTextThickness, &baseline);
	Point pos = Point(p.x - textSize.width, p.y + textSize.height / 2);
	putText(img, text.c_str(), pos, axisFontFace, axisFontScale, axisTextColor, axisTextThickness, LINE_AA);
}

void PlotHist::drawVerticalText(Mat &img, std::string text, Point p) {
	// draw text on a separate image
	int baseline;
	Size textSize = getTextSize(text.c_str(), axisFontFace, axisFontScale, axisTextThickness, &baseline);
	Point pos = Point(textSize.height, textSize.height + baseline);
	putText(imgText, text.c_str(), pos, axisFontFace, axisFontScale, axisTextColor, axisTextThickness, LINE_AA);
	cv::rotate(imgText, imgText, ROTATE_90_CLOCKWISE);

	// obtain text ROI coordinates
	int w = imgText.cols, h = imgText.rows;
	int x0 = w - pos.y - axisTextThickness;
	int x1 = x0 + textSize.height;
	int y0 = pos.x;
	int y1 = y0 + textSize.width;
	Mat roi = imgText(Rect(x0, y0, x1 - x0, y1 - y0));

	// draw rotated text over image
	std::mutex mtx;
	roi.forEach<Vec3b>([&mtx, &img, &p, this](Vec3b color, const int* pos) -> void {
		if (color[0] != 255 || color[1] != 255 || color[2] != 255) {
			std::lock_guard<std::mutex> guard(mtx);
			img.at<Vec3b>(pos[0] + p.y, pos[1] + p.x) = color;
		}
	});

	roi = imgBackgroundColor;
}

PlotHist::PlotHist() {
	imgText = Mat(64, 64, CV_8UC3, imgBackgroundColor);
}

// Quick function to calculate histogram and plot it into imgPlot
void PlotHist::calcPlot(Mat &img, int numBins, float vMin, float vMax, int plotWidth, int plotHeight) {
	// Calculate histogram
	Mat hist;
	int histSize = numBins;
	float range[] = { vMin, vMax };
	const float* histRange = { range };
	calcHist(&img, 1, 0, Mat(), hist, 1, &histSize, &histRange, true, false);

	for (int i = 0 ; i < numBins ; i++) printf("bin %d has %.1f\n", i, hist.at<float>(i));

	// Plot and store
	plot<float>(hist, numBins, range, plotWidth, plotHeight);
}

template <class T>
void PlotHist::plot(Mat &data, int numBins, float range[2], int plotWidth, int plotHeight) {
	numBars = numBins;
	imgPlot = Mat(plotHeight, plotWidth, CV_8UC3, imgBackgroundColor);
	w = plotWidth - paddingLeft - paddingRight;
	h = plotHeight - paddingTop - paddingBottom;
	x0 = paddingLeft;
	xf = plotWidth - paddingRight;
	y0 = paddingTop;
	yf = plotHeight - paddingBottom;
	valueMin = range[0], valueMax = range[1];
	float barIntervalSize = (valueMax - valueMin) / numBins;

	// normalize data to plot's pixel height
	Mat dataNorm;
	maxBarHeight = h - 20;
	normalize(data, dataNorm, 0, maxBarHeight, NORM_MINMAX, -1, Mat());

	// draw axis lines
	int binCount = data.rows;
	arrowedLine(imgPlot, Point(x0, yf), Point(x0, y0), axisColor, 1, 8, 0, axisArrowHeight / (yf - y0));
	arrowedLine(imgPlot, Point(x0, yf), Point(xf, yf), axisColor, 1, 8, 0, axisArrowHeight / (xf - x0));

	// draw X axis marks
	barWidth = (int)floor(float(w) / binCount);

	int barX0, barY0 = yf, barX1, barY1;
	float markValue;
	char format[8] = "%.0f";
	char markText[16];

	for(int i = 0 ; i <= binCount ; i++) {
		barX0 = x0 + (int)floor(barWidth * i);
		markValue = valueMin + i * barIntervalSize;
		sprintf(markText, format, markValue);
		line(imgPlot, Point(barX0, yf), Point(barX0, yf + 3), axisColor, 1); // add line marker
		drawVerticalText(imgPlot, string(markText), Point(barX0 - 3, yf + 4)); // add value text
	}

	// draw Y axis marks
	std:: vector<ushort> counts;
	for(int i = 0 ; i <= binCount ; i++) { counts.push_back(round(data.at<T>(i))); }
	std::sort(counts.begin(), counts.end());
	int markY, lastMarkY = 0, first = true;

	maxBarCount = counts.at(counts.size() - 1);

	for (ushort count : counts) {
		markY = yf - maxBarHeight * float(count) / maxBarCount;
		//printf("%d %d %d", count, markY, maxBarCount);
		if (first || abs(markY - lastMarkY) >= 16) {
			if (first) { first = false; }
			line(imgPlot, Point(x0 - 5, markY), Point(x0, markY), axisColor, 1);
			sprintf(markText, "%d", count);
			drawHorizontalText(imgPlot, markText, Point(x0 - 5, markY));
			lastMarkY = markY;
		}
	}

	// draw histogram bars
	for(int i = 0 ; i < binCount ; i++) {
		barX0 = x0 + round(barWidth * i) + barMargin;
		barX1 = barX0 + round(barWidth) - 2*barMargin;
		barY1 = yf - round(dataNorm.at<T>(i));
		rectangle(imgPlot, Point(barX0, barY0 - 1), Point(barX1, barY1), barBackgroundColor, cv::FILLED);
		//printf("%d\t%.1f", i, data.at<T>(i));
	}
}

// draws a horizontal line at count level over the current histogram plot
void PlotHist::drawHorizontalLine(double count, Vec3b color) {
	if (!imgPlot.empty() && h > 0) {
		int y = yf - maxBarHeight * count / maxBarCount;
		line(imgPlot, Point(x0 + 1, y), Point(xf - 1, y), color, 1);
	}
}

// draws a vertical line at value level over the current histogram plot
void PlotHist::drawVerticalLine(double value, Vec3b color) {
	if (!imgPlot.empty() && w > 0) {
		int valuePx = round(barWidth * numBars * value / (valueMax - valueMin));
		int x = x0 + valuePx;
		line(imgPlot, Point(x, y0), Point(x, yf - 1), color, 1);
	}
}


// template definitions
template void PlotHist::plot<ushort>(Mat &data, int numBins, float range[2], int imgWidth, int imgHeight);
template void PlotHist::plot<float>(Mat &data, int numBins, float range[2], int imgWidth, int imgHeight);
