#ifndef CLEANING_H
#define CLEANING_H

#include <mutex>
#include <iostream>
#include "px.h"
#include "display.h"
#include "opencv2/imgproc.hpp"

using namespace cv;

namespace clean {
/**
 * removes 4-connected blobs that meet criteria
 * elongation and density are tested together to accurately identify blobs
 * @param origin CV_8U {0,1} binary image
 * @param dest cloned origin with blobs removed
 * @param maxPixels blobs with less pixels are considered blobs
 * @param minElongation, maxElongation define blob thresholds for elongation, (0,0) deactivates this filter
 * @param minDensity, maxDensity define blob thresholds for density, (0,0) deactivates this filter
 */
	template <typename T>
	void cleanBlobs(Mat &origin, Mat &dest, int maxPixels, float minElongation, float maxElongation, float minDensity, float maxDensity) {
		bool checkElongation = (minElongation == 0 && maxElongation == 0);
		bool checkDensity = (minDensity == 0 && maxDensity == 0);
		std::mutex mtx;

		// get connected components of the intensity value
		Mat imgLabels, stats, centroids;
		cv::connectedComponentsWithStats(origin, imgLabels, stats, centroids, 4, CV_16U);
		//disp::showColormap(imgLabels, "Labels");

		// Decide which components to remove
		vector<bool> removeFlags((ulong)stats.rows);
		int numPixels, w, h;
		float elongation, density;
		bool remove;

		for (ulong i = 0 ; i < stats.rows ; i++) {
			numPixels = stats.at<int>(i, CC_STAT_AREA);
			w = stats.at<int>(i, CC_STAT_WIDTH);
			h = stats.at<int>(i, CC_STAT_HEIGHT);
			elongation = (w > h) ? float(w) / h : float(h) / w; // we always get the bigger elongation value
			density = float(numPixels) / (w * h);
			//printf("%d %d %d %.1f %.1f\n", numPixels, w, h, elongation, density);

			// decide if we should remove
			remove = false;
			if (numPixels <= maxPixels) remove = true;
			else if (checkElongation && checkDensity) {
				if (elongation >= minElongation && elongation <= maxElongation && density >= minDensity && density <= maxDensity) {
					remove = true;
				}
			}
			removeFlags.at(i) = remove;
		}

		// clone origin if dest is different
		if (&origin != &dest) dest = origin.clone();

		// remove components that don't meet the thresholds
		imgLabels.forEach<ushort>([&mtx, &removeFlags, &dest](ushort &label, const int* pos) -> void {
			if (removeFlags.at(label)) {
				lock_guard<mutex> guard(mtx);
				dest.at<T>(pos) = 0;
			}
		});
	}
}

#endif // CLEANING_H
