#include "Vector2D.h"

Vector2D::Vector2D() {
	x = 0.f;
	y = 0.f;
}

Vector2D::Vector2D(float x, float y) {
	this->x = x;
	this->y = y;
}


void Vector2D::set(float x, float y) {
	this->x = x;
	this->y = y;
}


/**
 * adds c to this vector's magnitude
 * @param c
 */
Vector2D& Vector2D::addMagnitude(float c) {
	double r = y / x;
	double xc = fabs(c / sqrt(1 + r * r));
	double yc = fabs(r * xc);
	
	// Make sure the sign is equal
	x += (x > 0) ? xc : -xc;
	y += (y > 0) ? yc : -yc;
	return *this;
}

/**
 * substracts c from this vector's magnitude
 * @param c
 */
Vector2D& Vector2D::substractMagnitude(float c) {
	double r = y / x;
	double xc = fabs(c / sqrt(1. + r * r));
	double yc = fabs(r * xc);
	
	// Make sure the sign is equal
	x -= (x > 0) ? xc : -xc;
	y -= (y > 0) ? yc : -yc;
	return *this;
}

/**
 * substracts c from magnitude, without changing direction
 * if after substracting one of the components is <= 0, it remains as 0
 * vector never changes orientation
 * @param c
 */
Vector2D& Vector2D::reduceMagnitude(float c) {
	// make sure c is a positive quantity
	if (c <= 0.f) return *this;
	// if 0 do nothing
	if (x == 0.f && y == 0.f) return *this;
	// vertical
	if (x == 0.f) {
		y = (fabs(y) >= c) ? (y - sign(y) * c) : 0.f;
	}
	// horizontal
	else if (y == 0.f) {
		x = (fabs(x) >= c) ? (x - sign(x) * c) : 0.f;
	}
	// diagonal
	else {
		float r = fabs(y / x);
		float xc = c / sqrt(1.f + r * r);
		float yc = r * xc;

		x = (xc > fabs(x)) ? 0.f : x - sign(x) * xc;
		y = (yc > fabs(y)) ? 0.f : y - sign(y) * yc;
	}

	return *this;
}



/**
 * Vector2D + Vector2D performs vector sum (components sum)
 * @param other
 * @return
 */
Vector2D Vector2D::operator+(const Vector2D &other) {
	return Vector2D(this->x + other.x, this->y + other.y);
}


/**
 * Vector2D - Vector2D performs vector substract (components substract)
 * @param other
 * @return
 */
Vector2D Vector2D::operator-(const Vector2D &other) {
	return Vector2D(this->x - other.x, this->y - other.y);
}

void Vector2D::operator+=(const Vector2D &other) {
	x += other.x;
	y += other.y;
}

void Vector2D::operator-=(const Vector2D &other) {
	x -= other.x;
	y -= other.y;
}

/**
 * Vector2D + scalar adds c to this vector's magnitude; direction remains unchanged
 */
Vector2D Vector2D::operator+(const float &c) {
	Vector2D v(x, y);
	if (c > 0.f) v.addMagnitude(c);
	else if (c < 0.f) v.substractMagnitude(c);
	return v;
}
Vector2D Vector2D::operator-(const float &c) {
	Vector2D v(x, y);
	if (c < 0.f) v.addMagnitude(c);
	else if (c > 0.f) v.substractMagnitude(c);
	return v;
}

/**
 * Multiplies magnitude by c; direction remains unchanged
 */
Vector2D Vector2D::operator*(const float &c) {
	return Vector2D(c * x, c * y);
}


void Vector2D::operator+=(const float &c) {
	if (c > 0.f) addMagnitude(c);
	else substractMagnitude(c);
}

void Vector2D::operator-=(const float &c) {
	if (c < 0.f) addMagnitude(c);
	else substractMagnitude(c);
}

void Vector2D::operator*=(const float &c) {
	x *= c;
	y *= c;
}

/**
 * HELPER: Returns 1 or -1 for the sign of f
 * @param f
 * @return
 */
float Vector2D::sign(const float f) {
	return (f >= 0.f) ? 1.f : -1.f;
}

float Vector2D::magnitude() {
	return sqrt(x*x + y*y);
}

/**
 * Replaces magnitude (direction doesn't change)
 * @param m
 */
void Vector2D::magnitude(const float m) {
	double r = y / x;
	float xc = (float)fabs(m / sqrt(1. + r * r));
	float yc = (float)fabs(r * xc);
	
	x = sign(x) * xc;
	y = sign(y) * yc;
}

/**
 * Stores in pos the floor() of y and x
 * Useful for turning a vector into a pixel position [row, col]
 * @param pos int array of size 2
 */
Point Vector2D::integerCoords() {
	return Point((int)floor(x), (int)floor(y));
}

void Vector2D::operator=(const Point2f &p) {
	x = p.x;
	y = p.y;
}

Vector2D::operator Point2f() {
	return Point2f(x, y);
}


Vector2D::Vector2D(Point2f p) {
	x = p.x;
	y = p.y;
}

/**
 * Turns this Vector2D into a magnitude 1 unit vector (direction unchanged
 */
Vector2D& Vector2D::normalize() {
	float m = magnitude();
	if (m != 0.f) {
		x /= m;
		y /= m;
	}
	return *this;
}

float Vector2D::dotProduct(Vector2D &v) {
	return (x * v.x + y * v.y);
}

Vector2D Vector2D::direction() {
	return Vector2D(x, y).normalize();
}

// Global * operator
Vector2D operator * (float c, Vector2D &v) {
	return v * c;
}

void Vector2D::invert() {
	x = -x;
	y = -y;
}

bool Vector2D::isZero() {
	return (x == 0.f && y == 0.f);
}
