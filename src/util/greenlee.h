#ifndef GREENLEE_H
#define GREENLEE_H

#define GL_NE 0
#define GL_E  1
#define GL_SE 2
#define GL_S  3
#define GL_SW 4
#define GL_W  5
#define GL_NW 6
#define GL_N  7

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
#include <mutex>

using namespace std;
using namespace cv;


namespace green {
	uchar encode(const Mat &img, int x, int y, uchar white);
	uchar encode(const Mat &img, int x, int y);
	void encodeMatrixForeground(const Mat &source, Mat &dest, uchar bgColor, uchar mode);
	void encodeMatrixForeground(const Mat &source, Mat &dest, uchar bgColor);
	void encodeMatrixNeighborCount(const Mat &source, Mat &dest, uchar bgColor);
	void encodeMatrix(const Mat &source, Mat &dest);
	bool getNeighbor(const Mat &encoded, int x, int y, uchar direction);
	bool getNeighbor(uchar code, uchar direction);
	uchar neighborCount(uchar code);
	bool isEndpoint(uchar code);
	void displaceEndpoint(Point &pos, uchar code);
	uchar oppositeEndpoint(uchar dir);
	bool opposites(uchar code1, uchar code2, uchar tolerance);
	bool opposites(uchar code1, uchar code2);
	bool is1pxLine(uchar code);
}

#endif // GREENLEE_H
