#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>
#include <stdarg.h>
#include <string>

using namespace std;

namespace debug {
	extern string logPath;
	extern FILE *fp;

	// Debug helper functions
	void log(const char *fmt, ...);
	void log(char colorCode, const char *fmt, ...);
}

#endif // DEBUG_H
