#include "profiler.h"

// Initialize profiler creation time when we create it
Profiler::Profiler() {
	creation = now();
	lastCheckpoint = creation;
	printf("\nProfiler:: Starting.\n");
}

// stores lastCheckpoint and optionally prints the time since last checkpoint
void Profiler::checkpoint(bool printMsg) {
	milliseconds n = now();
	lastDiff = n - lastCheckpoint;
	lastCheckpoint = n;
	if (printMsg) {
		print(lastDiff, 1);
	}
	checkCount++;
}
void Profiler::checkpoint() { checkpoint(true); }

void Profiler::finish(bool printMsg) {
	checkpoint(printMsg);
	milliseconds n = now();
	milliseconds difms = n - creation;
	lastCheckpoint = n;
	if (printMsg) {
		print(difms, 2);
	}
}
void Profiler::finish() { finish(true); }

// returns number of milliseconds since epoch
std::chrono::milliseconds Profiler::now() {
	return std::chrono::duration_cast<milliseconds>(std::chrono::system_clock::now().time_since_epoch());
}

// prints milliseconds
// type = 0 (start), 1(checkpoint), 2(end)
void Profiler::print(std::chrono::milliseconds d, int type) {
	string prefix;
	switch(type) {
		case 1: prefix = string("Profiler:: Time since last checkpoint: "); break;
		case 2: prefix = string("\nProfiler:: Finished with total time: "); break;
	}

	char msg[100];
	double dc = d.count();
	//if (dc < 10000) {
		sprintf(msg, "[%d] %s%lu ms.\n", checkCount, prefix.c_str(), d.count());
	/*} else if (dc >= 10000 && dc < 60000) {
		std::chrono::seconds sec = std::chrono::duration_cast<std::chrono::seconds>(d);
		sprintf(msg, "[%d] %s%lu s.\n", checkCount, prefix.c_str(), sec.count());
	} else {
		std::chrono::minutes min = std::chrono::duration_cast<std::chrono::minutes>(d);
		sprintf(msg, "[%d] %s%lu min.\n", checkCount, prefix.c_str(), min.count());
	}*/

	printf("%s", msg);
}
