#include "greenlee.h"
#include "display.h"


namespace green {	
	// neighbor count for every code
	uchar _glncounts[256] = {0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8};
	// codes for 1 pixel lines: 0 no, 1: yes
						// 0                    10                   20                   30                   40
	uchar _glLines[256] = {1,1,1,0,1,1,0,0,1,1, 1,0,0,1,0,0,1,1,1,1, 1,1,1,1,0,1,0,0,0,1, 0,0,1,1,1,1,1,1,1,1, 1,1,1,1,0,0,0,0,0,1,
	                       1,1,1,1,1,1,0,0,0,0, 0,0,0,0,1,1,1,1,1,1, 1,1,1,1,1,0,1,1,0,0, 1,1,1,1,1,1,1,0,1,1, 0,0,1,0,0,0,0,1,1,1,
	                       1,1,1,1,0,0,1,0,0,0, 0,0,0,1,1,1,1,0,1,0, 0,0,0,0,0,0,0,0,1,0, 1,0,1,1,0,0,1,1,1,0, 1,1,0,0,1,1,1,0,1,1,
	                       0,0,1,1,1,0,1,1,0,0, 1,0,1,0,1,0,1,0,1,1, 1,0,1,0,0,0,0,0,1,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,1,1,0,0,
	                       1,1,1,0,1,1,0,0,1,1, 0,0,1,0,0,0,1,1,0,0, 1,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0};

	// Returns the greenlee1985 code for the CV_8U image coords
	// works even on the image border
	// white is usually 255 but can be another number (ex. 0) if specified
	uchar encode(const Mat& img, int x, int y, uchar white) {
		const uchar *row, *rowPrev, *rowNext;
		int yMax = img.rows - 1;
		uchar code = 0;
		row = img.ptr<uchar>(y);

		if (y > 0) {
			rowPrev = img.ptr<uchar>(y - 1);
			code += 128 * (rowPrev[x] != white);
		}

		if (y < yMax) {
			rowNext = img.ptr<uchar>(y + 1);
			code += 8 * (rowNext[x] != white);
		}

		if (x > 0) {
			if (y > 0) code += 64 * (rowPrev[x-1] != white);
			if (y < yMax) code += 16 * (rowNext[x-1] != white);
			code += 32 * (row[x-1] != white);
		}
		if (x < img.cols - 1) {
			if (y > 0) code += 1 * (rowPrev[x+1] != white);
			if (y < yMax) code += 4 * (rowNext[x+1] != white);
			code += 2 * (row[x+1] != white);
		}
		//printf("gl_getCode(%d,%d) = %d", x, y, code);

		return code;
	}

	uchar encode(const Mat& img, int x, int y) {
		return encode(img, x, y, 255);
	}

	// Encodes all non-white pixels of source into dest
	// white pixels are assigned a 0 value instead of their neighborhood
	// origin should be CV_8U and ideally B&W only (0 or 255 pixels value)
	// Function initializes and resets dest to ceros (0) and then encodes black pixels in origin
	// mode 0 stores greenlee codes, 1 stores non-empty neighbor count
	void encodeMatrixForeground(const Mat &source, Mat &dest, uchar bgColor, uchar mode) {
		Mat kernel = (Mat_<float>(3, 3) << 64.f,128.f,1.f, 32.f,0.f,2.f, 16.f,8.f,4.f);
		cv::filter2D(source, dest, -1, kernel);
		dest = dest.mul(source);

		// set neighbor count
		if (mode == 1) {
			std::mutex mtx;
			dest.forEach<uchar>([&dest, &mtx, bgColor, mode](uchar &code, const int *pos) -> void {
				if (code != 0) {
					lock_guard<mutex> guard(mtx);
					code = neighborCount(code);
				}
			});
		}
	}
	void encodeMatrixForeground(const Mat &source, Mat &dest, uchar bgColor) {
		encodeMatrixForeground(source, dest, bgColor, 0);
	}

	// variation of encodeMatrixForeground where we store non-empty neighbor count instead of greenlee codes
	void encodeMatrixNeighborCount(const Mat &source, Mat &dest, uchar bgColor) {
		encodeMatrixForeground(source, dest, bgColor, 1);
	}

	// encodes all pixels including whites with black neighbors
	void encodeMatrix(const Mat &source, Mat &dest) {
		dest = Mat(source.size(), CV_8U);
		dest = Scalar(0);
		std::mutex mtx;

		source.forEach<uchar>([&source, &dest, &mtx](uchar value, const int* pos) -> void {
			uchar code = encode(source, pos[1], pos[0], 255);
			lock_guard<mutex> guard(mtx);
			dest.at<uchar>(pos[0], pos[1]) = code;
		});
	}

	// Given a greenlee encoded matrix, gets the neighbor value for (x,y) in the direction
	// returns true if the neighbor is not empty
	// Only 1 pixel access is needed (to get the code)
	// direction is: 0:ne, 1:e, 2:se, 3:s, 4:sw, 5:w, 6:nw, 7:n
	bool getNeighbor(const Mat &encoded, int x, int y, uchar direction) {
		uchar code = encoded.at<uchar>(y, x);
		return getNeighbor(code, direction);
	}

	// Given a GL code, return the neighbor to the direction
	// direction is: 0:ne, 1:e, 2:se, 3:s, 4:sw, 5:w, 6:nw, 7:n
	// returns true if the neighbor is not empty
	bool getNeighbor(uchar code, uchar direction) {
		switch (direction) {
			case 0: return (code % 2 == 1); break;
			case 1:
				if (code >= 128) code -= 128;
				if (code >= 64) code -= 64;
				if (code >= 32) code -= 32;
				if (code >= 16) code -= 16;
				if (code >= 8) code -= 8;
				if (code >= 4) code -= 4;
				return (code >= 2);
			break;
			case 2:
				if (code >= 128) code -= 128;
				if (code >= 64) code -= 64;
				if (code >= 32) code -= 32;
				if (code >= 16) code -= 16;
				if (code >= 8) code -= 8;
				return (code >= 4);
			break;
			case 3:
				if (code >= 128) code -= 128;
				if (code >= 64) code -= 64;
				if (code >= 32) code -= 32;
				if (code >= 16) code -= 16;
				return (code >= 8);
			break;
			case 4:
				if (code >= 128) code -= 128;
				if (code >= 64) code -= 64;
				if (code >= 32) code -= 32;
				return (code >= 16);
			break;
			case 5:
				if (code >= 128) code -= 128;
				if (code >= 64) code -= 64;
				return (code >= 32);
			break;
			case 6:
			if (code >= 128) code -= 128;
			return (code >= 64);
			break;
			case 7: return (code >= 128); break;
		}

		printf("green::ERROR: invalid direction %d", direction);
		return false;
	}

	// returns the number of non-empty neighbors for the code
	uchar neighborCount(uchar code) {
		return _glncounts[code]; // we use a global array with the count
	}

	// true if greenlee code is an endpoint
	bool isEndpoint(uchar code) {
		return (code==1 || code==2 || code==4 || code==8 || code==16 || code==32 || code==64 || code==128);
	}

	// moves the point towards the code.
	// Point must be an endpoint
	void displaceEndpoint(Point &pos, uchar code) {
		switch(code) {
			case 1: pos.x++; pos.y--; break;
			case 2: pos.x++; break;
			case 4: pos.x++; pos.y++; break;
			case 8: pos.y++; break;
			case 16: pos.x--; pos.y++; break;
			case 32: pos.x--; break;
			case 64: pos.x--; pos.y--; break;
			case 128: pos.y--; break;
		}
	}

	// Receives a GreenLee code for an endpoint; Returns the code for the endpoint in the opposite direction
	uchar oppositeEndpoint(uchar dir) {
		switch (dir) {
			case 1: return 16;
			case 2: return 32;
			case 4: return 64;
			case 8: return 128;
			case 16: return 1;
			case 32: return 2;
			case 64: return 4;
			case 128: return 8;
		}
		return 0;
	}

	/* true if code2 is too different from code1
	   code1 is the reference
	   tolerance is max pixel distance between codes without becoming opposite
	   tolerance = [1,3], with
				   3 being polar oposites
				   2 oposite can also be next to the polar opposite
				   1 only neighbors next to the pixel are not opposite
	*/
	bool opposites(uchar code1, uchar code2, uchar tolerance) {
		uchar opp = oppositeEndpoint(code1);
		if (code2 == opp) return true;

		if (tolerance == 2) {
			switch (code1) {
				case 1:   return (code2 == 8 || code2 == 32);
				case 2:   return (code2 == 16 || code2 == 64);
				case 4:   return (code2 == 32 || code2 == 128);
				case 8:   return (code2 == 64 || code2 == 1);
				case 16:  return (code2 == 128 || code2 == 2);
				case 32:  return (code2 == 1 || code2 == 4);
				case 64:  return (code2 == 2 || code2 == 8);
				case 128: return (code2 == 4 || code2 == 16);
			}
		}
		else if (tolerance == 1) {
			if (code1 == code2) return false;
			switch (code1) {
				case 1:   return !(code2 == 128 || code2 == 2);
				case 2:   return !(code2 == 1 || code2 == 4);
				case 4:   return !(code2 == 2 || code2 == 8);
				case 8:   return !(code2 == 4 || code2 == 16);
				case 16:  return !(code2 == 8 || code2 == 32);
				case 32:  return !(code2 == 16 || code2 == 64);
				case 64:  return !(code2 == 32 || code2 == 128);
				case 128: return !(code2 == 64 || code2 == 1);
			}
		}

		return false;
	}
	bool opposites(uchar code1, uchar code2) {
		return opposites(code1, code2, 3);
	}

	// Returns true if the code corresponds to a 1 px line, using a manually created codemap
	// complexity O(1)
	bool is1pxLine(uchar code) {
		return (_glLines[code] == 1);
	}



}
