#include "display.h"

namespace disp {

struct winInfo {
	string name;
	int w, h;
};

int _maxWindowsWidth = 1920;
int _maxWindowsHeight = 1080;
int _nextFreeX = 0;
string lastWindowName;
list<winInfo> windows;


/**
 * @brief addWindowName
 * @param winName
 */
bool addWindow(const string winName, int w, int h) {
	// First we remove closed windows
	windows.remove_if([](const winInfo &wi) {
		return (cv::getWindowProperty(wi.name, WND_PROP_AUTOSIZE) == -1);
	});

	// update nextFreeX value
	_nextFreeX = 0;
	for (winInfo &wi : windows) _nextFreeX += wi.w;

	if ((_nextFreeX + w/2) > _maxWindowsWidth) {
		_nextFreeX = 0;
	}

	// Search for this window
	for (winInfo &wi : windows) {
		if (wi.name == winName) return false;
	}

	// register new window
	winInfo nwi;
	nwi.name = winName;
	nwi.w = w;
	nwi.h = h;
	windows.push_back(nwi);

	return true;
}


// displays an image in a flexible window
void show(Mat &img, const std::string winName) {
	namedWindow(winName, WINDOW_NORMAL | WINDOW_KEEPRATIO | WINDOW_GUI_EXPANDED);

	int w = img.cols, h = img.rows;
	float ratio = float(h)/w;

	// maximum image size by default
	if (w > _maxWindowsWidth) w = _maxWindowsWidth;
	if (h > _maxWindowsHeight) h = _maxWindowsHeight;

	// If image is too small, resize it automatically
	if (w < 200) {
		w = 640;
		h = round(float(w) * ratio);
		if (h > _maxWindowsHeight) {
			h = _maxWindowsHeight;
			w = round(float(h) / ratio);
		}
	}

	// update next window position
	if (addWindow(winName, w, h)) {
		lastWindowName = winName;
		moveWindow(winName, _nextFreeX, 0);
	}

	cv::resizeWindow(winName, w, h);
	imshow(winName, img);
}

void showNormalized(Mat &img, const std::string winName) {
	if (img.empty()) {
		std::cout << "display ERROR: The image in [" << winName << "] is empty. Ignoring...\n";
		return;
	}

	// normalize to [0,255]
	Mat imgNorm;
	cv::normalize(img, imgNorm, 0, 255, NORM_MINMAX, CV_8UC1);
	show(imgNorm, winName);
}

void showColormap(Mat &img, const std::string winName, string savePath, Mat &imgCol) {
	std::mutex mtx;
	if (img.empty()) {
		printf("ShowColormap(%s) :: ERROR: Image is empty. Omitting...\n", winName.c_str());
		return;
	}
	if (imgCol.empty()) imgCol = Mat(img.rows, img.cols, CV_8UC3, Scalar(0));

	switch (img.type()) {
		case CV_8U:
			img.forEach<uchar>([&mtx, &imgCol](uchar &val, const int* pos) -> void {
				if (val > 0) { imgCol.at<Vec3b>(pos[0], pos[1]) = px::shinyColor(val); }
			});
		break;
		case CV_16U:
		img.forEach<ushort>([&mtx, &imgCol](ushort &val, const int* pos) -> void {
			if (val > 0) { imgCol.at<Vec3b>(pos[0], pos[1]) = px::shinyColor(val); }
		});
		break;
	}

	// save to path if needed
	if (savePath.length() > 0) {
		imwrite(savePath, imgCol);
	}

	show(imgCol, winName);
}
void showColormap(Mat &img, const std::string winName, string savePath) {
	Mat empty;
	showColormap(img, winName, savePath, empty);
}
void showColormap(Mat &img, const std::string winName) {
	Mat empty;
	showColormap(img, winName, "", empty);
}


// given a mat of float angles from 0.0 to 1.0, mapped to 0-180
// color them accordingly using a hue map
void showAnglemap(Mat &img, const string winName, string savePath) {
	if (img.empty()) { printf("ShowColormap(%s) :: ERROR: Image is empty. Omitting...\n", winName.c_str()); return; }
	if (img.type() != CV_32F) { printf("ShowColormap(%s) :: ERROR: Image format not implemented. Omitting.\n", winName.c_str()); return; }

	uchar maxHue = 170; // max hue for HSV color; should be less than 180
	Mat imgCol = Mat(img.size(), CV_8UC3, Scalar(0,0,0));

	std::mutex mtx;
	img.forEach<float>([&img, &imgCol, maxHue](float &angle, const int* pos) -> void {
		if (angle >= 0) {
			uchar hue;
			if (angle > 1.f) hue = maxHue;
			else hue = uchar(angle * 180.f);
			imgCol.at<Vec3b>(pos) = Vec3b(hue, 255, 255);
		}
	});

	// Draw hue guide at the first row of the image
	//for (int x = 0 ; x <= maxHue ; x++) imgHeat.at<Vec3b>(0, x) = Vec3b(x, 255, 255);
	cv::cvtColor(imgCol, imgCol, COLOR_HSV2BGR);

	// save to path if needed
	if (savePath.length() > 0) {
		imwrite(savePath, imgCol);
	}

	show(imgCol, winName);
}
void showAnglemap(Mat &img, const string winName) {
	showAnglemap(img, winName, "");
}


} // end disp
