#ifndef PROJECT_VECTOR2D_H
#define PROJECT_VECTOR2D_H

#include <opencv2/core.hpp>
#include "geometry.h"
using namespace cv;

class Vector2D {
private:
	float sign(const float f);

public:
	float x, y;
	
	Vector2D();
	Vector2D(float x, float y);
	
	void set(float x, float y);
	float magnitude();
	void magnitude(const float m);
	Vector2D& addMagnitude(float c);
	Vector2D& substractMagnitude(float c);
	Vector2D& reduceMagnitude(float c);
	Vector2D& normalize();
	void invert();
	
	Vector2D direction();
	bool isZero();
	
	float dotProduct(Vector2D &v);

	// OpenCV dependant
	Point integerCoords();
	operator Point2f();
	Vector2D(Point2f p);
	
	Vector2D operator+(const Vector2D& other);
	Vector2D operator-(const Vector2D& other);

	void operator+=(const Vector2D& other);
	void operator-=(const Vector2D& other);

	Vector2D operator+(const float &c);
	Vector2D operator-(const float &c);
	Vector2D operator*(const float &c);
	friend Vector2D operator * (float c, Vector2D &v);

	void operator+=(const float &c);
	void operator-=(const float &c);
	void operator*=(const float &c);
	void operator=(const Point2f& p);
	

};


#endif //PROJECT_VECTOR2D_H
