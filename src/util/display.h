#ifndef DISPLAY_H
#define DISPLAY_H

#include "px.h"
#include "opencv2/highgui.hpp"
#include <iostream>
#include <list>
#include <algorithm>

using namespace cv;
using std::string;
using std::list;

namespace disp {
	bool addWindow(const string winName, int w, int h);
	void show(Mat &img, const string winName);
	void showNormalized(Mat &img, const string winName);

	void showColormap(Mat &img, const string winName, string savePath, Mat &out);
	void showColormap(Mat &img, const string winName, string savePath);
	void showColormap(Mat &img, const string winName);

	void showAnglemap(Mat &img, const string winName, string savePath);
	void showAnglemap(Mat &img, const string winName);

	// Displays imgUnder and imgOver overlapped
	// Both images should be of pixel type T (uchar, etc.)
	// imgUnder pixels are white, imgOver are red, as long as their values are different form bgColor
	template <class T>
	void showOverlapped(Mat &imgUnder, Mat &imgOver, T bgColor, const string winName, const string savePath) {
		std::mutex mtx;
		Mat imgCol = Mat(imgUnder.rows, imgUnder.cols, CV_8UC3, Scalar(0));
		Vec3b both = Vec3b(255,50,155);
		Vec3b under = Vec3b(0,0,255);
		Vec3b over = Vec3b(0,255,0);

		imgUnder.forEach<T>([&mtx, &imgUnder, &imgCol, bgColor, under](T &val, const int* pos) -> void {
			if (val != bgColor) {
				imgCol.at<Vec3b>(pos) = under;
			}
		});

		imgOver.forEach<T>([&mtx, &imgUnder, &imgCol, &over, &both, bgColor](T &val, const int* pos) -> void {
			if (val != bgColor) {
				if (imgUnder.at<T>(pos) == bgColor) {
					imgCol.at<Vec3b>(pos) = over;
				} else {
					imgCol.at<Vec3b>(pos) = both;
				}
			}
		});

		// save to path if needed
		if (savePath.length() > 0) {
			imwrite(savePath, imgCol);
		}

		show(imgCol, winName);
	}
}

#endif // DISPLAY_H
