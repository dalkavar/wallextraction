#include "mbr.h"

MBR::MBR() {
	reset();
}

// Initialize with a Point as center
// Point does NOT get counted
MBR::MBR(Point p) {
	p0 = Point(p.x, p.y);
	p1 = Point(p.x, p.y);
	h = 0; w = 0;
}

// Initialize MBR values to fit region of Points
MBR::MBR(vector<Point> region) {
	Point p = region.front();

	p0 = Point(p.x, p.y);
	p1 = Point(p.x, p.y);
	h = 0; w = 0;

	for (Point &p : region) { countPoint(p); }

	print();
}

// Updates MBR and adds the new point to the pixel count
void MBR::countPoint(Point p) {
	if (p0.x > p.x) { p0.x = p.x; }
	else if (p1.x < p.x) { p1.x = p.x; }

	if (p0.y > p.y) { p0.y = p.y; }
	else if (p1.y < p.y) { p1.y = p.y; }

	w = p1.x - p0.x + 1;
	h = p1.y - p0.y + 1;
}

// Prints object to console
void MBR::print() {
	printf("MBR from (%d, %d) to (%d, %d), %d x %d", p0.x, p0.y, p1.x, p1.y, w, h);
}

// Clears all information
void MBR::reset() {
	p0 = Point(0, 0);
	p1 = Point(0, 0);
	w = 0;
	h = 0;
}
