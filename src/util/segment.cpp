#include "segment.h"


Segment::Segment() {
	p0 = Point2f(0,0);
	p1 = Point2f(0,0);
	thickness = 0.0;
	horizontalAspect = true;
	sliceThickness = 0.0;
}

// Parametrized constructor. Will compute ALL properties
Segment::Segment(Point2f p0, Point2f p1, float sliceThickness) : Segment() {
	this->p0 = p0;
	this->p1 = p1;
	this->sliceThickness = sliceThickness;
	update();
}

/**
 * Initialize by deserializing text
 * @param text string in the format described in unserialize()
 */
Segment::Segment(string text) {
	unserialize(text);
}


// Substract coordinates from both points (apply an offset)
void Segment::substract(Point2f p) {
	p0.x -= p.x;
	p0.y -= p.y;
	p1.x -= p.x;
	p1.y -= p.y;
}

// Multiplies all coordinates and thickness by the factor
void Segment::scale(float f) {
	p0 *= f;
	p1 *= f;
	thickness *= f;
	sliceThickness*= f;
	if (horizontalAspect) {
		slope *= f;
	} else {
		slope /= f;
	}
}

/**
 * @brief Computes geometrical segment center
 * @return center
 */
Point2f Segment::getCenter() {
	return geo::avgPoint(p0, p1);
}

/**
 * Returns one of the 2 normals for this segment
 * @param forwardOrder uses the direction p0->p1 (right hand) in that order; false uses order p1->p0.
 * @return Unit vector, normal
 */
Point2f Segment::getNormal(bool forwardOrder) {
	// horizontal & vertical cases
	if (isPerfectVertical()) {
		return (forwardOrder) ? Point2f(1.f, 0.f) : Point2f(-1.f, 0.f);
	}
	if (isPerfectHorizontal()) {
		return (forwardOrder) ? Point2f(0.f, -1.f) : Point2f(0.f, 1.f);
	}
	
	Point2f dir = geo::directionBetweenPoints(p0, p1);
	return (forwardOrder) ? Point2f(dir.y, -dir.x) : Point2f(-dir.y, dir.x);
}

/**
 * re-computes all segment properties from p0, p1 and sliceThickness
 * @param withThickness true also recomputes thickness from slicethickness
 */
void Segment::update(bool withThickness) {
	length = geo::euclideanDistance(p0, p1);
	
	if (withThickness) computeThickness();

	if (p0.x == p1.x) {
		if (p0.y > p1.y) std::swap<Point2f>(p0, p1); // organize
		horizontalAspect = false;
		slope = 0.0;
		offset = p0.x;
		angle = M_PI_2;
	}
	else if (p0.y == p1.y) {
		if (p0.x > p1.x) std::swap<Point2f>(p0, p1); // organize
		horizontalAspect = true;
		slope = 0.0;
		offset = p0.y;
		angle = 0.0;
	}
	else {
		if (p0.x > p1.x) std::swap<Point2f>(p0, p1); // always organize by x unless perfect vertical segment
		slope = double(p1.y - p0.y) / (p1.x - p0.x);
		horizontalAspect = (fabs(p1.y - p0.y) < fabs(p1.x - p0.x));
		offset = p0.y - slope * p0.x;
		angle = geo::radiansBetweenPoints(p0, p1);
	}
}
void Segment::update() { update(true); }

// Computes thickness from sliceThickness and slope (supposing they are set)
void Segment::computeThickness() {
	bool horizontalSlices = !horizontalAspect;
	if (sliceThickness == 0.f) return; // guard against uninitialized slice thickness
	
	//printf("Segment::computeThickness() with sliceThickness %.2f and slope %.2f.", sliceThickness, slope);
	if (slope == 0.0) {
		thickness = sliceThickness;
	}
	else {
		float a = std::fabs(angle);
		thickness = sliceThickness * ((horizontalSlices) ?  sin(a) : cos(a));
	}
}

// Given y, gets x for a point of this segment's line equation
// Point may NOT be contained in the range of the line segment
float Segment::getXforY(float y) {
	if (p0.x == p1.x) return p0.x; // Guard against vertical
	
	return geo::xForYinRect(slope, offset, y);
}
float Segment::getYforX(float x) {
	if (p0.y == p1.y) return p0.y; // Guard against horiz
	
	return geo::yForXinRect(slope, offset, x);
}

// True if this segment is highly similar to Segment s2
// tolerance is the distance threshold
bool Segment::isSimilarTo(const Segment *s2, float tolerance) {
	if (p0 == s2->p0 && p1 == s2->p1) {
		return true;
	}
	else if (geo::euclideanDistance(p0, s2->p0) <= tolerance) {
		return (geo::euclideanDistance(p1, s2->p1) <= tolerance);
	}

	return false;
}

// given a slice between points start and end, return true if slice intercepts the line equation for this segment
bool Segment::interceptsLine(Point &start, Point &end) {
	bool horizSlice = (start.y == end.y);
	float sx = start.x, sy = start.y, ex = end.x, ey = end.y;

	// handle vertical segment case first
	if (p0.x == p1.x) {
		if (horizSlice) return (p0.x >= sx && p0.x < ex + 1.0);
		else return (floor(p0.x) == sx);
	}

	// Diagonal segment
	if (horizSlice) {
		float lx = geo::xForYinRect(this->slope, this->offset, sy + 0.5);
		return (lx >= sx && lx < ex + 1.0);
	} else {
		float ly = geo::yForXinRect(this->slope, this->offset, sx + 0.5);
		return (ly >= sy && ly < ey + 1.0);
	}
}

/**
 * @brief Returns true if this segment contains point p, within a distance tolerance
 * @param p Point2f
 * @param t tolerance maximum distance from point to line segment
 * @return
 */
bool Segment::containsPoint(Point2f &p, float t) {
	// guard against coordinates outside of MBR
	if (p.x < (p0.x - t) && p.x < (p1.x - t)) return false;
	if (p.x > (p0.x + t) && p.x > (p1.x + t)) return false;
	if (p.y < (p0.y - t) && p.y < (p1.y - t)) return false;
	if (p.y > (p0.y + t) && p.y > (p1.y + t)) return false;

	// Guard against perfect vertical or horizontal (MBR check is enough)
	if (p0.x == p1.x || p0.y == p1.y) return true;

	// perfect point in line?
	float psx = getXforY(p.y), psy = getYforX(p.x);
	if (p.x == psx && p.y == psy) return true;

	// compare with the theoretical points; this gives us more precision than comparing against the full segment, especially for long segments
	Point2f pSegX = Point2f(psx, p.y);
	Point2f pSegY = Point2f(p.x, psy);
	float dist = geo::distPointToRect(p, pSegX, pSegY);
	//printf("dist from (%.1f,%.1f) to segment (%.1f,%.1f)-(%.1f,%.1f) = %.1f\n", p.x, p.y, pSegX.x, pSegX.y, pSegY.x, pSegY.y, dist);
	return (dist <= t);
}

/**
 * @brief Compares the angle of this segment with another's with some tolerance
 * @param other Pointer to the other segment
 * @param maxAngleDif maximum angle difference tolerable in degrees
 * @return
 */
bool Segment::isParallelTo(Segment *other, float maxAngleDif) {
	if (horizontalAspect != other->horizontalAspect) return false;
	if (angle == other->angle || fabs(angle - other->angle) < 0.00001f) return true;
	return (geo::lineDirectionDiference(geo::toDegrees(angle), geo::toDegrees(other->angle)) <= maxAngleDif);
}

/**
 * @brief If this segment is colinear (both segments are part of the same line)
 * @param other Pointer to the other segment
 * @param maxAngleDif maximum angle difference tolerable in degrees
 * @return
 */
bool Segment::isCollinearTo(Segment *other, float maxAngleDif) {
	// We require them to be approx. of equal angle
	if (!isParallelTo(other, maxAngleDif)) return false;

	// If they are collinear, each endpoint is part of the other segment's equation
	if (length >= other->length) {
		if (geo::distPointToLineProjection(other->p0, p0, p1) > 0.7f) return false;
		if (geo::distPointToLineProjection(other->p1, p0, p1) > 0.7f) return false;
	} else {
		if (geo::distPointToLineProjection(p0, other->p0, other->p1) > 0.7f) return false;
		if (geo::distPointToLineProjection(p1, other->p0, other->p1) > 0.7f) return false;
	}

	return true;
}

/**
 * @brief Returns true if this segment is a perfect vertical line
 * @return bool
 */
bool Segment::isPerfectVertical() {
	return (p0.x == p1.x);
}

bool Segment::isPerfectHorizontal() {
	return (p0.y == p1.y);
}

void Segment::print() {
	printf("p0:(%.1f,%.1f) | p1:(%.1f,%.1f) | angle: %.2f° | length: %.1f | thick: %.2f | sliceThick: %.2f | %s\n",
		   p0.x, p0.y, p1.x, p1.y, geo::toDegrees(angle), length, thickness, sliceThickness, ((horizontalAspect) ? "horizontal_aspect" : "vertical_aspect"));
}
void Segment::printCoords() {
	printf("(%.1f,%.1f)-(%.1f,%.1f)\n", p0.x, p0.y, p1.x, p1.y);
}

/**
 * Initializes Segment contents from text
 * @param text a line with csv format: p0.x, p0.y, p1.x, p1.y, thickness
 */
void Segment::unserialize(string &text) {
	std::stringstream ss(text);
	std::string item;
	getline(ss, item, ',');
	p0.x = stof(item);
	getline(ss, item, ',');
	p0.y = stof(item);
	getline(ss, item, ',');
	p1.x = stof(item);
	getline(ss, item, ',');
	p1.y = stof(item);
	getline(ss, item, ',');
	thickness = stof(item);
	update();
}

/**
 * True if p is near the line projection of this segment, with a maxOffset tolerance
 * @param p
 * @param maxOffset maximum distance from p to the projection
 * @return
 */
bool Segment::isCollinearTo(Point2f &p, float maxOffset) {
	if (isPerfectHorizontal()) {
		return (fabs(p.y - p0.y) <= maxOffset);
	}
	else if (isPerfectVertical()) {
		return (fabs(p.x - p0.x) <= maxOffset);
	}
	else {
		Point2f ps;
		if (horizontalAspect) {
			ps.x = p.x;
			ps.y = getYforX(ps.x);
			return (fabs(ps.y - p.y) <= maxOffset);
		} else {
			ps.y = p.y;
			ps.x = getXforY(ps.y);
			return (fabs(ps.x - p.x) <= maxOffset);
		}
	}
}

Rect Segment::getMBR() {
	float x0 = min<float>(p0.x, p1.x);
	float y0 = min<float>(p0.y, p1.y);
	float w = fabs(p0.x - p1.x);
	float h = fabs(p0.y - p1.y);
	return Rect(x0, y0, w, h);
}

// '0' if p0 is closer to p than p1, '1' otherwise
uchar Segment::closestEndpointTo(Point2f &p) {
	float dp0 = geo::euclideanDistance(p0, p);
	float dp1 = geo::euclideanDistance(p1, p);
	return (dp0 <= dp1) ? '0' : '1';
}
