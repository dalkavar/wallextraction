#include "debug.h"

namespace debug {
	string logPath = "log.txt";
	//FILE *fp = fopen(logPath.c_str(), "w"); // automagically closed by the system
}

// code is a number from 0 to 5
void debug::log(char colorCode, const char *fmt, ...) {
	//if (DEBUG_MODE) {
		char text[512];
		va_list args;
		va_start(args, fmt);
		vsprintf(text, fmt, args);
		va_end(args);

		char letters = colorCode; // letters
		if (letters > 5) letters = 0;

		// offset for color codes
		letters += 31;

		printf("\33[0;%dm%s\033[0m", letters, text);
		//fprintf(fp, "%s", text);
	//}
}

void debug::log(const char *fmt, ...) {
	char text[512];
	va_list args;
	va_start(args, fmt);
	vsprintf(text, fmt, args);
	va_end(args);

	//fprintf(fp, "%s", text);
}
