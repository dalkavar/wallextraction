#include "geometry.h"

#define G_2PI (2.0*M_PI)

// converts rads to degrees; normalizes when needed
float geo::toDegrees(double radians) {
	return radians * DEGREES_PER_RAD;
}
float geo::toRadians(double degrees) {
	return degrees * M_PI / 180.0;
}

/**
 * Returns an "integer" version of the float Point coordinates
 * @param p
 * @return Same Point with truncated decimal position
 */
Point geo::integerPoint(Point2f p) {
	return Point((int)floor(p.x), (int)floor(p.y));
}



// returns the slope dy on dx=1, between p0 and p1
// p0 and p1 can be in any order; slope will always go in the +x direction
// returns 0.0 if p0 and p1 are part of a vertical or horizontal line
float geo::getSlopeY(Point2f p0, Point2f p1) {
	if (p0.x == p1.x || p0.y == p1.y) return 0.0;

	if (p0.x < p1.x) {
		return (p1.y - p0.y) / (p1.x - p0.x);
	} else {
		return (p0.y - p1.y) / (p0.x - p1.x);
	}
}

// returns the slope dx on dy=1, between p0 and p1
// p0 and p1 can be in any order; slope will always go in the +y direction
// returns 0.0 if p0 and p1 are part of a vertical or horizontal line
float geo::getSlopeX(Point2f p0, Point2f p1) {
	if (p0.x == p1.x || p0.y == p1.y) return 0.0;

	if (p0.y < p1.y) {
		return (p1.x - p0.x) / (p1.y - p0.y);
	} else {
		return (p0.x - p1.x) / (p0.y - p1.y);
	}
}

// returns the slope between p0 and p1, on the dimension x (dx) if horizontal, or dy otherwise
// proxy for getSLopeX and getSlopeY
float geo::getSlopeXY(Point2f p0, Point2f p1, bool horizontal) {
	return (horizontal) ? getSlopeX(p0, p1) : getSlopeY(p0, p1);
}

// solves x for y=mx + b for a given y
float geo::xForYinRect(double m, double b, double y) {
	if (m == 0) return 0.0; // never happens
	return (y - b) / m;
}
// Given a y=mx+b equation, get y for given x
float geo::yForXinRect(double m, double b, double x) {
	return m * x + b;
}



// obtains y=mx+b line factors for p0 and p1
// returns false and m=0 if line is vertical
bool geo::points2LineEquation(Point2f p0, Point2f p1, double &m, double &b) {
	// handle vertical lines
	if (p0.x == p1.x) {
		m = 0; b = p1.x;
		return false;
	}

	// handle horizontal lines
	if (p0.y == p1.y) {
		m = 0; b = p1.y;
		return true;
	}

	// order points by x
	if (p0.x > p1.x) cv::swap<Point2f>(p0, p1);

	m = (p1.y - p0.y) / (p1.x - p0.x);
	b = p0.y - m * p0.x;
	return true;
}

// returns a Vec2f with m and b for a rect equation y=mx+b
// obtained for a rect with slope m that contains p
Vec2f geo::pointSlope2LineEquation(const Point2f &p, float m) {
	return Vec2f(m, p.y - m * p.x);
}

// Performs linear regression in points vector to get a y=mx+b equivalent
// returns the correlation coefficient [0.0, 1.0] where 1.0 is the best correlation and low values <0.2 mean no correlation
// If the line is perfectly vertical, returns a big m and b=avgX, with correlation 1.0
double geo::linearRegression(vector<Point2f> &points, double &m, double &b) {
	double x, y, r, sumX = 0.0, sumY = 0.0, sumY2 = 0.0, sumX2 = 0.0, sumXY = 0.0, n = 0;

	// handle case of only 2 points
	if (points.size() == 2) {
		geo::points2LineEquation(points[0], points[1], m, b);
		return 1.0;
	}

	for (Point2f &p : points) {
		x = p.x;
		y = p.y;
		sumX += x;
		sumY += y;
		sumX2 += x * x;
		sumXY += x * y;
		sumY2 += y * y;
	}
	n = points.size();

	double avgX = (sumX / n), avgY = (sumY / n);
	double Sxx = (sumX2 / n) - (avgX * avgX);
	double Syy = (sumY2 / n) - (avgY * avgY);
	double Sxy = (sumXY / n) - (avgX * avgY);

	// Guard against perfect vertical lines
	if (fabs(Sxx) <= 0.1) {
		//printf("perfect vertical line!\n");
		m = 999999.;
		b = avgX;
		return 1.0;
	}

	// Guard against perfect vertical lines
	if (fabs(Syy) <= 0.1) {
		//printf("perfect horizontal line!\n");
		m = 0.;
		b = avgY;
		return 1.0;
	}


	m = Sxy / Sxx;
	b = avgY - m * avgX;

	if (fabs(m) > 0.001) {
		r = std::fabs(Sxy / (sqrt(Sxx) * sqrt(Syy))); // correlation coefficient
	} else {
		// Guard against perfect horizontal line division by 0
		r = 1.0;
	}

	//printf("Linear regression with %.0f points, sumX %.2f, sumY %.2f, m %.2f, b %.2f, r %.2f\n", n, sumX, sumY, m, b, r);
	//printf("Sxx %.2f, Syy %.2f, Sxy %.2f\n", Sxx, Syy, Sxy);
	return r;
}


// Returns angle between points p0 and p1 in radians, measured counter-clockwise from p0 axis +x
// angle returned is always between (-PI/2, PI/2]
float geo::radiansBetweenPoints(Point2f p0, Point2f p1) {
	// deal with horizontal and vertical
	if (p0.y == p1.y) {
		return (p0.x <= p1.x) ? 0.0 : M_PI;
	} else if (p0.x == p1.x) {
		return (p0.y <= p1.y) ? M_PI_2 : -M_PI_2;
	} else {
		// make sure points are ordered
		if (p0.x > p1.x) { cv::swap<Point2f>(p0, p1); }
		float dx = abs(p1.x - p0.x);
		float dy = abs(p1.y - p0.y);
		return (p0.y > p1.y) ? atan(dy/dx) : - atan(dy/dx);
	}
}


// Returns radians between line segments P and Q expressed by their endpoints
// Handles vertical and horizontal angles
float geo::radiansBetweenSegments(Point2f &p0, Point2f &p1, Point2f &q0, Point2f &q1) {
	float ap = radiansBetweenPoints(p0, p1);
	float aq = radiansBetweenPoints(q0, q1);
	return angleDifferenceRad(ap, aq);
}

/**
 * @brief Returns the minimum angle difference between 2 angles (input can be any angle, positive or negative)
 * @param a degrees
 * @param b degrees
 * @return  degrees
 */
float geo::angleDifferenceDeg(float a, float b) {
	if (a == b) return 0.0;

	float d = a - b;
	return abs(abs(std::fmod((d + 180), 360)) - 180);
}

// Rad min distance between 2 rad angles
float geo::angleDifferenceRad(float a, float b) {
	if (a == b) return 0.0;

	float d = a - b;
	return abs(abs(std::fmod((d + M_PI), 2*M_PI)) - M_PI);
}

// Returns a measure [0.0, 1.0] of how similar one angle is to another
// 1 means parallel angles, 0 means perpendicular angles
// values in-between are the minimum angle difference ignoring angle direction, normalized
// can recover angle difference with (1-d)*M_PI_2
float geo::angleSimilarity(float a, float b) {
	// normalize angles to [-PI, PI]
	a = normalizeAngle(a);
	b = normalizeAngle(b);
	if (a == b) return 1.0;

	// get difference against b and b proyected
	float d0 = abs(a - b);
	float d1 = abs(a - normalizeAngle(b + M_PI));

	// normalize differences
	if (d0 > M_PI) { d0 = -(G_2PI - d0); }
	else if (d0 < -M_PI) { d0 = G_2PI + d0; }
	if (d1 > M_PI) { d1 = -(G_2PI - d1); }
	else if (d1 < -M_PI) { d1 = G_2PI + d1; }
	d0 = abs(d0);
	d1 = abs(d1);

	float d = std::min<float>(d0, d1); // minimum angle difference, projections considered, between 0 and PI/2
	return 1.0 - (d / M_PI_2); // normalized, inverted
}

// Normalizes rad angle to [-PI, PI]
float geo::normalizeAngle(float a) {
	if (a >= -M_PI && a <= M_PI) return a;

	// Normalize to [-2PI, 2PI]
	if (a > G_2PI) { a -= G_2PI * floor(a / G_2PI); }
	else if (a < -G_2PI) { a -= G_2PI * ceil(a / G_2PI); }

	// Normalize to [-PI, PI]
	if (a > M_PI) { a = -(G_2PI - a); }
	else if (a < -M_PI) { a = G_2PI + a; }

	return a;
}

// hypotenuse from cathetes
float geo::hypotenuse(float c1, float c2) {
	return sqrt(c1*c1 + c2*c2);
}


// Given 2 ranges (a0, a1) and (b0, b1) return the longitude of their intersection
// Range edges don't need to be in ascending order
// returns 0 if they never intersect
float geo::rangeIntersection(float a0, float a1, float b0, float b1) {
	// organize ranges
	float temp;
	if (a0 > a1) { temp = a0; a0 = a1; a1 = temp; }
	if (b0 > b1) { temp = b0; b0 = b1; b1 = temp; }

	// get interval length
	if ((b0 > a1) || (a0 > b1)) {
		return 0.0;
	} else {
		float ran0 = max(a0, b0);
		float ran1 = min(a1, b1);
		return abs(ran0 - ran1);
	}
}

// line segments a0-a1 and b0-b1 intersect? and where?
// returns: -1 (never cross and parallel), 0 (never cross), 1 (cross)
// factor is the multiplier to the length of (a,b) that would make it touch the other segment; numbers close to 0 and 1 mean they are almost connected
char geo::getLineIntersection(Point2f a0, Point2f a1, Point2f b0, Point2f b1, Point2f &cross, Point2f &f) {
	cross.x = 0; cross.y = 0;

	float s1_x, s1_y, s2_x, s2_y;
	s1_x = a1.x - a0.x;     s1_y = a1.y - a0.y;
	s2_x = b1.x - b0.x;     s2_y = b1.y - b0.y;
	float det = (-s2_x * s1_y + s1_x * s2_y);

	// if determinant is near zero, lines are colinear or parallel
	float detThreshold = 0.1;
	if (det >= 0 && det <= detThreshold) { return -1; }

	// handle vertical lines
	if (a0.y == a1.y && b0.y == b1.y) { return -1; }

	f.x = (-s1_y * (a0.x - b0.x) + s1_x * (a0.y - b0.y)) / det;
	f.y = ( s2_x * (a0.y - b0.y) - s2_y * (a0.x - b0.x)) / det;

	if (f.x >= 0 && f.x <= 1 && f.y >= 0 && f.y <= 1) {
		// Collision detected
		cross.x = a0.x + (f.x * s1_x);
		cross.y = a0.y + (f.y * s1_y);
		//printf("%.2f, %.2f between (%.1f, %.1f)-(%.1f, %.1f) and (%.1f, %.1f)-(%.1f, %.1f)\n", f.x, f.y, a0.x, a0.y, a1.x, a1.y, b0.x, b0.y, b1.x, b1.y);
		return 1;
	}

	return 0; // No collision, not parallel
}


// returns the cross point of {y = m1x + b1} and {y = m2x + b2}
// returns (0,0) if they never cross (parallel)
Point2f geo::rectCrosspoint(double m1, double b1, double m2, double b2) {
	// parallel lines return 0
	if (m1 == m2) return Point2f(0.0, 0.0);

	// handle horizontal lines
	double crossX, crossY;
	if (m1 == 0.0) {
		crossY = b1;
	} else if (m2 == 0.0) {
		crossY = b2;
	} else {
		crossY = (m2 * b1 - m1 * b2) / (m2 - m1);
	}

	crossX = (b2 - b1) / (m1 - m2);

	return Point2f(crossX, crossY);
}

// given 2 line points a0-a1 and b0-b1, return the point where their lines cross
// returns false if they are parallel
// the point returned might not be part of the line segments since we compare their line equations only
bool geo::linesCrosspoint(Point2f p0, Point2f p1, Point2f q0, Point2f q1, Point2f &cross) {
	// do they ever cross?
	double m_p, b_p, m_q, b_q;
	bool valid_p = geo::points2LineEquation(p0, p1, m_p, b_p);
	bool valid_q = geo::points2LineEquation(q0, q1, m_q, b_q);
	if (!valid_p && !valid_q) return false;
	if (m_p == m_q) return false;

	// handle vertical lines
	if (!valid_p && valid_q) {
		cross.x = p0.x;
		cross.y = m_q * cross.x + b_q;
	}
	else if (valid_p && !valid_q) {
		cross.x = q0.x;
		cross.y = m_p * cross.x + b_p;
	}
	else {
		// non-vertical, non-parallel lines
		cross = geo::rectCrosspoint(m_p, b_p, m_q, b_q);
	}

	return true;
}

// returns a Point for the projection of p into y=mx+b
Point2f geo::projectPoint2Rect(const Point2f &p, float m, float b) {
	// get a perpendicular rect that passes through p
	Vec2f rect = pointSlope2LineEquation(p, -(1 / m));

	float m2 = rect[0];
	float b2 = rect[1];
	//printf("perpendicular line: y = %.1f + x + (%.1f)", m2, b2);

	return rectCrosspoint(m, b, m2, b2);
}



// Returns euclidean distance between p0 and p1
float geo::euclideanDistance(const Point2f &p0, const Point2f &p1) {
	if (p0 == p1) return 0.f;
	else if (p0.x == p1.x) return fabs(p0.y - p1.y);
	else if (p0.y == p1.y) return fabs(p0.x - p1.x);

	return sqrt(pow(p1.y - p0.y, 2) + pow(p1.x - p0.x, 2));
}

// Distance from point (C) to segment A-B
float geo::distPointToRect(const Point2f &p, const Point2f &q1, const Point2f &q2) {
	// Guard against equal endpoints
	if (q1.x == q2.x && q1.y == q2.y) return euclideanDistance(q1, p);

	float c1 = euclideanDistance(q2, p);
	float c2 = euclideanDistance(q1, p);
	float base = euclideanDistance(q1, q2);

	return triangleHeightFromEdges(base, c1, c2);
}

/**
 * @brief Minimum distance from point p to the infinite line that contains endpointA and endpointB
 * @param p
 * @param a endpointA
 * @param b endpointB
 * @return
 */
float geo::distPointToLineProjection(Point2f &p, const Point2f &q0, const Point2f &q1) {
	// Handle vertical case
	if (q0.x == q1.x) {
		return std::fabs(p.x - q0.x);
	}
	// Handle horizontal case
	else if (q0.y == q1.y) {
		return std::fabs(p.y - q0.y);
	}
	else {
		double m, b;
		points2LineEquation(q0, q1, m, b);
		Point2f r0 = Point2f(xForYinRect(m, b, p.y), p.y);
		Point2f r1 = Point2f(p.x, yForXinRect(m, b, p.x));
		return distPointToRect(p, r0, r1);
	}
}


// Returns the perpendicular distance (minimum distance) between 2 rect equations y = m1*x + b1 and y = m2*x + b2
// Use with parallel rects ONLY, otherwise the distance is unreliable
float geo::distBetweenRects(double m, double b0, double b1) {
	if (m == 0.f) return std::fabs(b1 - b0); // horizontal lines

	float d = std::fabs(b1 - b0) / sqrt(m * m + 1.f);
	//printf("dist between (%.1f)x + %.1f and (%.1f)x + %.1f is %.1f\n", m, b0, m, b1, d);
	return d;
}

// Returns the perpendicular distance (minimum distance) between 2 rect equations y = m1*x + b1 and y = m2*x + b2
// p1 should be a point in rect segment 1 and is used to support non-parallel lines
// p1 serves as the distance measuring point since it will be different along non-parallel lines
// Use with rects of similar slope!
float geo::distBetweenRects(float m1, float b1, const Point2f &p1, float m2, float b2) {
	if (m1 == 0 && m2 == 0) return abs(b2 - b1); // horizontal lines

	// create rect perpendicular to both (on average)
	float m3 = (m1 + m2) / 2.0;
	m3 = -1.0 / m3;
	Vec2f perpen = pointSlope2LineEquation(p1, m3);
	//printf("perpendicular line: y = %.1f + x + (%.1f)", m3, perpen[1]);

	Point2f cross = rectCrosspoint(m2, b2, m3, perpen[1]);

	return euclideanDistance(p1, cross);
}

// Obtains the distance between 2 parallel or almost-parallel rects (a0, a1) and (b0, b1)
// might return imprecise results for non-parallel lines
// Returns a percent of intersection [0, 1] of the bigger segment
// Returns 0.0 if the rects are not positioned face-to-face
float geo::distanceBetweenParallelRects(Point2f A0, Point2f A1, Point2f B0, Point2f B1, float &distance, Point2f &direction) {
	double m0, b0, m1, b1;
	direction.x = 0.f; direction.y = 0.f;
	float lA = geo::euclideanDistance(A0, A1);
	float lB = geo::euclideanDistance(B0, B1);
	float lAnB, lAuB = lA + lB;
	bool swapped = false;

	// make sure the first line segment A has the shortest longitude
	if (lA > lB) { swapped = true; swap<Point2f>(A0, B0); swap<Point2f>(A1, B1); swap<float>(lA, lB); }

	bool verticalA = !points2LineEquation(A0, A1, m0, b0);
	bool verticalB = !points2LineEquation(B0, B1, m1, b1);

	// handle vertical lines (we suppose they are ALMOST vertical)
	if (verticalA || verticalB) {
		distance = fabs(0.5f*(A0.x + A1.x) - 0.5f*(B0.x + B1.x));
		lAnB = rangeIntersection(A0.y, A1.y, B0.y, B1.y);
		direction.x = (A0.x < B0.x) ? 1.f : -1.f;
		if (swapped) direction = -direction;
		return lAnB;
	}
	// handle horizontal lines
	if (m0 == 0 && m1 == 0) {
		distance = fabs(A0.y - B0.y);
		lAnB = rangeIntersection(A0.x, A1.x, B0.x, B1.x);
		direction.y = (A0.y < B0.y) ? 1.f : -1.f;
		if (swapped) direction = -direction;
		return lAnB;
	}

	// handle diagonals

	// prepare perpendicular slope mp
	double w0 = lA / lAuB;
	double w1 = lB / lAuB;
	double meanM = w0 * m0 + w1 * m1; // weighted mean slope
	double mp = - 1.0 / meanM; // perpendicular mean slope

	// grab an average reference point between both lines
	Point2f C0 = geo::avgPoint(A0, B0);
	Point2f C1 = geo::avgPoint(A1, B1);
	Point2f C = geo::avgPoint(C0, C1);

	// get perpendicular distance
	Vec2f eqp = pointSlope2LineEquation(C, mp);
	float bpp = eqp[1];
	Point2f pp0 = rectCrosspoint(m0, b0, mp, bpp);
	Point2f pp1 = rectCrosspoint(m1, b1, mp, bpp);
	distance = euclideanDistance(pp0, pp1);
	
	direction = geo::directionBetweenPoints(pp0, pp1);
	if (swapped) direction = -direction;

	// get perpendicular projection points of the endpoints of the small line on the big line
	Vec2f eq0, eq1;
	Point2f proy0, proy1;
	float rangeX, rangeY;
	eq0 = pointSlope2LineEquation(A0, mp);
	eq1 = pointSlope2LineEquation(A1, mp);
	proy0 = rectCrosspoint(m1, b1, mp, eq0[1]);
	proy1 = rectCrosspoint(m1, b1, mp, eq1[1]);
	rangeX = rangeIntersection(B0.x, B1.x, proy0.x, proy1.x);
	rangeY = rangeIntersection(B0.y, B1.y, proy0.y, proy1.y);


	float intersect = (rangeX == 0 || rangeY == 0) ? 0 : hypotenuse(rangeX, rangeY);

	// Optional: display a debug view of the lines
	/*Mat debug = Mat(465, 611, CV_8UC3, Scalar(0,0,0));
	cv::circle(debug, A0, 2, Scalar(255,0,0), cv::FILLED);
	cv::circle(debug, A1, 2, Scalar(255,0,0), cv::FILLED);
	cv::line(debug, A0, A1, Scalar(255,0,0));
	cv::circle(debug, B0, 2, Scalar(205,0,255), cv::FILLED);
	cv::circle(debug, B1, 2, Scalar(205,0,255), cv::FILLED);
	cv::line(debug, B0, B1, Scalar(205,0,255));
	cv::circle(debug, C, 1, Scalar(100,100,100), cv::FILLED);
	cv::circle(debug, pp0, 1, Scalar(0,255,255), cv::FILLED);
	cv::circle(debug, pp1, 1, Scalar(0,255,255), cv::FILLED);
	cv::circle(debug, proy0, 1, Scalar(0,255,2), cv::FILLED);
	cv::circle(debug, proy1, 1, Scalar(0,255,2), cv::FILLED);

	char legend[] = "d: 0.0000, i: 0.0000      ";
	sprintf(legend, "d: %.1f, i: %.2f", distance, intersect);

	cv::putText(debug, legend, Point(10,10), 1, 1, Scalar(255,255,255));
	disp::show(debug, "common");
	cv::resizeWindow("common", 1280, 860);
	waitKey(0);*/

	return intersect;
}


// returns the average coordinates between p0 and p1
Point2f geo::avgPoint(const Point2f &p0, const Point2f &p1) {
	return Point2f(0.5f * (p0.x + p1.x), 0.5f * (p0.y + p1.y));
}

/**
 * @brief Given line angles a and b (degrees), return the minimum angle distance between their line projections (ex: 0 and 180 have 0 difference)
 * @param a degrees (counter-clockwise from +x)
 * @param b degrees
 * @return angle distance between the projected directions
 */
float geo::lineDirectionDiference(float a, float b) {
	if (a == b) return 0.f;
	if (fmod(a, 180.f) == 0.f && fmod(b, 180.f) == 0.f) return 0.f;

	// handle normal angles
	float d1 = geo::angleDifferenceDeg(a, b);
	float d2 = geo::angleDifferenceDeg(a, b + 180.f);
	return std::min<float>(d1, d2);
}

/**
 * @brief Given 3 edge length a, b, c for a triangle, return the area using Heron's formula
 * @param a edge length, order does not matter
 * @param b
 * @param c
 * @return area of triangle
 */
float geo::triangleArea(float a, float b, float c) {
	float s = 0.5f * (a + b + c);
	float m = s * (s - a) * (s - b) * (s - c);

	if (m < 0) return 0.0; // guard against impossible area
	return sqrt(m);
}

/**
 * @brief Given only the edge lengths of the triangle (base edge must be known), return the triangle height
 * @param base length
 * @param s1 length (not base)
 * @param s2 length (not base)
 * @return triangle height relative to base edge
 */
float geo::triangleHeightFromEdges(float base, float s1, float s2) {
	return 2.f * triangleArea(base, s1, s2) / base;
}

/**
 * Returns a unit vector that points from p0 to p1
 * @param p0
 * @param p1
 * @return Point2f Unit vector
 */
Point2f geo::directionBetweenPoints(Point2f &p0, Point2f &p1) {
	return (p1-p0) / euclideanDistance(p0, p1);
}

/**
 * Retturns the opposite unit vector for dir
 * @param dir
 * @return Point2f unitary vector
 */
Point2f geo::oppositeDirection(Point2f &dir) {
	return -dir;
}

/**
 * Given the vector vect, returns the unit vector that represents its direction
 * @param vect
 * @return unit vector
 */
Point2f geo::vectorDirection(Point2f &vect) {
	return vect / vectorMagnitude(vect);
}

/**
 * Returns a vector's magnitude
 * @param vect
 * @return magnitude scalar
 */
double geo::vectorMagnitude(Point2f &vect) {
	return sqrt(vect.x * vect.x + vect.y * vect.y);
}

/**
 * Given x, y components of a unit vector, return the angle in DEGREES from +x ccw
 * @param x
 * @param y
 * @return angle in degrees (-180, 180]
 */
float geo::angleFromUnitVector(float x, float y) {
	if (x == 0 && y == 0) return 0.f;
	
	// horiz / vert cases
	if (x == 0) {
		return (y > 0) ? 90 : -90;
	}
	else if (y == 0) {
		return (x > 0) ? 0 : 180;
	}
	// diagonal cases
	else {
		float a = (float)(180 / M_PI) * atan2(y, x);
		if (a > 180) a = -(360 - a);
		return a;
	}
}

/**
 * Given segments p0-p1 and q0-q1, returns direction pointing from p to q
 * Segments are expected to be parallel, otherwise we try our best guess
 * @return unit vector pointing from p to q
 */
Point2f geo::directionBetweenSegments(Point2f &p0, Point2f &p1, Point2f &q0, Point2f &q1) {
	// Handle vertical and horizontal cases
	if (p0.x == p1.x || q0.x == q1.x) {
		// verticals
		float p_x = (p0.x == p1.x) ? p0.x : (p0.x + p1.x) / 2.f;
		float q_x = (q0.x == q1.x) ? q0.x : (q0.x + q1.x) / 2.f;

		if (p_x < q_x) return Point2f(1.f, 0.f);
		else if (p_x > q_x) return Point2f(-1.f, 0.f);
		else return Point2f(0.f, 0.f);
	}
	else if (p0.y == p1.y || q0.y == q1.y) {
		// horizontals
		float p_y = (p0.y == p1.y) ? p0.y : (p0.y + p1.y) / 2.f;
		float q_y = (q0.y == q1.y) ? q0.y : (q0.y + q1.y) / 2.f;

		if (p_y < q_y) return Point2f(0.f, 1.f);
		else if (p_y > q_y) return Point2f(0.f, -1.f);
		else return Point2f(0.f, 0.f);
	}
	else {
		// diagonals
		double p_m, p_b, q_m, q_b, m, b, p_l, q_l;
		Point2f c, p_cross, q_cross;

		// get equation of line orthogonal to both segments
		points2LineEquation(p0, p1, p_m, p_b);
		points2LineEquation(q0, q1, q_m, q_b);
		p_l = euclideanDistance(p0, p1);
		q_l = euclideanDistance(q0, q1);
		m = - 2.f / (p_m + q_m); // inverse average slope
		c = (p_l < q_l) ? avgPoint(p0, p1) : avgPoint(q0, q1);
		b = c.y - m * c.x;
		printf("m=%.3f, b=%.3f\n", m, b);

		// get orthogonal line crossing for both
		p_cross = rectCrosspoint(p_m, p_b, m, b);
		q_cross = rectCrosspoint(q_m, q_b, m, b);
		printf("crosses: (%.2f, %.2f) (%.2f, %.2f)\n", p_cross.x, p_cross.y, q_cross.x, q_cross.y);
		return directionBetweenPoints(p_cross, q_cross);
	}
}

/**
 * True if v has both components as 0.f
 * @param v
 * @return
 */
bool geo::isZero(Point2f &v) {
	return (v.x == 0 && v.y == 0);
}

// Returns the pixel length for sub-pixel length l
int geo::integerLength(double l) {
	if (l - floor(l) < 0.5) {
		return (int)floor(l);
	} else {
		return (int)ceil(l);
	}
}

