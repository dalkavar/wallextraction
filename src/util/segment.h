#ifndef SEGMENT_H
#define SEGMENT_H

#include <opencv2/core.hpp>
#include <sstream>
#include <string>
#include "geometry.h"


using namespace std;
using namespace cv;

/**
 * Represents a line segment
*/
class Segment {
public:
	Point2f p0, p1; // initial and final endpoints for the first and last slices
	float length;

	double angle; // angle in radians between p0 and p1
	double slope; // dy
	double offset; // y = (slope)*(x) + displacement

	float thickness = 1.f; // transversal line thickness
	float sliceThickness = 1.f; // vertical or horizontal slice thickness
	bool horizontalAspect; // true if segent's MBR width is higher than height

	Segment();
	Segment(Point2f p0, Point2f p1, float sliceThickness);
	Segment(string text);
	void update(bool withThickness);
	void update();
	void computeThickness();

	void substract(Point2f p);
	void scale(float f);

	Point2f getCenter();
	Point2f getNormal(bool forwardOrder);
	Rect getMBR();

	float getXforY(float y);
	float getYforX(float x);

	bool isSimilarTo(const Segment *s2, float tolerance);
	bool interceptsLine(Point &start, Point &end);
	bool containsPoint(Point2f &p, float tolerance);
	bool isParallelTo(Segment *other, float maxAngleDif);
	bool isCollinearTo(Segment *other, float maxAngleDif);
	bool isCollinearTo(Point2f &p, float maxOffset);
	uchar closestEndpointTo(Point2f &p);

	bool isPerfectVertical();
	bool isPerfectHorizontal();

	virtual void print();
	void printCoords();
	void unserialize(string &text);
};

#endif // SEGMENT_H
