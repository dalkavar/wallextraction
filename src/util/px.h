#ifndef PAINT_H
#define PAINT_H

#include <mutex>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
#include <geometry.h>

using namespace std;
using namespace cv;

// Pixel operations
namespace px {
	Point integerPoint(Point2f p);
	bool pointInsideMat(Point2f p, Mat& img);
	bool isPixelInsideMat(int x, int y, Mat& img);

	void setColor3(Mat& img, Point p, Vec3b color);
	void setColor3(Mat& img, Point2f p, Vec3b color);

	void setWhite(Mat& img, Point p);
	void setWhite(Mat& img, int x, int y);
	void setBlack(Mat& img, Point p);
	void setBlack(Mat& img, Point2f p);
	void setBlack(Mat& img, int x, int y);

	bool isBlack3(Mat& img, int x, int y);
	bool isBlack(Mat& img, int x, int y);
	bool isBlack(Mat& img, Point p);
	bool isBlack(Mat& img, Point2f p);
	bool looksBlack(Vec3b color, uchar threshold);

	bool isColor3(Vec3b& color);
	bool isGray3(Vec3b& color, uchar diff);
	bool isWhite3(Vec3b& color);

	bool isWhite(Mat& img, int x, int y);
	bool isWhite(Mat& img, Point p);

	bool notWhite(Mat& img, Point p);
	bool notWhite(Mat& img, Point2f p);

	Point nearestBlack(Mat& img, Point p);

	Point displacePoint(Point p, char direction, char length);

	void get8Neighbors(Mat &img, int x, int y, uchar nhood[]);
	void get8Neighbors(Mat &img, int x, int y, float nhood[]);
	uchar nhood8Jumps(uchar nhood[], uchar bgColor);
	vector<Point> getNeighbors(Mat& img, Point p, uchar gray);
	vector<Point> blackNeighbors(Mat& img, Point p);
	uchar countBlackNeighbors(Mat& img, int x, int y);

	void colorRegion3(Mat& img, vector<Point>& points, Vec3b color);
	void semicolorLine(Mat& img, Point p0, Point p1);

	void alterColor3(Mat& img, Point p);

	void addBorder(Mat& img, uchar color);
	Vec3b shinyColor(ushort n);
	void shinyColormapImage(Mat &input, Mat &output, ushort bgColor);

	void overlapImages(Mat &imgBackground, Mat &imgForeground, Scalar bgColor);

	uchar gl_getCode(const Mat& img, int x, int y);
	void displaceEndpoint(Point& p, uchar dir);
	bool gl_isEndpoint(uchar c);
	bool gl_similarDirection(uchar c1, uchar c2);
	uchar gl_oppositeEndpoint(uchar dir);
	uchar gl_directionCode(Point &from, Point &to);
	void gl_deleteNeighbors(Mat &img, int x, int y, short code, uchar bgColor);

	void drawDirection(Mat &img, int x, int y, float angle);
	int countPixels(Mat &img, uchar color);
	bool autocrop(Mat &img, uchar bgColor, Point &p0, Point &p1);

	void drawLine(Mat &img, Point2f p0, Point2f p1, float thickness, uchar color);
	void drawSlice(Mat &img, Point2f c, float radius, bool verticalSlice, uchar color);

}

#endif // PAINT_H
