#include <opencv2/opencv.hpp>
#include <opencv2/ml.hpp>
#include <algorithm>
#include <iostream>

#include "SliceTrainer.h"
#include <util/display.h>

int main(int argc, char* argv[]) {
	// Generate lines image and gt
	SliceTrainer trainer;

	trainer.genDatasetLines(50, 3, 6, 6, 0, 178, 3);
	disp::showColormap(trainer.imgData, "Data");
	disp::showAnglemap(trainer.imgDataGt, "DataGt");

	trainer.train();
	trainer.test(0, 178);

	waitKey(0);
}