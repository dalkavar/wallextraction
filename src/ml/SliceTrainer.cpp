//
// Created by danyalejandro on 07/11/18.
//

#include "SliceTrainer.h"

/**
 * draws a grid of lines at different angles, and an angle ground-truth
 * Stores the result images in imgData and imgDataGt
 * @param length in pixels
 * @param thickness line transversal thickness
 * @param paddingX padding between cells
 * @param paddingY
 * @param minAngle start angle (degrees)
 * @param maxAngle end angle (degrees)
 */
void SliceTrainer::genDatasetLines(float length, float thickness, int paddingX, int paddingY, float minAngle, float maxAngle, float stepAngle) {
	// set the right grid size
	int numLines = (int)((maxAngle - minAngle + 1) / stepAngle) + 1;
	int numCols = (int) floor(sqrt(numLines));
	int numRows = numCols;
	while (numCols * numRows < numLines) numRows++;
	float r = length / 2;

	int cellW = 2 * int(r + paddingX);
	int cellH = 2 * int(r + paddingY);
	int w = cellW * numCols;
	int h = cellH * numRows;

	imgDataGt = Mat(h, w, CV_32F, Scalar(-1.f));
	imgData = Mat (h, w, CV_8U, Scalar(0));

	Point2f center;
	float angleDegrees = minAngle, angleRadians; // angle in radians
	float angleNormalized;
	Point2f p0, p1;

	for (int row = 0 ; row < numRows ; row++) {
		center.y = row * cellH + 0.5f * cellH;
		for (int col = 0 ; col < numCols ; col++) {
			center.x = col * cellW + 0.5f * cellW;
			angleRadians = geo::toRadians(angleDegrees);
			p0 = Point2f(center.x + r * cos(angleRadians), center.y - r * sin(angleRadians));
			p1 = Point2f(center.x - r * cos(angleRadians), center.y + r * sin(angleRadians));

			angleNormalized = (angleDegrees - minAngle) / (maxAngle - minAngle);
			cv::line(imgData, p0, p1, 1, (int)thickness);
			cv::line(imgDataGt, p0, p1, angleNormalized, (int)thickness);
			//px::drawLine(imgData, p0, p1, thickness, 0);
			//px::drawLine(imgDataGt, p0, p1, thickness, (uchar)angleDegrees);
			//cv::putText(im, std::to_string(angle), Point2f(xC+2, yC+10), cv::FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0));
			angleDegrees += stepAngle;
			if (angleDegrees > maxAngle) break;
		}
	}

	// After generating the dataset images, mine data
	prepareDataset();
}

/**
 * Mines data in imgData and imgDataGt to build the labeled dataset in dset_samples and dset_labels
 */
void SliceTrainer::prepareDataset() {
	// slice transform on data image
	st = SliceTransform(&imgData);
	st.scanSlicesThreaded();
	//st.showChannels();

	// fill and store dataset
	std::mutex mtx;
	char sampleKey[16];
	st.imgTransform.forEach<Vec4b>([&mtx, &sampleKey, this](Vec4b &p, const int* pos) -> void {
		if (p[0] != 0) {
			lock_guard<mutex> guard(mtx);

			// do we already have this sample?
			if (dset_keys.find(sampleKey) == dset_keys.end()) {
				sprintf(sampleKey, "%x%x%x%x%.2f", p[0], p[1], p[2], p[3], imgDataGt.at<float>(pos));
				
				// build the entries
				float h = p[0], v = p[1], d = p[2], e = p[3];
				float hvRatio = h / v;
				float deRatio = d / e;
				float hvAngle = atan(h / v);
				float deAngle = atan(d / e);
				float avg = 0.25f * (h + v + d + e);
				float hSize = h / avg, vSize = v / avg, dSize = d / avg, eSize = e / avg;

				dset_keys.insert(sampleKey);
				dset_samples.push_back({ hSize, vSize, dSize, eSize,
							                hvRatio, deRatio,
							                hvAngle, deAngle });
				dset_labels.push_back({imgDataGt.at<float>(pos)});
			}
		}
	});

	for (string const &s : dset_keys) cout << s << endl;

	// gather some random data for testing
	for (int i = 0 ; i < 50 ; i++) {
		int idx = (int)(rand() % dset_samples.size());
		test_samples.push_back(dset_samples.at(idx));
		test_labels.push_back(dset_labels.at(idx));
	}

}

void SliceTrainer::train() {
	printf("Training...\n");
	net << fully_connected_layer(8,8) << relu()
		<< fully_connected_layer(8,2) << relu()
		<< fully_connected_layer(2,1);

	adagrad optimizer;
	optimizer.alpha = 0.23f;
	int epoch = 0, epochs = 10;

	net.fit<mse>(optimizer, dset_samples, dset_labels, 1, epochs[&](){
		t.elapsed();
		t.reset();
	},
	// called for each epoch
	[&](){
		result res = net.test(test_samples, test_labels);
		cout << res.num_success << "/" << res.num_total << endl;
		ofstream ofs (("epoch_"+to_string(epoch++)).c_str());
		ofs << net;
	});
	printf("Finished training.\n");
}

void SliceTrainer::test(float minAngle, float maxAngle) {
	printf("Testing...\n");
	std::mutex mtx;
	Mat imgAngles(imgData.size(), CV_32F, Scalar(-1));
	st.imgTransform.forEach<Vec4b>([&mtx, &imgAngles, minAngle, maxAngle, this](Vec4b &p, const int* pos) -> void {
		if (p[0] != 0) {
			lock_guard<mutex> guard(mtx);
			tiny_dnn::vec_t in = {float(p[0]), float(p[1]), float(p[2]), float(p[3])};
			imgAngles.at<float>(pos) = net.predict(in)[0];
			cout << p << ": " << net.predict(in)[0] << endl;
		}
	});
	printf("Finished Testing.\n");

	disp::showAnglemap(imgAngles, "Angles Predicted");
}
