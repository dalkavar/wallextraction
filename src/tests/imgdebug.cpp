#include "imgdebug.h"

ImgDebug::ImgDebug(Mat *img, int factor) {
	this->img = img;
	this->factor = factor;
	window = Rect(0, 0, img->cols, img->rows);

	scale();
	drawGrid();
}

ImgDebug::ImgDebug(Mat *img, int factor, Rect window) {
	this->img = img;
	this->factor = factor;
	this->window = window;

	scale();
	drawGrid();
}


// Scales the image by a factor on both sides, with INTER_NEAREST (no interpolation)
void ImgDebug::scale() {
	cv::resize(*img, imgDebug, Size(), factor, factor, INTER_NEAREST);
}

// Draws a squared grid with cell side size "step" into the img
void ImgDebug::drawGrid() {
	if (factor <= 3) return; // guard against small factors

	int step = factor, x, y;
	Vec3b color;
	for (x = 0 ; x < imgDebug.cols ; x += step) {
		for (y = 0 ; y < imgDebug.rows ; y += 2) {
			color = imgDebug.at<Vec3b>(y, x);
			alterColor(color);
			imgDebug.at<Vec3b>(y, x) = color;
		}
	}

	for (y = 0 ; y < imgDebug.rows ; y += step) {
		for (x = 0 ; x < imgDebug.cols ; x += 2) {
			color = imgDebug.at<Vec3b>(y, x);
			alterColor(color);
			imgDebug.at<Vec3b>(y, x) = color;
		}
	}
}

// Changes the color components to a similar yet different color
void ImgDebug::alterColor(Vec3b &color) {
	uchar i = 0;
	if (color[0] == 255 && color[1] == 255 && color[2] == 255) {
		// white
		color[0] = 210; color[1] = 210; color[2] = 210;
	} else if (color[0] + color[1] + color[2] > 380) {
		// light color, make darker
		for (i = 0 ; i < 3 ; i++) {
			color[i] = (color[i] > 40) ? color[i] - 40 : 0;
		}
	} else {
		// dark color, make lighter!
		for (i = 0 ; i < 3 ; i++) {
			color[i] = (color[i] < 155) ? color[i] + 100 : 255;
		}
	}
}

// Saves the debug image in the provided path
void ImgDebug::save(string path) {
	imwrite(path, imgDebug);
}



// Draws the sub-pixel segment projection from it's center
// slope is the horizontal dy slope
// vertical = true only if the line is completely vertical
// Outputs on the scaled test image
void ImgDebug::projectLine(Point2f center, float slope, bool vertical, Vec3b color) {
	Point pCenter = scalePoint(Point2f(center.x - window.x, center.y - window.y));
	Point2f p = pCenter;
	int w = imgDebug.cols, h = imgDebug.rows;
	float dx, dy;

	if (!vertical && slope == 0.0) { // horizontal line
		dx = 1.0; dy = 0.0;
	} else if (vertical && slope == 0.0) { // vertical line
		dx = 0.0; dy = 1.0;
	} else { // diagonal
		if (vertical) {
			dx = (1 / slope); dy = 1.0;
		} else {
			dx = 1.0; dy = slope;
		}
	}

	// project to both sides
	while (p.x > 0 && p.y > 0 && p.x < w && p.y < h) {
		px::setColor3(imgDebug, p, color);
		p.x += dx; p.y += dy;
	}
	p = pCenter;
	while (p.x > 0 && p.y > 0 && p.x < w && p.y < h) {
		px::setColor3(imgDebug, p, color);
		p.x -= dx; p.y -= dy;
	}
}

/**
 * @brief Draws all values of imgvalues into the pixel cels of ImgDebug
 * @param imgValues 2D 1 channel values mat
 */
void ImgDebug::drawMatValues(Mat &imgValues) {
	std::mutex mtx;
	char cellText[32];

	int textFont = FONT_HERSHEY_PLAIN;
	Scalar textColor = Scalar(255,255,255);
	float textScale = 0.6;
	int lineType = cv::LINE_AA;

	switch (imgValues.type()) {
		case CV_8U:
			imgValues.forEach<uchar>([&mtx, &cellText, textFont, textColor, textScale, lineType, this](uchar &val, const int* pos) -> void {
				int x = pos[1], y = pos[0];

				lock_guard<mutex> guard(mtx);
				sprintf(cellText, "%d", val);
				cv::putText(this->imgDebug, cellText, Point(x * factor, (y + 1) * factor - 1), textFont, textScale, textColor, 1, lineType);
			});
		break;
		case CV_32F:
			imgValues.forEach<float>([&mtx, &cellText, textFont, textColor, textScale, lineType, this](float &val, const int* pos) -> void {
				int x = pos[1], y = pos[0];

				lock_guard<mutex> guard(mtx);
				sprintf(cellText, "%.1f", val);
				cv::putText(this->imgDebug, cellText, Point(x * factor, (y + 1) * factor - 1), textFont, textScale, textColor, 1, lineType);
			});
		break;

	}
}


// Draws the equivalent line form p0 to p1
// Receives non-scaled sub-pixel coordinates
void ImgDebug::drawLine(Point2f p0, Point2f p1, Vec3b color) {
	Point q0 = scalePoint(p0);
	Point q1 = scalePoint(p1);
	line(imgDebug, q0, q1, color);
}


void ImgDebug::drawArrow(Point2f origin, Point2f direction, float length, Vec3b color) {
	Point2f p = scalePoint(origin);
	cv::arrowedLine(imgDebug, p, p + Point2f(direction) * length * factor, Scalar(color), 1, 8, 0, 0.3);
}

void ImgDebug::drawArrow(Point2f origin, Point2f dest, Vec3b color) {
	Point2f p = scalePoint(origin);
	Point2f p2 = scalePoint(dest);
	cv::arrowedLine(imgDebug, p, p2, Scalar(color), 1, 8, 0, 0.05);
}


// Draws the scaled equivalent of pixel in position p
// Receives non-scaled sub-pixel coordinates
void ImgDebug::drawPoint(Point2f p, Vec3b color) {
	Point q = scalePoint(p);
	imgDebug.at<Vec3b>(q) = color;
}

// Draw scaled circle
void ImgDebug::drawCircle(Point2f p, float radius, Vec3b color) {
	float r = factor * radius;
	Point q = scalePoint(p);
	cv::circle(imgDebug, q, r, color, -1);
}

// Draws scaled text
void ImgDebug::drawText(Point2f p, string text, float textScale, Vec3b color) {
	int textFont = FONT_HERSHEY_PLAIN;
	textScale *= factor;
	int lineType = cv::LINE_AA;
	Point q = scalePoint(p);
	cv::putText(this->imgDebug, text, q, textFont, textScale, color, 1, lineType);
}


// Paints a pixel (grid cell) in the debug image, handles the change in scale and position
void ImgDebug::drawCell(Point p, Vec3b color) {
	int x = floor((p.x - window.x) * factor);
	int y = floor((p.y - window.y) * factor);
	rectangle(imgDebug, Rect(x, y, factor, factor), color, cv::FILLED);
}

// Colors the sub-pixel slice lines in the scaled segment
// Segment must be already scaled
// Works over an U8C3 scaled image
void ImgDebug::colorLinesOnSegment(Segment seg) {
	/*float slope = seg.slope;
	float dx, dy, f = float(factor), step = f;
	bool horizontalSlice = !seg.horizontalAspect;

	Point2f p = seg.p0;
	Vec3b color = Vec3b(255,0,255);
	float radius = seg.sliceThickness / 2;

	step = (seg.slope < 0) ? -f : f;

	if (horizontalSlice) {
		dx = (slope == 0.0) ? 0.0 : (1 / slope);
		dy = step;
		if (slope < 0) dx = -dx;
	} else {
		dx = step;
		dy = slope;
	}

	while (seg.mbrContains(p)) {
		colorSliceAsLine(p, radius, horizontalSlice, color);
		p.y += dy;
		p.x += dx;
	}*/
}

// Colors a sub-pixel slice line in the scaled debug image
// Segment must have been already scaled
void ImgDebug::colorSliceAsLine(Point2f center, float radius, bool horizontalSlice, Vec3b color) {
	Point2f p0 = center, p1 = center;
	if (horizontalSlice) {
		p0.x = center.x - radius;
		p1.x = center.x + radius;
	} else {
		p0.y = center.y - radius;
		p1.y = center.y + radius;
	}
	line(imgDebug, p0, p1, color);
}

// Semi-Colors the slices in the slicegroup along the segment
// Works over an U8C3 non-scaled image
// The input segment must have it's parameters set, included "horizontal" and "sliceThickness"
void ImgDebug::colorSlicesOnSegment(Segment seg) {
	/*float slope = seg.slope, dx, dy, step = 1.0;
	bool horizontalSlices = !seg.horizontalAspect;

	step = (seg.slope < 0) ? -1.0 : 1.0;

	if (horizontalSlices) {
		dx = (slope == 0.0) ? 0.0 : (1 / slope);
		dy = step;
		if (slope < 0) dx = -dx;
	} else {
		dx = step;
		dy = slope;
	}

	Slice slice = Slice(seg.sliceThickness, seg.p0, horizontalSlices);

	while (seg.mbrContains(slice.center)) {
		colorSlice(slice);
		slice.center.y += dy;
		slice.center.x += dx;
	}*/
}

// semi-colors a slice in img (U8C3)
void ImgDebug::colorSlice(Slice slice) {
	px::semicolorLine(imgDebug, slice.start, slice.end);
}



// Colors a vector of points, considering an offset
void ImgDebug::colorPointsOffset(vector<Point>& points, Point offset, Vec3b color) {
	for (Point& p : points) {
		imgDebug.at<Vec3b>(p.y - offset.y, p.x - offset.x) = color;
	}
}

// Returns a scaled version of the coordinates
Point ImgDebug::scalePoint(Point2f p) {
	return Point(floor(p.x * factor), floor(p.y * factor));
}
Point ImgDebug::scalePoint(Point p) {
	return scalePoint(Point2f(p.x, p.y));
}

