#ifndef IMGDEBUG_H
#define IMGDEBUG_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <px.h>
#include "../walls/geometry/slicegroup.h"

using namespace std;
using namespace cv;

class ImgDebug {
public:
	Mat imgDebug; // scaled CV_8UC3 debug image
	Mat *img; // pointer to original image

	Rect window; // ROI from the original image for this single test
	int factor; // zoom factor


	ImgDebug(Mat *input, int factor);
	ImgDebug(Mat *input, int factor, Rect roi);

	void scale();
	void drawGrid();
	void save(string path);

	void drawLine(Point2f p0, Point2f p1, Vec3b color);
	void drawArrow(Point2f origin, Point2f direction, float length, Vec3b color);
	void drawArrow(Point2f origin, Point2f dest, Vec3b color);
	void drawPoint(Point2f p, Vec3b color);
	void drawCircle(Point2f p, float radius, Vec3b color);
	void drawText(Point2f p, string text, float textSize, Vec3b color);
	void drawCell(Point p, Vec3b color);
	void projectLine(Point2f center, float slope, bool vertical, Vec3b color);
	void drawMatValues(Mat &imgValues);

	void colorSlicesOnSegment(Segment seg);
	void colorLinesOnSegment(Segment seg);
	void colorSlice(Slice slice);
	void colorSliceAsLine(Point2f p, float radius, bool horizontalSlice, Vec3b color);
	void colorPointsOffset(vector<Point>& points, Point offset, Vec3b color);

	Point scalePoint(Point2f p);
	Point scalePoint(Point p);



private:


	void alterColor(Vec3b &color);
};

#endif // IMGDEBUG_H
