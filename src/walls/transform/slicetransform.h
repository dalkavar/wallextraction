#ifndef SLICETRANSFORM_H
#define SLICETRANSFORM_H

#include <mutex>
#include <algorithm>
#include <thread>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "walls/geometry/slice.h"
#include "../../util/px.h"
#include "../../util/plothist.h"
#include "../../util/greenlee.h"
#include "../../util/geometry.h"
#include "anglematrix.h"

using namespace px;

class SliceTransform {
public:
	Mat imgTransform; // CV_8UC4 mat (horiz, vert, diag-up, diag-down)

	SliceTransform() = default;
	SliceTransform(Mat* img);

	void scanSlices();
	void scanSlicesThreaded();
	void scanSlicesForChannel(Mat &im, uchar ch);
	void showChannels();

private:
	Mat *img; // original B/ W image
	uchar bgColor = 0; // default background color

	void computeSliceCount(Mat &im, int x0, int y0, char dx, char dy);
	void fillSliceWithValue(Mat &im, int xf, int yf, char dx, char dy, int sliceLength);
};

#endif // SLICETRANSFORM_H
