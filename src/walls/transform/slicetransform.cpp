#include "slicetransform.h"


SliceTransform::SliceTransform(Mat *img) {
	if (img->empty()) {
		printf("\nSliceTransform FATAL ERROR: Input image must not be empty.\n");
		exit(1);
	}

	this->img = img;
}

/**
 * Non-threaded scan slices function, will scan all slice counts and store them in imgTransform
 */
void SliceTransform::scanSlices() {
	Mat imgH, imgV, imgD, imgE;
	scanSlicesForChannel(imgH, 0);
	scanSlicesForChannel(imgV, 1);
	scanSlicesForChannel(imgD, 2);
	scanSlicesForChannel(imgE, 3);

	// store everything into imgTransform
	std::mutex mtx;
	imgTransform = Mat(img->size(), CV_8UC4, Scalar (0,0,0,0)); // reset scanSlices Mat
	imgH.forEach<uchar>([&mtx, &imgV, &imgD, &imgE, this](uchar &h, const int* pos) -> void {
		if (h != 0) {
			lock_guard<mutex> guard(mtx);
			imgTransform.at<Vec4b>(pos) = Vec4b(h, imgV.at<uchar>(pos), imgD.at<uchar>(pos), imgE.at<uchar>(pos));
		}
	});
}

void SliceTransform::scanSlicesThreaded() {
	// Scan slices in 4 threads and wait for them
	Mat imgH, imgV, imgD, imgE;
	SliceTransform *pst = this;
	thread tH(&SliceTransform::scanSlicesForChannel, pst, std::ref(imgH), 0);
	thread tV(&SliceTransform::scanSlicesForChannel, pst, std::ref(imgV), 1);
	thread tD(&SliceTransform::scanSlicesForChannel, pst, std::ref(imgD), 2);
	thread tE(&SliceTransform::scanSlicesForChannel, pst, std::ref(imgE), 3);
	tH.join();
	tV.join();
	tD.join();
	tE.join();

	// store everything into st
	imgTransform = Mat(imgH.size(), CV_8UC4, Scalar (0,0,0,0)); // reset scanSlices Mat
	std::mutex mtx;
	imgH.forEach<uchar>([&mtx, &imgV, &imgD, &imgE, this](uchar &h, const int* pos) -> void {
		if (h != 0) {
			lock_guard<mutex> guard(mtx);
			imgTransform.at<Vec4b>(pos) = Vec4b(h, imgV.at<uchar>(pos), imgD.at<uchar>(pos), imgE.at<uchar>(pos));
		}
	});
}

/**
 * Scans slices for channel ch and stores the values on image im
 * @param im Empty CV_8U image, will be initialized here
 * @param ch { 0:h, 1:v, 2:d, 3:e }
 */
void SliceTransform::scanSlicesForChannel(Mat &im, uchar ch) {
	printf("Scanning slices for channel %d\n", ch);
	int x {0}, y {0}, w {img->cols}, h {img->rows};
	im = Mat(img->size(), CV_8U, Scalar(0));

	switch (ch) {
		case 0:
			// Get H channel (horizontal slices)
			for (y = 0 ; y < h ; y++) { computeSliceCount(im, 0, y, 1, 0); }
			break;
		case 1:
			// Get V channel (vertical slices)
			for (x = 0 ; x < w ; x++) { computeSliceCount(im, x, 0, 0, 1); }
			break;
		case 2:
			// Get D channel (diagonal slices, 45 degrees /)
			for (x = 0 ; x < w ; x++) { computeSliceCount(im, x, 0, -1, 1); }
			x = w - 1;
			for (y = 1 ; y < h ; y++) { computeSliceCount(im, x, y, -1, 1); }
			break;
		case 3:
			// Get E channel (diagonal slices, -45 degrees \)
			for (x = 0 ; x < w ; x++) { computeSliceCount(im, x, 0, 1, 1); }
			for (y = 1 ; y < h ; y++) { computeSliceCount(im, 0, y, 1, 1); }
			break;
		default:
			break;
	}
}


/**
 * A full run starting at (x0, y0) in direction (dx,dy) to fill slice values in imgTransform channel ch
 * @param im CV_8U image to store the slice counts, initialized to 0
 * @param x0 initial x position
 * @param y0 initial y position
 * @param dx x step
 * @param dy y step
 * @param ch image channel { 0:X, 1:Y, 2:D1, 3:D2 }
 */
void SliceTransform::computeSliceCount(Mat &im, int x0, int y0, char dx, char dy) {
	int x {x0}, y {y0}, w {im.cols}, h {im.rows};
	bool inSlice {false};
	int sliceCount {0};

	while (x >= 0 && x < w && y >= 0 && y < h) {
		if (img->at<uchar>(y, x) != bgColor) {
			if (sliceCount < 255) sliceCount++;
			if (!inSlice) inSlice = true;
		}
		else {
			// if inSlice still true, the slice ended last pixel
			if (inSlice) {
				inSlice = false;
				fillSliceWithValue(im, x - dx, y - dy, dx, dy, sliceCount);
				sliceCount = 0;
			}
		}
		x += dx; y += dy;
	}

	// Guard: if inSlice, the slice ended at the image border
	if (inSlice) {
		fillSliceWithValue(im, x - dx, y - dy, dx, dy, sliceCount);
	}
}

/**
 * fills slice with sliceLength value, from (xf,yf) counting sliceLength pixels, in direction (-dx,-dy)
 * @param im image to store result
 * @param xf last pixel of the slice
 * @param yf
 * @param dx advancement dx and dy (will go backwards)
 * @param dy
 * @param sliceLength number of pixels to go back (might be more than 255)
 */
void SliceTransform::fillSliceWithValue(Mat &im, int xf, int yf, char dx, char dy, int sliceLength) {
	// first position is the final pixel
	int x {xf};
	int y {yf};

	// store the count on all the slice pixels
	uchar count = uchar((sliceLength > 255) ? 255 : sliceLength); // max slice length is 255
	do {
		im.at<uchar>(y, x) = count;
		x -= dx;
		y -= dy;
		sliceLength--;
	} while (sliceLength > 0);
}

/**
 * Visualizes the 4 channels as colormaps
 */
void SliceTransform::showChannels() {
	vector<Mat> chs;
	split(imgTransform, chs);
	disp::showColormap(chs[0], "X Channel", "ch-x.png");
	disp::showColormap(chs[1], "Y Channel", "ch-y.png");
	disp::showColormap(chs[2], "D1 Channel", "ch-d1.png");
	disp::showColormap(chs[3], "D2 Channel", "ch-d2.png");
}

