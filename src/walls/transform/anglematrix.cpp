#include "anglematrix.h"

// Requires a pointer to the B/W image
AngleMatrix::AngleMatrix(Mat *img, Mat *imgTransform) {
	this->img = img;
	this->imgTransform = imgTransform;
	green::encodeMatrixNeighborCount(*img, imgNeighs, 0);
}

// Extracts RAD angles from every pixel in img and stores them in imgAngle
void AngleMatrix::extractAngles() {
	imgAngle = Mat(img->rows, img->cols, CV_32F, Scalar_<float>(-1.f));

	// for every pixel we approximate the angle
	std::mutex mtx;
	imgTransform->forEach<Vec4b>([this, &mtx](Vec4b &pixel, const int pos[]) -> void {
		if (pixel[0] > 0) {
			float angle = angleFromDiagonals(pos[1], pos[0], pixel[0], pixel[1], pixel[2], pixel[3]);

			lock_guard<mutex> guard(mtx);
			this->imgAngle.at<float>(pos) = angle;
		}
	});

	// Blur the result
	Mat imgCounts;
	green::encodeMatrixNeighborCount(*img, imgCounts, 0);
	//disp::showColormap(imgCounts, "Neighbor count");
	for (int i = 0 ; i < blurLevel ; i++) blurAngleMatrix(imgCounts);
}

/**
 * @brief Given slice pixel counts, proposes an angle and a reliability for the angle
 * @param cx horizontal slice
 * @param cy vertical slice
 * @param cd1 diagonal upwads slice
 * @param cd2 diagonal downwards slice
 * @param angle (output) in degrees [0, 180)
 */
float AngleMatrix::angleFromDiagonals(int x, int y, int cx, int cy, int cd1, int cd2) {
	// prepare floats for sub-pixel operations
	float cxf = float(cx);
	float cyf = float(cy);
	float cd1f = float(M_SQRT2 * cd1);
	float cd2f = float(M_SQRT2 * cd2);
	float shorter = 0.1f; // We use this to decide if one slice is much longer than the other
	float r[3];
	float angle, ahv, ad;

	// order counts from small to big
	float counts[4] = { cxf, cyf, cd1f, cd2f };
	std::sort(counts, counts + 4);
	//debug::log("(%d, %d) counts: %.0f %.0f %.0f %.0f\n", x, y, counts[0], counts[1], counts[2], counts[3]);
	float cMax = counts[3], cMin = counts[0];
	float fC = 0.5f;

	uchar neighCount = imgNeighs.at<uchar>(y, x);

	// Perfect vertical or horizontal line?
	if (cd1 == cd2 && cd1 == cx) {
		angle = 90.f;
		r[0] = 1.f - (cxf / cyf);
	}
	else if (cd1 == cd2 && cd1 == cy) {
		angle = 0.f;
		r[0] = 1.f - (cyf / cxf);
	}
	// perfect diagonals d1 / d2
	else if (cx == cy && cd2f < shorter * cd1f) {
		debug::log("Perfect D1 rule\n");
		angle = 45.f;
		r[0] = 1.f - (cd2f / cd1f);
	}
	else if (cx == cy && cd1f < shorter * cd2f) {
		angle = 135.f;
		r[0] = 1.f - (cd1f / cd2f);
	}
	// A diagonal that looks vertical or horizontal; low reliability
	else if (cd1 == cd2 && cx > cy) {
		angle = 0.f;
		r[0] = 1.f - (cyf / cxf);
	}
	else if (cd1 == cd2 && cy > cx) {
		angle = 90.f;
		r[0] = 1.f - (cxf / cyf);
	}
	// HV line crossings
	/*
	else if (fC*cMax >= cMin && fC*counts[2] >= cMin) {
		//debug::log("HV cross rule\n");
		if (cx == cMax) angle = 0.f;
		else if (cy == cMax) angle = 90.f;
		else if (cd1 == cMax) angle = 45.f;
		else angle = 135.f;
	}*/
	// approximate vertical / horizontal line with overlapped elements
	/*else if ((cy == cd1 || cy == cd2) && (cyf <= shorter * cxf)) {
		angle = 0.f;
		r[0] = 1.f - (cyf / cxf);
	}
	else if ((cx == cd1 || cx == cd2) && (cxf <= shorter * cyf)) {
		angle = 90.f;
		r[0] = 1.f - (cxf / cyf);
	}*/
	// handle non-perfect diagonal
	else {
		// prepare angle from diagonals and hv
		bool looksHorizontal = (cx > cy);
		bool looksAscending = (cd1 > cd2);
		if (looksHorizontal) {
			if (looksAscending) {
				ad = geo::toDegrees(atan(cd1f / cd2f) - M_PI_4);
				ahv = geo::toDegrees(atan(cyf / cxf));
				r[0] = 1.f - (cd2f / cd1f);
			} else {
				ad = geo::toDegrees(-(atan(cd2f / cd1f) - M_PI_4));
				ahv = -geo::toDegrees(atan(cyf / cxf));
				r[0] = 1.f - (cd1f / cd2f);
			}
		} else { //looksVertical
			if (looksAscending) {
				ad = geo::toDegrees(atan(cd2f / cd1f) + M_PI_4);
				ahv = geo::toDegrees(atan(cyf / cxf));
				r[0] = 1.f - (cd2f / cd1f);
			} else {
				ad = geo::toDegrees(-(atan(cd1f / cd2f) + M_PI_4));
				ahv = -geo::toDegrees(atan(cyf / cxf));
				r[0] = 1.f - (cd1f / cd2f);
			}
		}
		if (ad < 0.f) ad += 180.f;
		if (ahv < 0.f) ahv += 180.f;

		// decide which one to use
		float dd = fabs(cxf - cyf), dhv = fabs(cd1f - cd2f), f = 0.5;
		if (f*dd > dhv) {
			angle = ad;
		}else if (f*dhv > dd) {
			angle = ahv;
		} else {
			angle = 0.5f * (ad + ahv);
		}
		//debug::log("Got angle %.1f from MBR rules with ad=%.1f and ahv=%.1f\n", angle, ad, ahv);
	}

	if (angle < 0.f) angle += 180.f; // angle must be between 0 and 180

	printf("(%d,%d) with counts [%.1f,%.1f,%.1f,%.1f] gives angle %.1f\n", x, y, cxf, cyf, cd1f, cd2f, angle);
	return angle;
}

// Generates a color hue representation of the angles and
// returns it in CV_8UC3 out
void AngleMatrix::angleHueMap(Mat &imgHeat) {
	float maxHue = 180; // max hue for HSV color; should be less than 180
	float factor = 1;//maxHue / M_PI;
	imgHeat = Mat(imgAngle.size(), CV_8UC3, Scalar(0,0,0));

	imgAngle.forEach<float>([&imgHeat, factor](float &angle, const int* pos) -> void {
		if (angle >= 0) {
			imgHeat.at<Vec3b>(pos[0], pos[1]) = Vec3b(floor(angle), 255, 255);
		}
	});

	// Draw hue guide at the first row of the image
	//for (int x = 0 ; x <= maxHue ; x++) imgHeat.at<Vec3b>(0, x) = Vec3b(x, 255, 255);

	cv::cvtColor(imgHeat, imgHeat, COLOR_HSV2BGR);
}

void AngleMatrix::blurAngleMatrix(const Mat &imgCounts) {
	Mat result = imgAngle.clone();
	std::mutex mtx;
	imgAngle.forEach<float>([&result, &mtx, &imgCounts, this](float &angle, const int* pos) -> void {
		if (angle >= 0) {
			lock_guard<mutex> guard(mtx);
			uchar countSimilar = 0, countDifferent = 0;
			float nhood[10], similarAngles[11], differentAngles[10];

			// get 8 immediate neighbors
			px::get8Neighbors(imgAngle, pos[1], pos[0], nhood);

			// gather non-empty neighbors that have a minimum of connected pixels
			for (int i = 0 ; i < 8 ; i++) {
				if (nhood[i] >= 0) {
					//if (nhoodCert[i] >= 0.5) {
						if (geo::lineDirectionDiference(angle, nhood[i]) < T_MAX_DIFF_ANGLES) {
							similarAngles[countSimilar++] = nhood[i];
						} else {
							differentAngles[countDifferent++] = nhood[i];
						}
					//}
				}
			}
			//debug::log("(%d, %d) %d similars, %d different\n", pos[1], pos[0], countSimilar, countDifferent);

			// if most neighbors are similar, average them with the pixel
			float blurAngle;
			/*if (imgCertainty.at<float>(pos) <= 0.8) {
				if (countDifferent > 0) {
					blurAngle = this->averageAngleDirection(differentAngles, countDifferent);
				} else if (countSimilar > 0) {
					blurAngle = this->averageAngleDirection(similarAngles, countSimilar);
				}
			}
			else*/

			if (countSimilar > countDifferent) {
				//similarAngles[countSimilar] = angle;
				blurAngle = this->averageAngleDirection(similarAngles, countSimilar);
				result.at<float>(pos) = blurAngle;
				//debug::log("(%d, %d) blurring %d similars = %.1f.\n", pos[1], pos[0], countSimilar, blurAngle);
			}
			else if (countDifferent > countSimilar) {
				blurAngle = this->averageAngleDirection(differentAngles, countDifferent);
				result.at<float>(pos) = blurAngle;
				//debug::log("(%d, %d) blurring %d differents = %.1f.\n", pos[1], pos[0], countDifferent, blurAngle);
			}
			else {
				//debug::log("(%d, %d) doing nothing.\n", pos[1], pos[0]);
			}
		}
	});

	imgAngle = result.clone();
	result.release();
}

/**
 * @brief Averages line angles (degrees) considering only their direction (e.g. 0 and 180 are equal for direction purposes)
 * @param angles array of degree angles
 * @param count number of angles to evaluate from the angles array; the rest are never considered
 * @return positive [0, 180] average angle in degrees
 */
float AngleMatrix::averageAngleDirection(float angles[], int count) {
	float first, d1, d2, angle, avg;
	first = angles[0];
	avg = first;
	//printf("Averaging: %.1f", first);
	if (count == 1) return first;

	// accumulate in avg the closest direction values considering the first angle as reference
	for (int i = 1 ; i < count ; i++) {
		if (angles[i] == 0.f || angles[i] == 180.f) {
			angle = (first < 90.f) ? 0.f : 180.f;
		}
		else {
			d1 = geo::angleDifferenceDeg(first, angles[i]);
			d2 = geo::angleDifferenceDeg(first, 180 + angles[i]);
			angle = (d1 <= d2) ? angles[i] : 180 - angles[i];
		}
		avg += angle;
		//printf(", %.1f", angle);
	}
	//printf(" -> sum: %.1f", avg);
	avg /= float(count);
	if (avg < 0.f) {
		avg += 180.f;
	} else if (avg > 180.f) {
		avg -= 180.f;
	}
	//printf(", +avg: %.1f\n", avg);
	return avg;
}

// averages the neighbors in positions where similar is equal to flag
float AngleMatrix::averageNeighbors(float centerAngle, float neighbors[], uchar similar[], uchar flag) {
	float d1, d2, average = 0.0f, closestAngle;
	int countFlagged = 0;

	// only process flagged neighbors
	for (int i = 0 ; i < 8 ; i++) {
		if (similar[i] == flag) {
			// neighbor is flagged. Obtain the closest angle for this neighbor
			d1 = geo::angleDifferenceDeg(centerAngle, neighbors[i]);
			d2 = geo::angleDifferenceDeg(centerAngle, neighbors[i] + 180.f);
			closestAngle = (d1 < d2) ? neighbors[i] : neighbors[i] + 180.f;

			// represent the closest angle in a way that works for averaging
			if (abs(centerAngle - closestAngle) > 180) closestAngle -= 360.f;
			average += closestAngle;
			countFlagged++;
		}
	}

	average /= countFlagged;

	// We also average with the center if neighbors are similar
	if (flag == 1) {
		// make sure the centerAngle has the same sign as majority of similar neighbors

		d1 = geo::angleDifferenceDeg(centerAngle, average);
		d2 = geo::angleDifferenceDeg(centerAngle, average + 180.f);
		closestAngle = (d1 < d2) ? average : average + 180.f;
		if (abs(centerAngle - closestAngle) > 180) closestAngle -= 360.f;
		average = (average + centerAngle) / 2.0f;
	}

	// print debug info
	/*std::stringstream ss;
	ss << "blurred " << geo::toDegrees(centerAngle) << " {";
	for (int i = 0 ; i < 8 ; i++) {
		if (neighbors[i] < 700.0f) ss << geo::toDegrees(neighbors[i]) << " ";
	}
	ss << "} [";
	for (int i = 0 ; i < 8 ; i++) {
		if (neighbors[i] < 700.0f) ss << ((similar[i] == 1) ? "S " : "D ");
	}
	ss << "] to " << geo::toDegrees(average);
	ss << " countFlagged: " << countFlagged;
	printf("%s", ss.str().c_str());*/

	if (average < 0.f) average += 180.f;
	return average;
}

// returns in hist the histogram of the angle matrix
void AngleMatrix::histogram(Mat &hist, int numBins) {
	int histSize[] = {numBins};
	float range[] = { -M_PI_2, M_PI_2 };
	const float* ranges[] = { range };
	int channels[] = {0};

	calcHist( &(this->imgAngle), 1, channels, Mat(),
			  hist, 1, histSize, ranges, true, false );
}

// returns an ordered map of angles and their count from a histogram of numBins
void AngleMatrix::orderedHistogram(vector<pair<float, int>> angleCounts, int numBins) {
	// get histogram
	Mat matHist;
	histogram(matHist, numBins);

	// create a map with the histogram
	float count;
	for (int i = 0 ; i < numBins ; i++) {
		count = matHist.at<float>(i, 0);
		angleCounts.push_back(pair<float,int>(i, count));
	}

	sort(angleCounts.begin(), angleCounts.end(), [=](pair<float, int>& a, pair<float, int>& b) {
		return a.second < b.second;
	});
	// print what we have
	for (pair<float, int> it : angleCounts) {
		printf("%.2f\t%d", it.first - 90, it.second);
	}
}

// Returns an image with arrows for pixel angles
// Image will be scaled by factor "scale"
Mat AngleMatrix::arrowMap(float scale) {
	Mat imgArrows = img->clone();
	cv::cvtColor(imgArrows, imgArrows, COLOR_GRAY2BGR);

	int f = scale;
	cv::resize(imgArrows, imgArrows, Size(), 4, 4, cv::INTER_NEAREST);
	bool advance = false;
	for (int y = 0 ; y <= imgArrows.rows ; y++) {
		for (int x = 0 ; x <= imgArrows.cols ; x++) {
			float a = imgAngle.at<float>(y, x);
			if (a < 2*M_PI) {
				px::drawDirection(imgArrows, 4*x, 4*y, a);
				(fabs(a) < M_PI / 8) ? x += 5 : x += 1;
				advance = true;
			}
		}
		if (advance) y += 1;
	}
	return imgArrows;
}

/**
 * @brief Compares imgAngle with the provided ground-truth image
 * @param imgGT CV_8U with angle per pixel (int degrees); empty pixels can have any value (e.g. "0") as they are not scanned
 * @param out Mat with a normalized representation of the difference with GT
 * @return average error percentage [0.0, 1.0]
 */
unsigned long int AngleMatrix::compareWithGt(string gtPath, unsigned long int &accumulatedError, double &avgError) {
	unsigned long int pixelCount = 0;
	float d, d1, d2;

	Mat imgGT = imread(gtPath, IMREAD_GRAYSCALE);
	if (imgGT.empty()) {
		printf("FATAL: GT image not loaded\n");
		exit(1);
	}
	Mat out = Mat(imgGT.rows, imgGT.cols, CV_8U, Scalar(0));

	accumulatedError = 0;
	std::mutex mtx;
	imgAngle.forEach<float>([&imgGT, &out, &accumulatedError, &pixelCount, &d, &d1, &d2, &mtx](float &angle, const int* pos) -> void {
		if (angle >= 0) {
			lock_guard<mutex> guard(mtx);

			float angleGt = imgGT.at<uchar>(pos);
			d1 = geo::angleDifferenceDeg(angle, angleGt);
			d2 = geo::angleDifferenceDeg(angle, angleGt + 180);
			d = std::min<float>(d1, d2);
			//if (d > 10) printf("(%d, %d) gt=%f, got %f, error %f degrees\n", pos[1], pos[0], angleGt, angle, d);

			out.at<uchar>(pos) = uchar(d);
			accumulatedError += d;
			pixelCount++;
		}
	});

	// Display numerical error
	avgError = accumulatedError / double(pixelCount);

	// Display error areas
	Mat imgDiffColor;
	normalize(out, out, 255, 0, NORM_MINMAX);
	cv::cvtColor(255 * (*img), imgDiffColor, COLOR_GRAY2BGR);

	out.forEach<uchar>([&imgDiffColor, &mtx](uchar &val, const int* pos) -> void {
		if (val > 0) {
			lock_guard<mutex> guard(mtx);
			imgDiffColor.at<Vec3b>(pos) = Vec3b(255-val, 255-val, 255);
		}
	});
	imwrite("error.png", imgDiffColor);

	//disp::show(imgDiffColor, "AngleMatrix GT Error");
	return pixelCount;
}


// FAILED
void AngleMatrix::detectCurves(Mat &img) {
	img = Mat(imgTransform->rows, imgTransform->cols, CV_8U, Scalar_<uchar>(0));
	int maxCol =  img.cols - 2;
	int maxRow =  img.rows - 2;

	// for every pixel we read the neighbor change
	std::mutex mtx;
	imgTransform->forEach<Vec4b>([this, &img, maxCol, maxRow, &mtx](Vec4b &pixel, const int pos[]) -> void {
		int x = pos[1], y = pos[0];
		if (x <= 1 || y <= 1 || x >= maxCol || y >= maxRow) return;

		if (pixel[0] > 0) {
			lock_guard<mutex> guard(mtx);
			float dif = compareCounts(pixel, this->imgTransform->at<Vec4b>(y - 1, x));
			dif += compareCounts(pixel, this->imgTransform->at<Vec4b>(y + 1, x));
			dif += compareCounts(pixel, this->imgTransform->at<Vec4b>(y, x - 1));
			dif += compareCounts(pixel, this->imgTransform->at<Vec4b>(y, x + 1));
			img.at<uchar>(pos) = (uchar)ceil(dif);
		}
	});

	disp::showColormap(img, "changes", "curves.png");
	/*Mat imgNorm;
	cv::normalize(img, imgNorm, 0, 255, NORM_MINMAX, CV_8U);
	imwrite("curves.png", imgNorm);*/
}

// compares v0 and v1 pixels of imgTransform, returns a difference measure
float AngleMatrix::compareCounts(Vec4b &v0, Vec4b &v1) {
	if (v0[0] == 0 || v1[0] == 0) return 1;

	uchar n0, n1;
	if (v0[0] <= v0[1] && v0[0] <= v0[2] && v0[0] <= v0[3]) n0 = 0;
	else if (v0[1] <= v0[0] && v0[1] <= v0[2] && v0[1] <= v0[3]) n0 = 1;
	else if (v0[2] <= v0[0] && v0[2] <= v0[1] && v0[2] <= v0[3]) n0 = 2;
	else n0 = 3;
	if (v1[0] <= v1[1] && v1[0] <= v1[2] && v1[0] <= v1[3]) n1 = 0;
	else if (v1[1] <= v1[0] && v1[1] <= v1[2] && v1[1] <= v1[3]) n1 = 1;
	else if (v1[2] <= v1[0] && v1[2] <= v1[1] && v1[2] <= v1[3]) n1 = 2;
	else n1 = 3;

	float dif = (n0 == n1) ? 1 : 0;

	if (dif > 0) {
		printf("comparing [%d %d %d %d] with [%d %d %d %d] got %.3f\n", v0[0], v0[1], v0[2], v0[3], v1[0], v1[1], v1[2], v1[3], dif);
	}
	return dif;
}


// draws a grid of lines at different angles for testing purposes
// outputs 2 images: an image of black lines on white, and an image of degree angle values in black background (angle 0 is the same as bg)
void AngleMatrix::drawLineAngleGrid(Mat &imgLines, Mat &imgGT, float length, float thickness, int paddingX, int paddingY, int minAngle, int maxAngle) {
	// set the right grid size
	int numLines = maxAngle - minAngle + 1;
	int numCols = (int)floor(sqrt(numLines));
	int numRows = numCols;
	if (numCols * numRows < numLines) numRows++;

	float radius = length / 2;
	int t = int(thickness); // thickness
	int celW2 = int(radius + paddingX), celH2 = int(radius + paddingY);
	int xC, yC, angleDegrees = minAngle;
	float angleRadians; // angle in radians

	imgGT = Mat(2 * celH2 * numRows, 2 * celW2 * numCols, CV_8U, Scalar(255));

	for (int row = 0 ; row < numRows ; row++) {
		yC = (row * 2 * celH2) + celH2;
		for (int col = 0 ; col < numCols ; col++) {
			xC = (col * 2 * celW2) + celW2;
			angleRadians = geo::toRadians(angleDegrees);

			px::drawLine(imgGT,
					 Point2f(xC + radius*cos(angleRadians), yC - radius*sin(angleRadians)),
					 Point2f(xC - radius*cos(angleRadians), yC + radius*sin(angleRadians)),
					 thickness, (uchar)angleDegrees);
			//cv::putText(im, std::to_string(angle), Point2f(xC+2, yC+10), cv::FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0));
			angleDegrees++;
			if (angleDegrees > maxAngle) break;
		}
	}

	threshold(imgGT, imgLines, 254, 255, cv::THRESH_BINARY);
}

/**
 * Generates and shows the Angle Hue Map
 */
void AngleMatrix::showAngleHueMap() {
	Mat imgMap;
	angleHueMap(imgMap);
	disp::show(imgMap, "Angle Hue Map");
	imwrite("angle-matrix.png", imgMap);
}

/**
 * @brief Returns in the out Mat the pixels greater than angleMin and less than angleMax (inclusive)
 * @param out CV_8U {0, 1} Mat, gets initialized to CV_8U
 * @param angleMin Radians min interval
 * @param angleMax Radians max interval
 */
void AngleMatrix::pixelsForAngleInterval(Mat &out, float angleMin, float angleMax, float intervalTolerance) {
	std::mutex mtx;
	out = Mat(img->size(), CV_8U, Scalar(0));
	angleMax += intervalTolerance;
	angleMin -= intervalTolerance;

	imgAngle.forEach<float>([&out, &mtx, angleMin, angleMax](float &a, const int *pos) -> void {
		if (a > 0) {
			if ((angleMin < angleMax) && (a >= angleMin) && (a <= angleMax)) {
				lock_guard<mutex> guard(mtx);
				out.at<uchar>(pos) = 1;
			}
			else if ((angleMin > angleMax) && (a >= angleMin || a <= angleMax)) {
				lock_guard<mutex> guard(mtx);
				out.at<uchar>(pos) = 1;
			}
		}
	});
}
