#ifndef LINETHICKNESS_H
#define LINETHICKNESS_H

#include <iostream>
#include <opencv2/core.hpp>
#include <slicetransform.h>
#include "util/plothist.h"

using namespace cv;
using std::cout;
using std::endl;

// Divides image per line thickness
class LineThickness {
	const float MIN_THICKNESS_COUNT = 0.03; // min percentage of the most common a line thickness count needs to not get ignoresd
	Mat *img; // BW binary {0, 1} input image

public:
	Mat imgTh; // Thickness CV_8U Mat
	Mat img1px; // 1px thick lines are moved here
	Mat counts; // 256 x 1 CV_16U Vector Mat with pixel count per line thickness
	uchar maxCountThickness; // most popular line thickness
	ushort maxCount; // most popular line thickness' count

	LineThickness(Mat *imgBW);
	void removeOnePixelLines();
	void getThickness(Mat *imgTx);
	void blurThickness();
	void filterByLineThickness(Mat &dest);
};

#endif // LINETHICKNESS_H
