#include "linethickness.h"

LineThickness::LineThickness(Mat *imgBW) {
	this->img = imgBW;
}


// gathers line thickness from origin into dest

// 1 px thick lines must have been removed before using removeOnePixelLines()
// origin must be a CV_8U mat with black background
void LineThickness::getThickness(Mat *imgTx) {
	std::mutex mtx;
	imgTh = Mat(img->size(), CV_8U, Scalar(0));

	// gather line thickness
	imgTx->forEach<Vec4b>([&mtx, this](Vec4b &p, const int pos[]) -> void {
		if (p[0] > 0) {
			uchar th = 0; // 0 is not set yet
			uchar h = p[0], v = p[1], d = p[2], e = p[3];

			// RULE: detect common 1px cases
			if (h == 1 || v == 1 || d == 1 || e == 1) {
				// probably a corner or edge; ignore the 1px slice
				uchar ps[4] = { h, v, d, e };
				std::sort(begin(ps), end(ps));
				th = (uchar)round((float(ps[1]) + float(ps[2])) / 2.f);
			}
			else {
				// get ordered slice array
				uchar ps[4] = { h, v, d, e };
				std::sort(begin(ps), end(ps));

				// If the first 2 slices are equal, that's the thickness
				if (ps[0] == ps[1]) th = ps[0];
				// If the first slice < 0.5 the second slice (crosspoint), tfirst slice is the thickness
				else if (ps[0] > 1 && (2 * ps[0] < ps[1])) th = ps[0];
				// Default: round first 2 slices
				else th = (uchar)round((float(ps[0]) + float(ps[1])) / 2.0);
			}

			lock_guard<mutex> guard(mtx);
			this->imgTh.at<uchar>(pos) = th;
		}
	});

	// blur to stabilize measurements
	blurThickness();
}


// Blurs the thickness values in imgTh based in rules
// Also gathers a histogram of thickness in counts
void LineThickness::blurThickness() {
	Mat old = imgTh.clone();
	counts = Mat(256, 1, CV_16U, Scalar(0));

	std::mutex mtx;
	imgTh.forEach<uchar>([&mtx, &old, this](uchar &th, const int* pos) -> void {
		int x = pos[1], y = pos[0];
		if (th > 1) {
			float sum = 0.0;
			bool equal = true; // true if all non-empty neighs are equal valued
			uchar equalValue; // if equal, all neighs are equal to this value
			uchar votes = 0, jumps;
			uchar neighs[9];
			px::get8Neighbors(old, x, y, neighs);

			// get votes and jumps from neighbors
			for (int i = 0 ; i < 8 ; i++) {
				if (neighs[i] > 0) {
					sum += neighs[i];
					votes++;

					// check for neighborhood equality
					if (equal) {
						if (votes == 1) equalValue = neighs[i];
						else {
							if (equalValue != neighs[i]) {
								equal = false;
							}
						}
					}
				}
			}
			jumps = px::nhood8Jumps(neighs, 0);
			uchar newTh = th;
			if (votes > 1) {
				// do nothing if pixel and neighs are equal
				if (equal && th == equalValue) {
					// skip
				}
				// for corners, we discard and decide from the neighbors
				else if (votes == 3 && jumps == 2) {
					newTh = uchar(round(sum / float(votes)));
				}
				// for borders, we discard and replace if all neighbors are equal
				else if (equal && jumps == 2) {
					newTh = equalValue;
				}
				// default: replace with neighborhood average
				else if (votes >= 3) {
					uchar avg = uchar(round(sum / float(votes)));
					if (th != avg) newTh = avg;
				}
			}

			// store and accumulate into counts histogram
			lock_guard<mutex> guard(mtx);
			if (th != newTh) th = newTh;
			this->counts.at<ushort>(th)++;
		}
	});
}

// Returns a binary {0, 1} CV_8U with the pixels that have thickness values not un-popular
// Thresholded against a percentge of the max Thickness count found
// Call after process()
void LineThickness::filterByLineThickness(Mat &dest) {
	// find most popular line thickness
	double maxVal;
	int maxPos;
	cv::minMaxIdx(counts, nullptr, &maxVal, nullptr, &maxPos);
	this->maxCount = (ushort)maxVal;
	this->maxCountThickness = (uchar)maxPos;
	ushort minCount = MIN_THICKNESS_COUNT * float(this->maxCount);

	// Return a Mat with all pixels that meet the threshold
	dest = Mat(img->rows, img->cols, CV_8U, Scalar(0));
	std::mutex mtx;
	imgTh.forEach<uchar>([&mtx, &dest, minCount, this](uchar &thickness, const int* pos) -> void {
		if (thickness > 1) {
			ushort count = this->counts.at<ushort>(thickness);

			if (thickness <= (2 * this->maxCountThickness) || count >= minCount) {
				dest.at<uchar>(pos) = 1;
			}
		}
	});
}


/**
 * Moves all 1 pixel lines from img to local img1px binary image
 * Uses the greenlee matrix to apply manual code rules
 */
void LineThickness::removeOnePixelLines() {
	Mat imgGreen;
	green::encodeMatrixForeground(*img, imgGreen, 0); // Get greenlee encoded matrix
	img1px = Mat(img->size(), CV_8U, Scalar(0));

	std::mutex mtx;
	img->forEach<uchar>([&mtx, &imgGreen, this](uchar &px, const int* pos) -> void {
		if (px != 0) {
			uchar gl = imgGreen.at<uchar>(pos);
			if (green::is1pxLine(gl)) {
				lock_guard<mutex> guard(mtx);
				this->img->at<uchar>(pos) = 0;
				this->img1px.at<uchar>(pos) = 1;
			}
		}
	});
}
