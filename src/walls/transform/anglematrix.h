#ifndef ANGLEMATRIX_H
#define ANGLEMATRIX_H

#include <mutex>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include <sstream>
#include <utility>
#include <set>
#include <vector>
#include "util/px.h"
#include "util/greenlee.h"
#include "util/geometry.h"
#include "util/debug.h"

using namespace cv;
using namespace std;

class AngleMatrix {
	Mat* img; // original B/ W image
	Mat* imgTransform; // CV_8UC4 4-ch slice scanSlices

	float T_MAX_DIFF_ANGLES = 20; // maximum angle difference (degrees) to consider angles similar
	float almostVertical = 0.17; // 10 degrees

	float compareCounts(Vec4b &v0, Vec4b &v1);

public:
	uchar blurLevel = 0;
	Mat imgNeighs; // neighbor count per pixel
	Mat imgAngle; // Deg angle matrix. -1 is an empty pixel

	AngleMatrix() = default;
	AngleMatrix(Mat *img, Mat *imgTransform);

	void extractAngles();
	float angleFromDiagonals(int x, int y, int cx, int cy, int cd1, int cd2);
	void angleHueMap(Mat &imgHeat);
	void showAngleHueMap();
	void blurAngleMatrix(const Mat &imgCounts);
	float averageAngleDirection(float angles[], int count);
	float averageNeighbors(float centerAngle, float neighbors[], uchar similar[], uchar flag);
	void histogram(Mat &hist, int numBins);
	void orderedHistogram(vector<pair<float, int>> angleCounts, int numBins);
	Mat arrowMap(float scale);
	void pixelsForAngleInterval(Mat &out, float angleMin, float angleMax, float intervalTolerance);

	static void drawLineAngleGrid(Mat &imgLines, Mat &imgGT, float length, float thickness, int paddingX, int paddingY, int minAngle, int maxAngle);
	unsigned long int compareWithGt(string gtPath, unsigned long int &accumulatedError, double &avgError);
	void detectCurves(Mat &out);

};

#endif // ANGLEMATRIX_H
