#include <cstdio>
#include <iostream>
#include <string>

#include <preprocessing/textseparation.h>
#include <walls/transform/slicetransform.h>
#include <walls/transform/anglematrix.h>
#include <walls/transform/linethickness.h>
#include <walls/geometry/extractor.h>
#include <walls/geometry/groupmanager.h>
#include <tests/imgdebug.h>

#include "../external/cxxopts.hpp"
#include <util/profiler.h>
#include <util/debug.h>
#include <util/cleaning.h>
#include <util/segment.h>
#include <preprocessing/Binarizer.h>


using namespace std;

// Draw debug sub-pixel precision information
void saveDebugImage(GroupManager &mgr, Mat &imgInitial, string outPath) {
	printf("Saving debug image...\n");
	Mat imgSlices, imgBg, img255 = 30 * imgInitial;
	cvtColor(img255, imgBg, cv::COLOR_GRAY2BGR);

	// paint slices
	mgr.paintColorGroups(imgSlices, mgr.groups);
	px::shinyColormapImage(imgSlices, imgBg, 0);

	uchar n = 1;
	Vec3b color;
	Vec3b arrowColor(100, 80, 255);
	Vec3b colinearColor(255, 200, 0);
	Vec3b parallelLineColor(0, 255, 0);
	Vec3b connectionEndpointColor(255,255, 0);
	Vec3b connectionBodyColor(255,155, 0);

	bool drawParallelRelations = true; // draw info on parallel relationships
	bool drawEndpointRelations = true; // draw endpoint relationships


	ImgDebug imd = ImgDebug(&imgBg, 3);
	Point2f p0, p1, center;
	string txt;
	printf("Printing %lu groups\n", mgr.groups.size());
	for (SliceGroup &g : mgr.groups) {
		if (!g.enabled) continue;
		if (!g.parallels.empty()) color = parallelLineColor;
		else if (g.connectedWall) color = Vec3b(50,200,255);
		else color = Vec3b(255,255,255);

		//if (g.closedLoop) color = Vec3b(0,0,255);
		//if (g.parallel) {
			//if (g.connectedByAnotherGroupTo(g.parallelGroup)) color = Vec3b(0,0,255);
		//}
			//if (g.type == 9) color = Vec3b(255,0,255);
		//else if (g.parallelSeriesConnected) color = Vec3b(255,0,255);


		//color = px::shinyColor(n++);
		p0 = g.seg.p0;
		p1 = g.seg.p1;

		g.getPresentationEndpoints(p0, p1);
		imd.drawLine(p0, p1, color);

		//imd.drawArrow(p0, p1, color);


		// Draw parallel arrow relationships
		if (drawParallelRelations && !g.parallels.empty()) {
			Point2f a0, a1;
			for (ParallelRelation &r : g.parallels) {
				a0 = r.other->seg.getCenter();
				a1 = a0 - r.direction * r.distance;
				imd.drawLine(a0, a1, arrowColor);
				imd.drawCircle(a0, 1.0, parallelLineColor);
				imd.drawCircle(a1, 1.5, arrowColor);
				//txt = to_string((int)g.parallels.size());
				//imd.drawText(g.seg.getCenter(), txt, 0.6f, Vec3b(250,250,255));
			}
		}


		// Draw endpoint connections
		if (drawEndpointRelations) {
			int np0 = (int)g.connP0.size(), np1 = (int)g.connP1.size();
			Vec3b colorConn;

			if (np0 > 0) {
				colorConn = (g.connP0.front().toEndpoint) ? connectionEndpointColor : connectionBodyColor;
				imd.drawCircle(g.seg.p0, 1.0, colorConn);
				imd.drawText(g.seg.p0, to_string(np0), 0.2f, colorConn);
			}
			if (np1 > 0) {
				colorConn = (g.connP1.front().toEndpoint) ? connectionEndpointColor : connectionBodyColor;
				imd.drawCircle(g.seg.p1, 1.0, colorConn);
				imd.drawText(g.seg.p1, to_string(np1), 0.2f, colorConn);
			}
		}


		// Write number of crosses
		//txt = to_string((int)g.connectedGroups.size());
		//imd.drawText(g.seg.getCenter(), txt, 0.6f, Vec3b(250,250,255));

		// Draw virtual colinears
		if (g.minColinearDistP0 > 0) imd.drawLine(g.seg.p0, g.colinearP0, colinearColor);
		if (g.minColinearDistP1 > 0) imd.drawLine(g.seg.p1, g.colinearP1, colinearColor);

		// Draw normals
		//center = g.seg.getCenter();
		//imd.drawArrow(center, g.normal, 15, arrowColor);

	}
	disp::show(imd.imgDebug, "Debug");
	imd.save(outPath + "segments.png");
}

// extracts the line segments for lineDirection and stores them in the GroupManager
void extractSegmentsForDirection(GroupManager &mgr, SliceTransform &st, AngleMatrix &am, Mat &imgInitial, Mat &imgExtrusion, uchar lineDirection) {
	string chanText[4] = {"horizontal", "vertical", "diagonal 1", "diagonal 2"};
	printf("SliceTranform extracting line segments on the %s channel...\n", chanText[lineDirection].c_str());
	float tolerance = 1;

	// Get pixels for general line direction
	Mat imgCh;
	switch (lineDirection) {
		case DIR_HORIZONTAL:    am.pixelsForAngleInterval(imgCh, 157.5, 22.5,  tolerance); break;
		case DIR_VERTICAL:      am.pixelsForAngleInterval(imgCh, 67.5,  112.5, tolerance); break;
		case DIR_DIAGONAL_INCR: am.pixelsForAngleInterval(imgCh, 22.5,  67.5,  tolerance); break;
		case DIR_DIAGONAL_DECR: am.pixelsForAngleInterval(imgCh, 112.5, 157.5, tolerance); break;
	}

	// remove isolated pixels and 1px lines
	std::mutex mtx;
	Mat imgGreen;
	green::encodeMatrixForeground(imgCh, imgGreen, 0); // Get greenlee encoded matrix
	imgCh.forEach<uchar>([&mtx, &imgGreen](uchar &px, const int* pos) -> void {
		if (px != 0) {
			if (green::is1pxLine(imgGreen.at<uchar>(pos))) {
				lock_guard<mutex> guard(mtx);
				px = 0;
			}
		}
	});

	//disp::showColormap(imgCh, string("Pixels for direction ") + to_string(lineDirection), "channel-" + to_string(lineDirection) + ".png");
	mgr.extrudeChannel(imgCh, imgExtrusion, lineDirection, 3);
	//mgr.extrudeChannel(imgCh, lineDirection, 3);
	disp::showColormap(imgCh, string("Extruded for direction ") + to_string(lineDirection), "channel-e" + to_string(lineDirection) + ".png");

	// Extract and store slice groups
	Extractor ex = Extractor(&imgCh, &imgInitial);
	ex.runFullExtraction(lineDirection, &imgExtrusion);
	//mgr.mergeAllCloseColinearGroups(ex.groups);
	mgr.addGroupsVector(ex.groups);
}


int main(int argc, const char *argv[]) {
	/*Mat img = Mat(300, 300, CV_8U, Scalar(0));
	WallPainter::drawLine(img, Point2f(10,10), Point2f(10, 100), 3, 1);
	disp::showColormap(img, "Line", "line.png");
	waitKey(0);
	exit(0);*/
	Profiler profiler;
	profiler.checkpoint();

	printf("** Performing wall extraction **\n\n");
	//test::drawLineAngleGrid(50, 3, 5, 5, 27, 30);

	cxxopts::Options options("WallExtraction", "A command line computer vision program for extracting walls in floor plan images.");
	options.add_options()
			("img", "Input image path", cxxopts::value<string>())
			("out", "Output files directory path", cxxopts::value<string>()->default_value("assets/"))
			("binarize", "Binarize input image", cxxopts::value<bool>())

			("bt", "Binarization threshold", cxxopts::value<int>()->default_value("20"))
			("cet", "Character elongation threshold", cxxopts::value<int>()->default_value("20"))
			("cat", "Character area threshold factor", cxxopts::value<int>()->default_value("5"))
			("tmin", "Minimum line thickness", cxxopts::value<int>()->default_value("2"))
			("tmax", "Maximum line thickness", cxxopts::value<int>()->default_value("60"))
			;
	auto result = options.parse(argc, argv);
	if (result.count("img") == 0) {
		printf("Please specify the filename of the image with --img to run this program.\n");
		exit(1);
	}

	// load image as a binary {0, 1} matrix
	// real-section-diag real-section-diag2 real-section-diag3 real-section-rect real-section-rect2 test-4 connected-test
	string outPath = result["out"].as<string>();
	bool binarizeInput = (result.count("binarize") > 0);

	//string inputPath = result["img"].as<string>();
	//string inputFilename = "10.png";
	string inputPath =
	//"../test-images/learning/Triumph10/img/" + inputFilename;
	//"../test-images/real-section-diag.png";
	//"../test-images/lines.png";
	"../test-images/boxes.png";
	//"../test-images/test-4.png";
	//"../test-images/real/railyards1.png";
	//"../test-images/real/hope.png";
	//"../test-images/real/cottage.png";
	//"../test-images/real/dakova.png";
	

	// * Binarize to {0,1} if requested
	Binarizer binarizer(inputPath, 20);
	Mat imgInitial, imgPrepro;
	Point p0, p1;
	if (binarizeInput) {
		printf("Binarizing input on request...\n");
		if (!binarizer.load()) {
			printf("Couldn't load image '%s'.", inputPath.c_str());
			exit(1);
		}
		binarizer.getBlackPixels(imgInitial);
		imwrite(outPath + "wall-input.png", imgInitial); // Save for rooms segmentation
	}
	else {
		printf("Loading image from Matlab...\n");
		imgInitial = imread(inputPath, cv::IMREAD_GRAYSCALE);
	}
	px::addBorder(imgInitial, 0);

	//disp::showColormap(imgInitial, "Initial");

	// * Text/Graphics separation
	TextSeparation textSep;
	textSep.removeTextFromImage(imgInitial, imgPrepro);
	//disp::showColormap(textSep.imgText, "Text");
	//imgPrepro = imgInitial;

	// * Remove 1px thick lines
	LineThickness lt = LineThickness(&imgPrepro);
	lt.removeOnePixelLines();
	//disp::showOverlapped<uchar>(imgPrepro, lt.img1px, 0, "Preprocessed2", "pixel-lines.png");

	// * Remove dense square-like blobs
	clean::cleanBlobs<uchar>(imgPrepro, imgPrepro, 8, 1, 2, 0.5f, 1.0f);

	// * Slice Transform
	SliceTransform st = SliceTransform(&imgPrepro);
	st.scanSlicesThreaded(); //st.scanSlices();
	//st.showChannels();

	// * Line thickness filter
	lt.getThickness(&st.imgTransform);
	lt.filterByLineThickness(imgPrepro);
	//disp::showColormap(lt.imgTh, "Thickness", "thickness.png");
	//disp::showNormalized(imgPrepro, "FilteredThickness");

	// * Angle Matrix
	st.scanSlices(); // update
	AngleMatrix am = AngleMatrix(&imgPrepro, &st.imgTransform);
	am.extractAngles();
	//st.am.compareWithGt("../test-images/lines-gt.png");
	am.showAngleHueMap();
	return 0;

	GroupManager mgr(&imgPrepro);
	mgr.outPath = outPath;
	//extractSegmentsForDirection(mgr, st, am, imgPrepro, imgInitial, DIR_HORIZONTAL);
	extractSegmentsForDirection(mgr, st, am, imgPrepro, imgInitial, DIR_VERTICAL);
	//extractSegmentsForDirection(mgr, st, am, imgPrepro, imgInitial, DIR_DIAGONAL_INCR);
	//extractSegmentsForDirection(mgr, st, am, imgPrepro, imgInitial, DIR_DIAGONAL_DECR);

	// *** Perform geometrical operations ***

	//mgr.detectParallels(false); // for all lines, find the closest similar length parallel line
	//mgr.filterParallelsByDistance(); // remove parallel lines unexpectedly far or close to each other
	//mgr.detectSegmentConnections(); // for all, store the line that each endpoint touches, and those crossed in the middle
	//mgr.filterIsolatedParallels(); // remove parallels with both endpoints disconnected
	//mgr.purgeGroups();

	//mgr.subdivideLines(); // subdivide parallel lines where an endpoint connects to their body
	//mgr.detectParallels(false);
	//mgr.filterParallelsByDistance();
	//mgr.detectSegmentConnections();

	//mgr.labelWallConnectedEndpoints(); // label lines connected to lines with a parallel
	/*mgr.filterOverlappedParallels(); // remove parallels (and those conected to its endpoints) that cross too many wall lines


	mgr.purgeGroups();

	mgr.labelParallelSimilarGroups(true);
	mgr.extrudeColinearEndpoints(binarizer.imgCodes); // extrude endpoints to match close colinear endpoints
	//mgr.connectCloseEndpoints(); // FAILS on dakova

	mgr.labelAllConnectedSegments();
	mgr.updateWallNormals();
	mgr.filterTextContainers(textSep.imgText);

	//mgr.updateWallLabels();
	//mgr.filterWallConnectedShapes(); // Remove unexpected shapes connected to walls FAILS ON real-section-diag

	//mgr.wallConnectFreeEndpoints(); // create new walls to connect remaining free endpoints to best candidate
	//mgr.purgeGroups(); // remove all non-wall groups from list
*/


	//mgr.writeSegmentsToFile(); // Export segments to CSV segments.txt and connections.txt
	profiler.checkpoint(true);


	saveDebugImage(mgr, imgInitial, outPath);

	// Compare with ground-truth
	/*printf("Comparing with Ground Truth...\n");
	unsigned long int pixelCount = 0, tp = 0, fp = 0, tn = 0, fn = 0;
	Mat imgConfusion = Mat(imgWalls.rows, imgWalls.cols, CV_8U, Scalar(0)); // 1=TP, 2=TN, 3=FP, 4=FN
	Mat imgGt = imread("../test-images/learning/Triumph10/gt/" + inputFilename, cv::IMREAD_GRAYSCALE);
	cv::threshold(imgGt, imgGt, 200, 1, CV_THRESH_BINARY);

	std::mutex mtx;
	imgWalls.forEach<uchar>([&imgGt, &imgConfusion, &tp, &tn, &fp, &fn, &mtx](uchar &val, const int* pos) -> void {
		lock_guard<mutex> guard(mtx);
		uchar gt = imgGt.at<uchar>(pos);
		if (val == 1) { // P
			if (gt == val) { // T
				imgConfusion.at<uchar>(pos) = 1; // TP green
				tp++;
			} else { //F
				imgConfusion.at<uchar>(pos) = 6; // FP cyan
				fp++;
			}
		} else { // N
			if (gt == val) { // T
				//imgConfusion.at<uchar>(pos) = 0; // TN black
				tn++;
			} else { //F
				imgConfusion.at<uchar>(pos) = 19; // FN red
				fn++;
			}
		}
	});
	printf("Confusion Matrix: %lu\t%lu\t%lu\t%lu\n", tp, fp, tn, fn);
	printf("Precision: %.2f, Recall: %.2f\n", float(tp) / float(tp+fp), float(tp) / float(tp + fn));
	printf("Image Dimensions: %d\t%d\n", imgWalls.cols, imgWalls.rows);
	//for (uchar i = 0 ; i < 20 ; i++) imgConfusion.at<uchar>(0,i) = i;
	disp::showColormap(imgConfusion, "Confusion", "../test-images/learning/Triumph10/confusion/" + inputFilename);*/

	waitKey();
	printf("Process finished.\n");
}
