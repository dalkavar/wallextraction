#ifndef PARALLELRELATION_H
#define PARALLELRELATION_H

#include "slicegroup.h"

class SliceGroup;

/**
 * Stores a relationship from this SliceGroup to another parallel SliceGroup
 */
class ParallelRelation {
public:
	SliceGroup *other;
	float distance = 0; // Parallel distance
	float overlap = 0; // Length of overlap
	Point2f direction; // Unit normal vector pointing to the other group

	ParallelRelation(SliceGroup *other, float distance, float overlap, Point2f direction) {
		this->other = other;
		this->distance = distance;
		this->overlap = overlap;
		this->direction = direction;
	}
};

#endif //PARALLELRELATION_H
