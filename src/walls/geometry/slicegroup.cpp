#include "slicegroup.h"

// Constructor populates slicegroup with connected slices found at seed (x,y)
// Channel is used to set the projection direction
SliceGroup::SliceGroup(Mat &img, Mat &imgExtrude, int x, int y, uchar sliceType, uchar chDx, uchar chDy) {
	//debug::log(0, "\nCreating SliceGroup at (%d, %d)\n\n", x, y);
	this->sliceType = sliceType;
	this->chDx = chDx;
	this->chDy = chDy;

	// read first slice
	Slice seedSlice = Slice(img, x, y, sliceType);
	if (seedSlice.length == 1) {
		//printf("Seed slice is a pixel, re-scanning ");
		//seedSlice.print();
		seedSlice.scanCentered(imgExtrude, seedSlice.center.x, seedSlice.center.y, 3);
		if (!slicePixelsNotInAGroup(seedSlice, img)) return;
	}

	Slice slice = seedSlice;
	addSlice(slice, MOVING_FORWARD);

	// read following slices
	//debug::log(1, "reading next slices...\n");
	scanSlicesInDirection(img, imgExtrude, seedSlice, MOVING_FORWARD);

	// read previous slices
	//debug::log(2, "reading previous slices...\n");
	scanSlicesInDirection(img, imgExtrude, seedSlice, MOVING_BACKWARD);

	unsigned long numSlices = slices.size();
	if (numSlices >= 4) {
		// get stats on slope
		vector<float> slopes;
		slopes.reserve(numSlices - 2);
		double avg, accum, stdev;

		if (sliceType == SLICE_VERTICAL) {
			for (int i = 1; i < (numSlices - 1); i++) slopes.push_back(slices[i + 1].center.y - slices[i - 1].center.y);
		} else {
			for (int i = 1; i < (numSlices - 1); i++) slopes.push_back(slices[i + 1].center.x - slices[i - 1].center.x);
		}

		/*
		// DONT filter by stdev, its an absolute measure!!
		avg = std::accumulate(slopes.begin(), slopes.end(), 0.f) / slopes.size();
		accum = 0.0;
		std::for_each(slopes.begin(), slopes.end(), [&](const float s) { accum += (s - avg) * (s - avg); });
		stdev = sqrt(accum / (slopes.size() - 1));
		printf("stdev: %.3f\n", stdev);
		if (stdev > 0.5) isLine = false;*/
	}
	else {
		isLine = false;
	}

	//debug::log(3, "Final slices:\n");
	//for (Slice &s : slices) s.print();

	// if this segment is "small" and the slices are not strongly correlated, re-scan manually
	/*if (originalSize <= T_SLICES_SMALL) {
		// Find max slice length
		int maxSliceLength = 0;
		for (Slice &s : slices) {
			if (s.length > maxSliceLength) maxSliceLength = s.length;
		}
		// Re-scan all slices to max slice length
		if (maxSliceLength > 1) {
			debug::log(4, "SliceGroup is small, re-scanning manually with slice length %d\n", maxSliceLength);
			for (Slice &s : slices) {
				s.scanCentered(imgExtrude, s.center.x, s.center.y, maxSliceLength);
			}
		}
	}*/

	if (!isLine) {
		//printf("Disabling because it's not a line...\n");
		disable();
	}

	findMainLengths();
	labelValidSlices();
	updateSegment();
}


SliceGroup::SliceGroup(Segment s) {
	seg = s;
}


// Returns in dx and dy the proper advancement steps for this segment
// forward determines if the extrusion direction is forward or backward
void SliceGroup::stepsForSegment(float &dx, float &dy, bool forward) {
	dx = 0.0, dy = 0.0;

	if (sliceType == SLICE_HORIZONTAL) {
		// horizontal slices
		dy = (seg.p0.y < seg.p1.y) ? 1.0 : -1.0;
		if (seg.p0.x != seg.p1.x) {
			dx = (1 / seg.slope);
		}
	} else {
		// vertical slices
		dx = (seg.p0.x < seg.p1.x) ? 1.0 : -1.0;
		if (seg.p0.y != seg.p1.y) {
			dy = seg.slope;
		}
	}

	if (!forward) {
		dx = -dx;
		dy = -dy;
	}
}

// Finds the true, main slice lengths of this body
void SliceGroup::findMainLengths() {
	map<uchar, int>::iterator it;

	// Guard: if group slices are the same length, store
	if (sliceCounts.size() == 1) {
		it = sliceCounts.begin();
		mainLengths.push_back(it->first);
	}
	else {
		// Find the most popular slice length
		uchar idMax;
		int maxCount = 0;
		for (it = sliceCounts.begin(); it != sliceCounts.end(); it++) {
			if (it->second > maxCount) {
				idMax = it->first;
				maxCount = it->second;
			}
			//printf("%d: %d\n", it->first, it->second);
		}
		mainLengths.push_back(idMax);

		// Find all slice lengths with a count higher than a percentage of maxCount
		for (it = sliceCounts.begin(); it != sliceCounts.end(); it++) {
			if (it->first != idMax && it->second > T_MAIN_LENGTH_PERCENT * maxCount) {
				mainLengths.push_back(it->first);
			}
		}
	}

	/*printf("Main slice lengths: ");
	for (uchar &n : mainLengths) printf("%d ", n);
	printf("\n\n");*/
}

// Finds the first and last valid slices of mainLength for a group of stable slices
// we suppose there's a maximum of 2 mainLengths
void SliceGroup::findExteriorSlices() {
	deque<Slice>::iterator it;

	// find first slice
	for (it = slices.begin() ; it != slices.end() ; it++) {
		// if slice is valid
		if(sliceLengthIsStable(*it)) {
			idxFirstValid = it - slices.begin();
			break;
		}
	}

	// find last slice
	deque<Slice>::reverse_iterator it2;
	for (it2 = slices.rbegin() ; it2 != slices.rend() ; it2++) {
		if(sliceLengthIsStable(*it2)) {
			idxLastValid = slices.size() - (it2 - slices.rbegin()) - 1;
			break;
		}
	}
}

// labels slices that are valid
void SliceGroup::labelValidSlices() {
	//printf("\n Labeling valid slices for segment with main length %d..\n", mainLengths[0]);
	for (Slice &s : slices) {
		s.stableLength = sliceLengthIsStable(s);
	}
}

// Update endpoint slices and compute segment
void SliceGroup::updateSegment() {
	findExteriorSlices();

	// Handle vertical and horizontal cases
	if ((slices[idxFirstValid].center.x == slices[idxLastValid].center.x) || (slices[idxFirstValid].center.y == slices[idxLastValid].center.y)) {
		seg = Segment(slices[idxFirstValid].center, slices[idxLastValid].center, mainLengths.at(0));
	}
	else {
		// gather center points of valid slices and do linear regression on them
		vector<Point2f> centers;
		for (Slice &s : slices) {
			if (s.stableLength) {
				centers.push_back(s.center);
				//printf("center (%.2f, %.2f)\n", s.center.x, s.center.y);
			}
		}

		if (centers.size() < 2) return; // guard against groups too small to be a line segment

		// get line segment endpoints from rect
		Point2f p0 = slices[idxFirstValid].center;
		Point2f p1 = slices[idxLastValid].center;

		// linear regression
		double m, b;
		double correlation = geo::linearRegression(centers, m, b);
		//printf("Original segment: (%.1f,%.1f)-(%.1f,%.1f) with m=%.2f, b=%.2f\n", p0.x, p0.y, p1.x, p1.y, m, b);

		isLine = (correlation >= T_MIN_CORRELATION);
		if (correlation >= T_MIN_CORRELATION) {

			// guard against perfect vertical lines
			if (m > 9999.) {
				p0.x = b;
				p1.x = b;
			}
			else if (sliceType == SLICE_HORIZONTAL) {
				p0.x = geo::xForYinRect(m, b, p0.y);
				p1.x = geo::xForYinRect(m, b, p1.y);
			} else {
				p0.y = geo::yForXinRect(m, b, p0.x);
				p1.y = geo::yForXinRect(m, b, p1.x);
			}
		}

		//printf("Segment approximated to (%.1f,%.1f)-(%.1f,%.1f) with correlation %.2f\n", p0.x, p0.y, p1.x, p1.y, correlation);
		seg = Segment(p0, p1, mainLengths.at(0));
		seg.update();
	}
}

// Returns true if the space described by the slice is not occupied by other slices in imgGroups
// imgGroups has 0 for empty pixels, 1 for non-empty unassigned pixels and 2 for pixels assigned to a slice group
bool SliceGroup::slicePixelsNotInAGroup(Slice &s, Mat &imgGroups) {
	Point p = s.start;

	for (int i = 0 ; i < s.length ; i++) {
		if (imgGroups.at<uchar>(p) == 2) return false;
		p.x += s.sdx;
		p.y += s.sdy;
	}

	return true;
}

// Removes slices whose position is outside of the line segment
// Updates slicegroup and segment
void SliceGroup::removeMisplacedSlices() {
	int i, size = slices.size();
	bool modified = false;

	// trim the start of the group
	for (i = 0 ; i < size ; i++) {
		if (seg.interceptsLine(slices[i].start, slices[i].end)) break;
	}
	if (i > 0) {
		slices.erase(slices.begin(), slices.begin() + i);
		modified = true;
	}

	// Guard against empty groups (should not happen)
	if (slices.size() == 0) {
		//printf("ERROR: All %lu slices dissapeared\n", slices.size());
		return;
	}

	// trim the end of the group
	int lastIdx = slices.size() - 1;
	for (i = lastIdx ; i >= 0 ; i--) {
		if (seg.interceptsLine(slices[i].start, slices[i].end)) break;
	}
	if (i != lastIdx) {
		slices.erase(slices.begin() + i + 1, slices.end());
		modified = true;
	}

	if (modified) updateSegment();
}

// Returns true if slice s2 appears to be connected to s1
// s1 is used as comparison reference
bool SliceGroup::slicesAppearConnected(Slice &s1, Slice &s2) {
	float intsect;
	if (s1.type == SLICE_HORIZONTAL) {
		// slices should be contiguous
		if (abs(s1.start.y - s2.start.y) != 1) return false;
		// BOTH slices' centers should be close enough
		intsect = geo::rangeIntersection(s1.start.x, s1.end.x + 1, s2.start.x, s2.end.x + 1);
		if (intsect < 0.5 * s1.length || intsect < 0.5 * s2.length) return false;
	}
	else {
		// same rules for vertical slices
		if (abs(s1.start.x - s2.start.x) != 1) return false;
		intsect = geo::rangeIntersection(s1.start.y, s1.end.y + 1, s2.start.y, s2.end.y + 1);
		if (intsect < 0.5 * s1.length || intsect < 0.5 * s2.length) return false;
	}

	return true;
}

// keeps scanning slices and advancing towards direction until no longer possible (or disconnected)
// img has 0 for bg, 1 for non-empty unassigned, 2 for assigned pixels
// imgExtrude is a {0,1} binary
// direction can be one of the class constants
void SliceGroup::scanSlicesInDirection(Mat &img, Mat &imgExtrude, Slice slice, bool direction) {
	Slice firstSlice, lastSlice, lastSliceChecked;
	int stepCount = 0, maxSliceLength = slice.length;
	int gdx = (direction) ? this->chDx : -this->chDx;
	int gdy = (direction) ? this->chDy : -this->chDy;
	int w = img.cols, h = img.rows;
	double m, b, offset, sx, sy;
	Point2f p0, p1;
	int checkCount, angleCheckFrecuency = 10;
	checkCount = angleCheckFrecuency;
	bool firstCheck = true;

	firstSlice.stableLength = false;

	while (slice.length > 0) {
		lastSlice = slice;
		slice.advanceAndScan(img, gdx, gdy);

		// break on empty slice or image border
		if (slice.length == 0 || slice.start.x <= 0 || slice.start.y <= 0 || slice.end.x >= w || slice.end.y >= h) {
			break;
		}
		// if length=1, try re-scanning or break
		else if (slice.length == 1) {
			slice.scanCentered(imgExtrude, slice.center.x, slice.center.y, maxSliceLength);
			if (!slicePixelsNotInAGroup(slice, img)) break;
		}

		if (slice.length > maxSliceLength) maxSliceLength = slice.length;

		// stop if the next slice is not clearly connected to the last slice
		if (!slicesAppearConnected(lastSlice, slice)) {
			slice.paint<uchar>(img, 1);
			break;
		}

		// store the first slice that looks stable (reference point)
		if (slice.length == lastSlice.length) {
			if (!firstSlice.stableLength) {
				firstSlice = slice;
				firstSlice.stableLength = true;
			}
		}

		// angle check: stop if the slice does not contain the expected pixel obtained by projecting the line segment
		if (stepCount > 20) {
			checkCount--;
			// Check only if slice looks normal
			if (checkCount <= 0 && slice.length == lastSlice.length) {
				if (firstCheck) {
					//printf("first angle check... Skipping.\n");
					firstCheck = false;
				} else {
					sx = slice.center.x; sy = slice.center.y;
					p0 = firstSlice.center; p1 = lastSliceChecked.center;
					/*printf("Checking for line projection discontinuity at slice centered (%.1f, %.1f)\n", sx, sy);
					printf("First slice has center (%.1f, %.1f)\n", p0.x, p0.y);
					printf("Test slice has center (%.1f, %.1f)\n", p1.x, p1.y);*/

					// figure out the offset vs. what we were expecting
					if (sliceType == SLICE_HORIZONTAL) {
						if (p0.x == p1.x) {
							offset = fabs(p0.x - sx);
						} else {
							geo::points2LineEquation(p0, p1, m, b);
							offset = fabs(geo::xForYinRect(m, b, sy) - sx);
						}
					} else {
						if (p0.y == p1.y) {
							offset = fabs(p0.y - sy);
						} else {
							geo::points2LineEquation(p0, p1, m, b);
							offset = fabs(geo::yForXinRect(m, b, sx) - sy);
						}
					}
					//printf("offset from projection: %.1f\n", offset);

					// if the expected point is not contained in the slice, break
					if (offset > 0.5 * slice.length) {
						printf("Too far from projection! Stopping...\n");
						slice.paint<uchar>(img, 1);
						break;
					}
				}

				checkCount = angleCheckFrecuency;
				lastSliceChecked = slice;
			}
		}

		addSlice(slice, direction);
		stepCount++;
	}
}

/**
 * @brief If this group is not connected to anything on both endpoints
 * @return true if disconnected
 */
bool SliceGroup::isDisconnected() {
	return (connP0.empty() && connP1.empty());
}

/**
 * @brief Returns corrected segment coordinates for presentation purposes (e.g. exporting)
 * @param p0 output corrected point
 * @param p1 output
 */
void SliceGroup::getPresentationEndpoints(Point2f &p0, Point2f &p1) {
	if (connectedToP0 != nullptr) {
		p0 = p0Cross;
	} else {
		p0 = seg.p0; // could reduce thickness?
	}
	if (connectedToP1 != nullptr) {
		p1 = p1Cross;
	} else {
		p1 = seg.p1;
	}
}

// Return the presentation endpoint p0 or p1 '0' or '1' on p
void SliceGroup::getPresentationEndpoint(uchar endpoint, Point2f &p) {
	if (endpoint == '0') {
		p = (connectedToP0 != nullptr) ? p0Cross : seg.p0;
	} else {
		p = (connectedToP1 != nullptr) ? p1Cross : seg.p1;
	}
}
Point2f SliceGroup::getPresentationEndpoint(uchar endpoint) {
	Point2f p;
	getPresentationEndpoint(endpoint, p);
	return p;
}


/**
 * @brief Absorbs the other group. Notice that resulting group might not be correlated if the groups are not perfectly colinear.
 * @param other pointer to a different slicegroup
 */
void SliceGroup::absorbGroup(SliceGroup &other) {
	// Find which one goes before the other
	/*bool thisGoesFirst;
	if (seg.p0.x == seg.p1.x && other.seg.p0.x == other.seg.p1.x) {
		thisGoesFirst = (seg.p0.y <= other.seg.p0.y);
	}
	else if (seg.p0.y == seg.p1.y && other.seg.p0.y == other.seg.p1.y) {
		thisGoesFirst = (seg.p0.x <= other.seg.p0.x);
	}
	else {
		float xSeg[4] = { seg.p0.x, seg.p1.x, other.seg.p0.x, other.seg.p1.x };
		float xMin = 99999.f;
		uchar posMin;
		for (uchar i = 0 ; i < 4 ; i++) {
			if (xSeg[i] < xMin) {
				xMin = xSeg[i];
				posMin = i;
			}
		}
		thisGoesFirst = (posMin < 2);
	}

	if (thisGoesFirst) {
		slices.insert(slices.end(), other.slices.begin(), other.slices.end());
	} else {
		slices.insert(slices.begin(), other.slices.begin(), other.slices.end());
	}

	// Absorb parallel status
	if (!parallel && other.parallel) {
		parallel = true;
		minParallelDist = other.minParallelDist;
		//parallelGroup = other.parallelGroup;
		//parallelScore = other.parallelScore;
		normal = other.normal;
	}

	if (other.connectedWall) connectedWall = true;

	/*printf("\nSlices after absorb:\n");
	for (Slice &s : slices) {
		s.print();
	}*/
	other.disable();
	absorbedGroups.push_back(other.id);
	updateSegment();
}


/**
 * @brief gets first stable slice
 * @return Slice3
 */
Slice SliceGroup::getFirstStableSlice() {
	for (int i = 0 ; i < slices.size() ; i++) {
		if (slices[i].stableLength) return slices[i];
	}
	return slices[0];
}
/**
 * @brief gets last stable slice
 * @return Slice3
 */
Slice SliceGroup::getLastStableSlice() {
	for (unsigned long i = (slices.size() - 1) ; i >= 0 ; i--) {
		if (slices[i].stableLength) return slices[i];
	}
	return slices[slices.size() - 1];
}

/**
 * @brief Register the other group as parallel to this one
 * @param other (not modified)
 */
void SliceGroup::registerParallelGroup(SliceGroup *other, float distance, float overlap, Point2f direction) {
	// Distance should be less than rectangular golden ratio
	if (distance > 0.618f * seg.length) return;

	// Minimum overlap as a measulre of own length
	if (overlap < 0.20f * seg.length) return;

	int count = (int)parallels.size();
	if (count == 0 || (count > 0) && (distance < minParallelDist)) {
		minParallelDist = distance;
	}
	parallels.emplace_back(ParallelRelation(other, distance, overlap, direction));
}


// Adds a connection relationship to another group. Cases: 1) They cross each other 2) Our endpoints connect to the other
void SliceGroup::registerConnection(SliceGroup *other, Point2f &cross, char myEnd, char otherEnd) {
	// If they cross each other
	if (myEnd == -1 && otherEnd == -1) {
		for (SliceGroup* g : crossedGroups) { if (g == other) return; }
		crossedGroups.push_back(other);
	}
	else {
		if (myEnd == 0) {
			for (ConnectRelation &r : connP0) { if (r.other == other) return; }
			connP0.emplace_back(ConnectRelation(other, cross, otherEnd));
		}
		else if (myEnd == 1) {
			for (ConnectRelation &r : connP1) { if (r.other == other) return; }
			connP1.emplace_back(ConnectRelation(other, cross, otherEnd));
		}
	}
}



// projects slices starting from firstValid and lastValid until no new valid slice is found
// stops on collision with other sliceGroups
// projection space is img CV_8U {0, 1}
// works with the segment information
// forward determines if the extrusion direction is forward or backward
void SliceGroup::extrudeSlices(Mat &imgExt, Mat &imgGroups, vector<SliceGroup> &groups, bool forward) {
	seg.update();
	//printf("\nExtruding segment\n");
	//seg.print();
	int w = imgGroups.cols, h = imgGroups.rows;

	// get forward steps for this segment
	float dx, dy;
	stepsForSegment(dx, dy, forward);
	//printf("extruding group id:%d, size:%lu with steps (%.2f, %.2f)\n", id, groups.size(), dx, dy);

	// extrude forward
	float completeness;
	unsigned long otherSize;
	int steps = -1, countAbsorbed = 0;
	ushort lastIdxAbsorbed = 0;
	Point2f p = (forward) ? seg.p1 : seg.p0;
	Slice last;
	short posGroup; // position in array
	short pixelId; // ID read in pixel from groups mat
	SliceGroup *other;
	Point pInt;

	last.setType(sliceType);
	bool advance = true;
	const float maxAngleDifference = geo::toRadians(3);

	do {
		p.x += dx;
		p.y += dy;
		steps++;

		pInt = px::integerPoint(p);
		if (pInt.x < 0 || pInt.x >= w || pInt.y < 0 || pInt.y >= h) break;
		if (imgExt.at<uchar>(pInt) == 0) { /*printf("(%.1f, %.1f) is empty.\n", p.x, p.y);*/ break; } // center pixel at extrusion space must not be empty

		pixelId = imgGroups.at<ushort>(pInt); // id of group at pixel
		posGroup = (short)(pixelId - 1); // position of that group in groups array
		//printf("advanced to (%d,%d) = %d, ", pInt.x, pInt.y, pixelId);

		if (pixelId == 0 || pixelId == id) {
			// everything ok
		}
		else if (countAbsorbed == 0 || (countAbsorbed > 0 && pixelId != lastIdxAbsorbed)) {
			// Collision! Do we absorb the group or stop?
			other = &(groups.at(posGroup));
			if (!other->isLine) break; // don't absorb noise

			otherSize = other->slices.size();
			//printf("Collided with %d which refers to group id:%d with size:%d\n", pixelId, other->id, otherSize);

			// Ignore and absorb the other group if it's small
			if (otherSize < 15) {
				//printf("Ignoring and absorbing group id:%d at (%d,%d)\n", other->id, pInt.x, pInt.y);
				other->absorbed = true;
				absorbedGroups.push_back(pixelId);
				lastIdxAbsorbed = pixelId;
				countAbsorbed++;
			}
			// If the group is big but of similar direction, absorb and copy slices
			else if (otherSize < slices.size() && geo::angleDifferenceRad(seg.angle, other->seg.angle) < maxAngleDifference) {
				//printf("Absorbing group id:%d at (%d,%d)\n", other->id, pInt.x, pInt.y);
				absorbGroup(*other);
				p = (forward) ? other->seg.p1 : other->seg.p0;
				lastIdxAbsorbed = pixelId;
				countAbsorbed++;
			}
			// Else, the group can't be absorbed
			else break;
		}

		// Continue extruding?
		completeness = last.forceScan(imgExt, p.x, p.y, mainLengths[0]);
		advance = (completeness >= T_EXTRUDE_COMPLETENESS);
		//printf("completeness: %.1f\n", completeness);

		// If we got a good slice, add it to the SliceGroup
		if (advance) {
			last.stableLength = this->sliceLengthIsStable(last);
			addSlice(last, forward);
		}
	} while (advance);

	updateSegment();
}

// Registers a new slice's data
// forward determines at what end of the deque we insert the new element
void SliceGroup::addSlice(Slice &slice, bool forward) {
	// only count non-empty slices
	if (slice.length == 0) return;

	// store slice at the correct position
	(forward) ? slices.push_back(slice) : slices.push_front(slice);

	// increment slice counters
	int c = slice.length;
	if (sliceCounts.find(c) != sliceCounts.end()) {
		sliceCounts[c] = sliceCounts[c] + 1;
	} else {
		sliceCounts[c] = 1;
	}
}

// true if we consider this slice's length stable, compared to the slice group information
bool SliceGroup::sliceLengthIsStable(Slice &slice) {
	if (slice.length <= 1) return false;
	if (slice.length == mainLengths[0]) return true;
	else if (mainLengths.size() > 1 && slice.length == mainLengths[1]) return true;

	return false;
}

// paints all slices in group at img
// img is of type T and slices will be painted with value id
// if withHighlights, paint special slices on a different color
// if withId, paints with color=id, else color=1
template<class T> void SliceGroup::paintSlices(Mat &img, bool withHighlights, bool withId) {
	ushort color = (withId) ? id : 1;
	if (withHighlights) {
		for (Slice &s : slices) {
			if (s.stableLength)	s.paint<T>(img, color);
			else	s.paint<T>(img, color + 1);
		}
	} else {
		for (Slice &s : slices) s.paint<T>(img, color);
	}
}

// stabilizes the slices with imgExtrude as reference
void SliceGroup::sliceStabilize(Mat *imgExtrude) {
	//printf("\n*** Stabilizing for segment: ");
	//seg.print();
	if (slices.empty()) return; // guard against empty groups

	bool stable;
	uchar oldLength;
	Point2f seed;

	labelValidSlices();
	uchar lastValidLength = (mainLengths.size() > 1) ? min(mainLengths[0], mainLengths[1]) : mainLengths[0];


	// stabilize slices between 2 stable slices to fit between them
	/*int idxLast = slices.size() - 2;
	for (int i = 1 ; i < idxLast ; i++) {
		if (!slices[i].stableLength) {
			if (slices[i - 1].stableLength && slices[i + 1].stableLength) {
				slices[i].stabilizeWithNeighbors(slices.at(i - 1), slices.at(i + 1));
			}
		}
	}*/

	for (Slice &s : slices) {
		if (!s.stableLength) {
			//printf("\nUnstable slice found: ");
			//s.print();

			// find new seed center for this slice
			if (sliceType == SLICE_HORIZONTAL) { // horizontal slice
				seed.y = s.center.y;
				seed.x = (seg.p0.x == seg.p1.x) ? seg.p0.x : seg.getXforY(seed.y);
			}
			else { // vertical slice
				seed.x = s.center.x;
				seed.y = (seg.p0.y == seg.p1.y) ? seg.p0.y : seg.getYforX(seed.x);
			}
			//printf("New seed center (%.1f, %.1f)\n", seed.x, seed.y);

			// re-scan the slice on imgExtrude, with limited length
			oldLength = s.length;
			s.scanCentered(*imgExtrude, seed.x, seed.y, lastValidLength);
			s.stableLength = sliceLengthIsStable(s);
		}
		else {
			lastValidLength = s.length; // we store the last valid length to give continuity to slice length
		}
	}


	updateSegment();
}

/**
 * Forcefully extrudes segment endpoint (0 or 1) to match p
 * Coordinates are obtained from the segment's line equation to account for p not being exactly colinear
 * updates segment too.
 * @param endpoint {0, 1} which endpoint to extrude
 * @param p
 */
void SliceGroup::extrudeEndpointToPoint(uchar endpoint, Point2f &p) {
	resetEndpointData(endpoint); // reset config

	if (endpoint == 0) {
		// change coordinates
		seg.p0.x = (seg.horizontalAspect) ? p.x : seg.getXforY(p.y);
		seg.p0.y = (seg.horizontalAspect) ? seg.getYforX(p.x) : p.y;
	} else {
		p1ToEndpoint = false;
		connectedToP1 = nullptr;
		seg.p1.x = (seg.horizontalAspect) ? p.x : seg.getXforY(p.y);
		seg.p1.y = (seg.horizontalAspect) ? seg.getYforX(p.x) : p.y;
	}

	seg.update(false);
}

/**
 * Queues an endpoint connection action between myEndpoint towards otherEndpoint of other
 * Only queues if new endpoint is closer than previous candidates
 * Action is not executed here, only queued
 * @param other
 * @param myEndpoint {0, 1}
 * @param otherEndpoint {0, 1}
 */
void SliceGroup::queueEndpointConnection(SliceGroup *other, uchar myEndpoint, uchar otherEndpoint, float distance) {
	if (!qConn.queued || distance < qConn.distance) {
		qConn.myEndpoint = myEndpoint;
		qConn.otherEndpoint = otherEndpoint;
		qConn.other = other;
		qConn.distance = distance;
		qConn.queued = true;
	}
}

/**
 * True if there's a segment that connects to this and the other group on both endpoints
 * @param other
 * @return
 */
bool SliceGroup::connectedByAnotherGroupTo(SliceGroup *other) {
	// Discard both endpoints disconnected case
	if (!p0ToEndpoint && !p1ToEndpoint) return false;
	if (!other->p0ToEndpoint && !other->p1ToEndpoint) return false;
	if (p0ToEndpoint) {
		if (connectedToP0 == other->connectedToP0 || connectedToP0 == other->connectedToP1) return true;
	}
	else if (p1ToEndpoint) {
		if (connectedToP1 == other->connectedToP0 || connectedToP1 == other->connectedToP1) return true;
	}
	return false;
}

// If group is not labeled as any kind of wall
bool SliceGroup::isLabeledWall() {
	if (absorbed) return false;
	if (parallels.empty() && !connectedWall) return false;
	return true;
}

// Forget about endpoint's registered data
void SliceGroup::resetEndpointData(uchar endpoint) {
	if (endpoint == 0) {
		connP0.clear();
	} else {
		connP1.clear();
	}
}

// Registers the colinear (disconnected) endpoint p, if it's the closest found so far
bool SliceGroup::registerColinearEndpoint(Point2f &p) {
	// which endpoint is closer to p?
	float dp0 = geo::euclideanDistance(seg.p0, p);
	float dp1 = geo::euclideanDistance(seg.p1, p);
	uchar endpoint = (dp0 < dp1) ? '0' : '1';
	if ((endpoint == '0') && (minColinearDistP0 < 0 || dp0 < minColinearDistP0)) {
		minColinearDistP0 = dp0;
		colinearP0 = p;
		return true;
	}
	else if ((endpoint == '1') && (minColinearDistP1 < 0 || dp1 < minColinearDistP1)) {
		minColinearDistP1 = dp1;
		colinearP1 = p;
		return true;
	}
	return false;
}

// true if endpoint '0' or '1' is disconnected
bool SliceGroup::isEndDisconnected(uchar endpoint) {
	return (endpoint == '0') ? (connectedToP0 == nullptr) : (connectedToP1 == nullptr);
}

// true if endpoint '0' or '1' is connected to another endpoint
bool SliceGroup::isEndToEnd(uchar endpoint) {
	return (endpoint == '0') ? p0ToEndpoint : p1ToEndpoint;
}

// true if p is a better candidate as a colinear endpoint to p0 or p1 specified
bool SliceGroup::isCloserColinearPoint(Point2f &p, float distance, uchar endpoint) {
	if (endpoint == '0') {
		return ((minColinearDistP0 < 0) || (distance < minColinearDistP0));
	} else {
		return ((minColinearDistP1 < 0) || (distance < minColinearDistP1));
	}
}

// true if one of the segment endpoints connects to other
bool SliceGroup::isEndConnectedTo(SliceGroup *other) {
	return ((connectedToP0 == other && p0ToEndpoint) || connectedToP1 == other && p1ToEndpoint);
}

// group is no more considered for anything
void SliceGroup::disable() {
	enabled = false;
	absorbed = true;
	parallels.clear();
	connectedWall = false;
	resetEndpointData('0');
	resetEndpointData('1');
}

// true if slicegroup looks like a line segment
bool SliceGroup::isLineSegment() {
	return true;
}

// num of disconnected endpoints {0,1,2}
int SliceGroup::countDisconnectedEndpoints() {
	if (connectedToP0 == nullptr) {
		return (connectedToP1 == nullptr) ? 0 : 1;
	}

	return (connectedToP1 == nullptr) ? 1 : 2;
}

// true if is endpoint connected to a parallel's endpoint (not to its body)
bool SliceGroup::isEndConnectedToParallel() {
	//return ((p0ToEndpoint && connectedToP0->parallel) || (p1ToEndpoint && connectedToP1->parallel));
	return false; // REIMPLEMENT IF NEEDED
}


/**
 * @brief stores in line a text representation of this segment for storage
 * @param line output (dont return a copy for performance)
 */
void SliceGroup::serialize(char line[]) {
	//getPresentationEndpoints(p0p, p1p);
	Point2f &sp0 = seg.p0;
	Point2f &sp1 = seg.p1;
	sprintf(line, "%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n", sp0.x, sp0.y, sp1.x, sp1.y, seg.thickness, seg.sliceThickness, normal.x, normal.y, minParallelDist);
}

// true if endpoint is connected to another segment's body (not endpoint)
bool SliceGroup::isEndConnectedToBody(uchar endpoint) {
	if (endpoint == '0') {
		return (connectedToP0 != nullptr && !p0ToEndpoint);
	} else {
		return (connectedToP1 != nullptr && !p1ToEndpoint);
	}

	return false;
}

/**
 * Splits this slicegroup in two: This (p0, p) and the other (p, p1).
 * @param p subdivision point. Must be part of this group's mathematical segment
 * @param other the other slicegroup created
 * @return true if the subdivision happened
 */
bool SliceGroup::subdivideAt(Point2f p, SliceGroup &other) {
	printf("Subdividing segment: "); seg.printCoords();

	// @GUARD: don't subdivide near the endpoints
	if (geo::euclideanDistance(p, seg.p0) < seg.thickness || geo::euclideanDistance(p, seg.p1) < seg.thickness) return false;

	slices.clear(); // get rid of slices

	// split in two
	Point2f p0 = seg.p0, p1 = seg.p1;
	float sliceThickness = seg.sliceThickness;
	Segment otherSeg = Segment(p, p1, sliceThickness);
	other = SliceGroup(otherSeg);
	other.isLine = true;
	seg = Segment(p0, p, sliceThickness);

	printf("Obtained:\n"); seg.printCoords(); other.seg.printCoords();

	// remove all relationships
	parallels.clear();
	connP0.clear();
	connP1.clear();

	return true;
}

SliceGroup::SliceGroup() {

}

bool SliceGroup::isParallel() {
	return !(parallels.empty());
}

// template declaration here to avoid template compiler errors
template void SliceGroup::paintSlices<ushort>(Mat &img, bool withHighlights, bool withId);
template void SliceGroup::paintSlices<uchar>(Mat &img, bool withHighlights, bool withId);
