#ifndef SLICEGROUP_H
#define SLICEGROUP_H

#define MOVING_FORWARD true
#define MOVING_BACKWARD false

#include <mutex>
#include <map>
#include <algorithm>
#include <numeric>
#include <deque>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "util/display.h"
#include "util/px.h"
#include "slice.h"
#include "util/segment.h"
#include "util/debug.h"
#include "parallelRelation.h"
#include "connectrelation.h"

using namespace cv;
using std::map;

class SliceGroup;
class ParallelRelation;

// HELPER: Represents a queued endpoint extrusion
struct queuedConnection {
	SliceGroup *other = nullptr; // other slicegroup
	uchar myEndpoint = 0; // {0, 1}
	uchar otherEndpoint = 0; // {0, 1}
	float distance = 0; // Euclidean distance between both endpoints
	bool queued = false; // true if this is queued to execute
	bool executed = false; // true if the connection was executed
};


class SliceGroup {
	float T_MAIN_LENGTH_PERCENT = 0.5; // Minimum percentage of the most popular slice length's count to be considered a "main" slice length
	float T_EXTRUDE_COMPLETENESS = 0.6; // Min percentage of non-empty slice pixels to advance extrusion
	double T_MIN_CORRELATION = 0.5; // Minimum correlation to be considered a straight line
	uint T_SLICES_SMALL = 20; // Max number of slices to consider this a "small" group

	void stepsForSegment(float &dx, float &dy, bool forward);
	void findMainLengths();
	void findExteriorSlices();
	void labelValidSlices();
	void updateSegment();
	bool slicePixelsNotInAGroup(Slice &s, Mat &imgGroups);
	bool slicesAppearConnected(Slice &s1, Slice &s2);
	void scanSlicesInDirection(Mat &img, Mat &imgExtrude, Slice slice, bool direction);

public:
	ushort id = 0; // unique id for this group, greater than 0
	uchar type = 0; // 0: normal, 1: virtual
	bool enabled = true; // false if this slicegroup should be omitted
	bool isLine = true; // false if not a straight line

	Segment seg; // segment we approximate this group to
	deque<Slice> slices; // ordered slices
	int idxFirstValid, idxLastValid; // first and last normal slices indexes
	double correlation; // Correlation coefficient between the slices and the segment

	map<uchar, int> sliceCounts; // Map of slice thickness -> count
	vector<uchar> mainLengths; // Most popular slice lengths

	uchar sliceType; // SLICE_HORIZONTAL or SLICE_VERTICAL
	uchar chDx, chDy; // advance offset for channel
	
	vector<ushort> absorbedGroups; // id of groups that were absorbed on extrusion
	bool absorbed = false; // true if this group was absorbed.


	bool parallelSeriesConnected = false; // true if parto of a chain of endpoint-connected to a parallel
	bool closedLoop = false; // true if part of a closed loop
	bool connectedWall = false; // true if a group's endopoint is connected to a wall line segment
	SliceGroup *connectedToP0 = nullptr; // group p0 is connected to
	SliceGroup *connectedToP1 = nullptr; // group p1 is connected to
	bool p0ToEndpoint = false; // true if p0 is connected to another endpoint
	bool p1ToEndpoint = false;
	Point2f p0Cross, p1Cross; // if p0 or p1 are connected, this is the cross point between segments

	Point2f colinearP0 = Point2f(0, 0); // closest colinear endpoint to p0
	Point2f colinearP1 = Point2f(0, 0); // closest colinear endpoint to p1
	float minColinearDistP0 = -1, minColinearDistP1 = -1; // distance to closest colinear endpoints

	vector<SliceGroup*> crossedGroups; // pointers of groups crossed (away from my endpoints)
	vector<SliceGroup*> connectedGroups; // pointers to all groups connected

	queuedConnection qConn; // holds a single endpoint connection request

	list<ConnectRelation> connP0, connP1; // list of connections to our endpoints
	list<ParallelRelation> parallels; // list of parallel groups

	//bool parallel = false; // true if they are parallel to any other close line
	//SliceGroup *parallelGroup = nullptr; // Points to the parallel segment to this group
	float minParallelDist = 0.f; // min distance to a parallel line considered; 0 usually means not
	//float parallelScore = 0.f;
	Point2f normal = Point2f(0, 0); // unit vector, points in the opposite direction of the parallel group

	SliceGroup(Mat &img, Mat &imgExtrude, int x, int y, uchar sliceType, uchar chDx, uchar chDy);
	SliceGroup(Segment s);
	SliceGroup();

	void addSlice(Slice &slice, bool forward);

	template<class T> void paintSlices(Mat &img, bool withHighlights, bool withId);

	bool sliceLengthIsStable(Slice &slice);
	void sliceStabilize(Mat *imgExtrude);
	void extrudeSlices(Mat &imgExtrude, Mat &imgGroups, vector<SliceGroup> &groups, bool forward);
	void extrudeEndpointToPoint(uchar endpoint, Point2f &p);
	void removeMisplacedSlices();

	void absorbGroup(SliceGroup &other);

	Slice getFirstStableSlice();
	Slice getLastStableSlice();

	void registerParallelGroup(SliceGroup *other, float distance, float overlap, Point2f direction);
	void registerConnection(SliceGroup *other, Point2f &cross, char myEnd, char otherEnd);
	void resetEndpointData(uchar endpoint);
	bool registerColinearEndpoint(Point2f &p);
	void disable();

	bool connectedByAnotherGroupTo(SliceGroup *other);
	bool isDisconnected();
	bool isLabeledWall();
	bool isParallel();
	bool isEndDisconnected(uchar endpoint);
	bool isEndToEnd(uchar endpoint);
	bool isCloserColinearPoint(Point2f &p, float distance, uchar endpoint);
	bool isEndConnectedTo(SliceGroup *other);
	bool isEndConnectedToParallel();
	bool isEndConnectedToBody(uchar endpoint);

	bool isLineSegment();
	bool subdivideAt(Point2f p, SliceGroup &other);

	int countDisconnectedEndpoints();
	void getPresentationEndpoints(Point2f &p0, Point2f &p1);
	void getPresentationEndpoint(uchar endpoint, Point2f &p);
	Point2f getPresentationEndpoint(uchar endpoint);
	void serialize(char line[]);

	void queueEndpointConnection(SliceGroup *other, uchar myEndpoint, uchar otherEndpoint, float distance);
};


#endif // SLICEGROUP_H
