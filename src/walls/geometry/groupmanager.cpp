#include "groupmanager.h"


/**
 * @brief Determines if we consider g1 and g2 to be parallel or approximately parallel
 * @param g1
 * @param g2
 * @param tolerance maximum difference in degrees allowed to consider line directions similar
 * @return true if they are approximately parallel
 */
bool GroupManager::groupsHaveSimilarAngle(SliceGroup &g1, SliceGroup &g2, float tolerance) {
	// handle perfect cases of equal slope (and discard verticals)
	if (g1.seg.isPerfectVertical() && g2.seg.isPerfectVertical()) return true;
	if (g1.seg.isPerfectHorizontal() && g2.seg.isPerfectHorizontal()) return true;
	if (g1.seg.slope == g2.seg.slope) return true;

	return (geo::lineDirectionDiference(g1.seg.angle, g2.seg.angle) <= tolerance);
}

/**
 * @brief Iteratively propagate the connectedWall flag to connected groups, starting from next
 * @param next
 */
void GroupManager::propagateWallConnectedLabel(SliceGroup *prev, SliceGroup *next, bool connectedLabel, bool parallelSeriesLabel) {
	/*SliceGroup *first = prev;
	int step = 0, maxSteps = 12;

	while ((next != nullptr) && (next != first) && (next != prev) && !next->parallel && (step < maxSteps)) {
		//printf("Propagating from %p to %p (step %d)\n", prev, next, step);
		next->connectedWall = connectedLabel;
		next->parallelSeriesConnected = parallelSeriesLabel;
		if (!connectedLabel) next->parallel = false;

		// advance to the connected group we haven't visited yet
		if (next->p0ToEndpoint && next->connectedToP0 != prev) {
			prev = next;
			next = next->connectedToP0;
		} else if (next->p1ToEndpoint && next->connectedToP1 != prev) {
			prev = next;
			next = next->connectedToP1;
		} else {
			next = nullptr;
		}
		step++;
	}*/
	//printf("Finished propagating\n");
}


// true if there's an uninterrupted path in img between p0 and p1, with tolerance
bool GroupManager::uninterruptedPath(Mat &img, Point2f &p0, Point2f &p1, float tolerance) {
	Point2f p = p0;
	Point2f dir = geo::directionBetweenPoints(p0, p1);
	float length = geo::euclideanDistance(p0, p1);

	int blacks = 0, changes = 0;
	uchar val, lastVal = 1;

	//Mat img2 = Mat(img.rows, img.cols, CV_8U, Scalar(0));

	float n = 0.f;
	//printf("\ntesting between (%.1f, %.1f) and (%.1f, %.1f) with dir (%.1f, %.1f)\n", p0.x, p0.y, p1.x, p1.y, dir.x, dir.y);
	while (n < length) {
		//printf("scanning (%.1f, %.1f)\n", p.x, p.y);
		p += dir;
		val = img.at<uchar>(p);

		// count non-empties
		if (val != 0) {
			blacks++;
			if (blacks > tolerance * 2) return false;
		}

		if (lastVal != val) {
			changes++;
			if (changes > 2) return false;
		}
		lastVal = val;
		n += 1.f;

		//img2.at<uchar>(p) = 255;
	}
	//disp::show(img2, "path");
	//waitKey();

	return true;
}


GroupManager::GroupManager(Mat *img) {
	this->img = img;
}

/**
 * @brief Adds new group to the groups vector if the group is valid; resets group id to match vector position
 * @param g candidate group to add
 */
void GroupManager::addGroup(SliceGroup &g) {
	// Guard against disabled groups
	if (!g.enabled) return;
	//if (g.slices.size() < MIN_GROUP_SLICES) return;

	g.id = groups.size() + 1;
	groups.push_back(g);
}

/**
 * Adds a sliceless group built using a segment
 * we suppose the segment is a proven wall segment
 * @param seg
 */
void GroupManager::addGroup(Segment seg, Point2f normal, float minParallelDist) {
	SliceGroup g = SliceGroup(seg);
	g.id = groups.size() + 1;
	g.normal = normal;
	g.connectedWall = true;
	g.minParallelDist = minParallelDist;
	groups.push_back(g);
}



/**
 * @brief Adds all of the valid SliceGroups in grs to our groups vector
 * @param grs vector of SliceGroups
 */
void GroupManager::addGroupsVector(vector<SliceGroup> &grs) {
	for (SliceGroup &g : grs) addGroup(g);
}

/**
 * @brief Marks every group that has a close parallel line of similar length
 */
void GroupManager::detectParallels(bool onlyWalls) {
	printf("Detecting parallel lines...\n");

	// first reset all known parallel relationships
	for (SliceGroup &g : groups) g.parallels.clear();

	// Detect all parallel relationships
	runForAllGroupPairs(groups, [this, onlyWalls](SliceGroup *gi, SliceGroup *gj)-> bool {
		// Only consider enabled groups
		if (!gi->enabled) return false;
		if (!gj->enabled) return true;

		// Optional: Only work with walls (e.g. correcting)
		if (onlyWalls) {
			if (!gi->connectedWall) return false;
			if (!gj->connectedWall) return true;
		}

		Segment *si = &(gi->seg);
		Segment *sj = &(gj->seg);
		float li = si->length, lj = sj->length;

		// Filter short segments that don't look like lines
		if (li < MIN_LINE_LENGTH) { gi->disable(); return false; }
		if (lj < MIN_LINE_LENGTH) { gj->disable(); return true;  }

		// Make sure gj is parallel to gi
		if (!(si->isParallelTo(sj, MAX_DIF_PARALLEL_ANGLES))) return true;

		//printf("\nComparing:\n"); si->print(); sj->print();
		float distance;
		Point2f direction; // from si to sj
		float overlap = geo::distanceBetweenParallelRects(si->p0, si->p1, sj->p0, sj->p1, distance, direction);
		//printf("parallel distance: %.1f, overlap: %.1f\n", distance, overlap);

		float minDistThickness = 1.0f * (si->thickness + sj->thickness) / 2.f;
		//printf("minDistThickness: %.2f\n", minDistThickness);
		if (distance < minDistThickness) return true; // filter min distance from thickness

		// Register parallels
		if (overlap > 0) {
			//printf("Parallels!!\n\n");
			gi->registerParallelGroup(gj, distance, overlap, direction);
			gj->registerParallelGroup(gi, distance, overlap, -direction);
		}

		return true;
	});
}

/**
 * @brief Remove "parallel" label from groups where the minDistance is uncommon
 */
void GroupManager::filterParallelsByDistance() {
	printf("Filtering parallels by distance...\n");

	// find parallel distance thresholds
	float minDist = 99999, maxDist = 0, avgThickness = 0;
	int parallelCount = 0;
	vector<float> dists;
	for (SliceGroup &g : groups) {
		// store information of parallel groups only
		if (!g.parallels.empty()) {
			avgThickness += g.seg.thickness;
			parallelCount++;
		}

		for (ParallelRelation &r : g.parallels) {
			dists.push_back(r.distance);

			if (r.distance < minDist) minDist = r.distance;
			if (r.distance > maxDist) maxDist = r.distance;
			//printf("%.2f\t%.2f\t%.2f\n", g.seg.length, r.distance, r.overlap);
		}
	}

	if (parallelCount == 0) {
		printf("filterParallelsByDistance:: ABORT. No parallels found.\n");
		return;
	}
	avgThickness /= parallelCount;

	// Threshold by parallel distance, using the quartiles
	float q1, q2, q3, minD, maxD;
	vector<float> quant = stats::Quantile<float>(dists, { 0.25, 0.5, 0.75 });
	q1 = quant[0];
	q2 = quant[1]; // median
	q3 = quant[2];
	printf("q1:%.1f\tq2:%.1f\tq3:%.1f\n", q1, q2, q3);

	// Only threshold if there's a big variation in parallel distance
	float iqr = q3 - q1;
	// if the iqr is too small, set tolerance manually
	if (iqr < avgThickness) {
		minD = q1 - avgThickness;
		maxD = q3 + avgThickness;
	} else {
		minD = q1 - 0.1f * (q3 - q1);
		maxD = q3 + 0.1f * (q3 - q1);
	}
	printf("Filtering parallel distance out of the range [%.2f, %.2f]\n", minD, maxD);
	for (SliceGroup &g : groups) {
		g.parallels.remove_if([minD, maxD] (ParallelRelation &r) {
			return (r.distance < minD || r.distance > maxD);
		});
	}

	// threshold my minimum accumulated overlap
	float sum;
	for (SliceGroup &g : groups) {
		sum = 0;
		for (ParallelRelation &r : g.parallels) sum += r.overlap;
		if (sum <= 0.5 * g.seg.length) {
			g.parallels.clear();
		}
	}
}

/**
 * @brief Remove parallels that don't have any connected endpoint
 */
void GroupManager::filterIsolatedParallels() {
	printf("Filtering disconnected walls...\n");

	for (SliceGroup &g : groups) {
		if (!g.parallels.empty()) {
			// filter if not connected to anything (endpoint or group body)
			if (g.isDisconnected() && g.crossedGroups.empty()) {
				g.disable();
			}
		}
	}
}

/**
 * @brief Removes parallel or connected segments (and their connected lines) if they cross a high number of parallels
 * or if they are inside a wall
 */
void GroupManager::filterOverlappedParallels() {
	/*printf("Filtering overlapped walls...\n");

	int countCrossed;

	for (SliceGroup &g : groups) {
		// deactivate if parallel is inside a wall
		//if (g.parallel) {

		//}

		if (g.parallel || g.connectedWall) {
			// filter if crosses too many parallels
			if (g.crossedGroups.size() >= 4) {
				countCrossed = 0;
				for (SliceGroup* gc : g.crossedGroups) {
					if (gc->parallel) countCrossed++;
				}
				if (countCrossed >= 4) {
					g.parallel = false;
					g.connectedWall = false;
					// also deactivate connected
					if (g.p0ToEndpoint) propagateWallConnectedLabel(&g, g.connectedToP0, false, false);
					if (g.p1ToEndpoint) propagateWallConnectedLabel(&g, g.connectedToP1, false, false);
				}
			}
		}
	}*/
}

/**
 * @brief Merges groups whose segments are colinear and have close endpoints
 */
void GroupManager::mergeAllCloseColinearGroups(vector<SliceGroup> &grs) {
	runForAllGroupPairs(grs, [this](SliceGroup *gi, SliceGroup *gj)-> bool {
		// Guard against disabled groups
		if (gi->absorbed || !gi->isLine) return false;
		if (gj->absorbed || !gj->isLine) return true;

		bool gip0conn = (gi->connectedToP0 != nullptr), gip1conn = (gi->connectedToP1 != nullptr);
		bool gjp0conn = (gj->connectedToP0 != nullptr), gjp1conn = (gj->connectedToP1 != nullptr);

		// guard against gi or gj already fully connected
		if (gip0conn && gip1conn) return false;
		if (gjp0conn && gjp1conn) return true;

		// must be strictly almost equal angles and slice thickness
		if (fabs(gi->seg.thickness - gj->seg.thickness) > 1.f) return true; // require similar slice thickness
		if (!(gi->seg.isCollinearTo(&(gj->seg), this->MAX_DIF_PARALLEL_ANGLES))) return true; // require collinearity

		// check the 4 possible combinations and merge if close enough
		float maxEndpointDistance = 2.f * std::max<float>(gi->seg.thickness, gj->seg.thickness); // THRESHOLD

		if ((!gip0conn && !gjp0conn && (geo::euclideanDistance(gi->seg.p0, gj->seg.p0) <= maxEndpointDistance)) ||
			(!gip0conn && !gjp1conn && (geo::euclideanDistance(gi->seg.p0, gj->seg.p1) <= maxEndpointDistance)) ||
			(!gip1conn && !gjp0conn && (geo::euclideanDistance(gi->seg.p1, gj->seg.p0) <= maxEndpointDistance)) ||
			(!gip1conn && !gjp1conn && (geo::euclideanDistance(gi->seg.p1, gj->seg.p1) <= maxEndpointDistance)))
			{ mergeGroups(gi, gj); }

		return true;
	});
}


/**
 * @brief For all groups, detect and store other groups connected to their endpoints
 */
void GroupManager::detectSegmentConnections() {
	printf("Detecting line connections...\n");

	for (SliceGroup &g : groups) {
		g.resetEndpointData('0');
		g.resetEndpointData('1');
		g.crossedGroups.clear();
	}

	runForAllGroupPairs(groups, [this](SliceGroup *gi, SliceGroup *gj)-> bool {
		Segment &si = gi->seg;
		Segment &sj = gj->seg;
		float maxDistance = 2 * std::max<float>(si.thickness, sj.thickness); // THRESHOLD

		// find the cross-point between both line equations (re-use the slope and offset already stored in the segments)
		Point2f cross = segmentCrossPoint(si, sj);
		if (cross.x <= 0 || cross.x >= img->cols || cross.y <= 0 || cross.y >= img->rows) return true;

		//printf("Detecting connections between (%.1f,%.1f)(%.1f,%.1f) and (%.1f,%.1f)(%.1f,%.1f)\n", si.p0.x, si.p0.y, si.p1.x, si.p1.y, sj.p0.x, sj.p0.y, sj.p1.x, sj.p1.y);

		// make sure the cross point is contained in both line segments (they touch each other)
		if (!si.containsPoint(cross, 0.5f * si.thickness)) { /*printf("s0 doesn't contain point.\n");*/ return true; }
		if (!sj.containsPoint(cross, 0.5f * sj.thickness)) { /*printf("s1 doesn't contain point.\n");*/ return true; }

		// at this point we're sure they are connected
		char siEnd = -1, sjEnd = -1; // {-1,0,1}. -1: no other endpoint
		bool i0Cross, i1Cross, j0Cross, j1Cross;
		i0Cross = (geo::euclideanDistance(si.p0, cross) <= maxDistance);
		i1Cross = (geo::euclideanDistance(si.p1, cross) <= maxDistance);
		j0Cross = (geo::euclideanDistance(sj.p0, cross) <= maxDistance);
		j1Cross = (geo::euclideanDistance(sj.p1, cross) <= maxDistance);
		//printf("Endpoints connected: gi: {%d %d}, gj: {%d %d}\n", i0Cross,i1Cross,j0Cross,j1Cross);

		// Disable segments with invalid connections
		if (i0Cross && i1Cross && (!j0Cross || !j1Cross)) { gi->disable(); return false; }
		if (j0Cross && j1Cross && (!i0Cross || !i1Cross)) { gj->disable(); return true; }
		if (j0Cross && j1Cross && i0Cross && i1Cross) {
			if (si.length >= sj.length) { gj->disable(); return true; }
			else { gi->disable(); return false; }
		}

		// Register connection or crossing relationship, supposing a valid connection case
		if (i0Cross || i1Cross) siEnd = char((i0Cross) ? 0 : 1);
		if (j0Cross || j1Cross) sjEnd = char((j0Cross) ? 0 : 1);
		gi->registerConnection(gj, cross, siEnd, sjEnd);
		gj->registerConnection(gi, cross, sjEnd, siEnd);

		return true;
	});
}

/**
 * @brief Labels all groups connected directly or indirectly to the endpoint of a wall line segment
 */
void GroupManager::labelWallConnectedEndpoints() {
	printf("Detecting lines connected to walls...\n");

	for (SliceGroup &g : groups) {
		// Parallel lines are walls
		if (!g.parallels.empty()) {
			g.connectedWall = true;
		}
		// lines connected to parallel lines' bodies are walls (probably)
		else {
			bool connectedParallel = false;
			for (ConnectRelation &r : g.connP0) {
				if (!r.other->parallels.empty()) connectedParallel = true;
			}
			for (ConnectRelation &r : g.connP1) {
				if (!r.other->parallels.empty()) connectedParallel = true;
			}

			g.connectedWall = connectedParallel;
		}

		/*if (g.connectedWall) {
			bool endConnectedToParallel = (g.isEndConnectedToParallel());
			bool connectedWallFlag = true;
			// also label all connected groups to both endpoints
			propagateWallConnectedLabel(&g, g.connectedToP0, connectedWallFlag, endConnectedToParallel);
			propagateWallConnectedLabel(&g, g.connectedToP1, connectedWallFlag, endConnectedToParallel);
		}*/
	}
}

/**
 * @brief Merges a pair of groups. Bigger group absorbs smaller group. Smaller group gets deleted.
 * @param g0
 * @param g1
 */
void GroupManager::mergeGroups(SliceGroup *g0, SliceGroup *g1) {
	/*printf("\nMerging groups:\n");
	g0->seg.print();
	g1->seg.print();*/
	if (g0->slices.size() >= g1->slices.size()) {
		g0->absorbGroup(*g1);
	} else {
		g1->absorbGroup(*g0);
	}
}

/**
 * @brief Helper: paint all SliceGroup ids on imgDebug
 * @param imgDebug CV_16U
 */
void GroupManager::paintColorGroups(Mat &imgOut, list<SliceGroup> &grs) {
	// Guard against non-initialized img
	if (imgOut.empty()) imgOut = Mat(img->rows, img->cols, CV_16U, Scalar(0));

	for (SliceGroup &sg : grs) {
		if (!sg.absorbed) sg.paintSlices<ushort>(imgOut, false, true);
	}
}

/**
 * @brief Writes all SliceGroups to path, 1 per line, serialized
 * Purge groups before using this function so we get continuous ids
 * @param path output full path with name
 */
void GroupManager::writeSegmentsToFile() {
	printf("Storing walls...\n");
	ofstream fileWalls, fileVirtuals;
	char line[255];
	Point2f p0, p1;

	try {
		fileWalls.open(outPath + "segments.txt");
		fileVirtuals.open(outPath + "virtuals.txt");

		for (SliceGroup &g : groups) {
			if (!g.absorbed && g.connectedWall) {
				// Write wall segment
				g.serialize(line);
				fileWalls << line;

				// Write virtual walls
				if (g.minColinearDistP0 > 0) {
					p0 = g.getPresentationEndpoint('0');
					p1 = g.colinearP0;
					sprintf(line, "%.2f,%.2f,%.2f,%.2f\n", p0.x, p0.y, p1.x, p1.y);
					fileVirtuals << line;
				}
				if (g.minColinearDistP1 > 0) {
					p0 = g.getPresentationEndpoint('1');
					p1 = g.colinearP1;
					sprintf(line, "%.2f,%.2f,%.2f,%.2f\n", p0.x, p0.y, p1.x, p1.y);
					fileVirtuals << line;
				}
			}
		}

		fileWalls.close();
		fileVirtuals.close();
	}
	catch (const ifstream::failure& e) {
		cout << "Exception opening/reading file";
	}
}

// Reads groups from file in path
void GroupManager::readSegmentsFromFile() {
	Point2f normal;
	Segment seg;
	float minParallelDistance;

	// Load wall segments
	char seps[] = ",";
	char *token;
	char line[512];

	string path = outPath + "segments.txt";
	FILE* fp = fopen(path.c_str(), "r");
	while(fgets(line, 512, fp) != nullptr) {
		token = strtok(line, seps);		seg.p0.x = strtof(token, nullptr);
		token = strtok (nullptr, seps);	seg.p0.y = strtof(token, nullptr);
		token = strtok (nullptr, seps);	seg.p1.x = strtof(token, nullptr);
		token = strtok (nullptr, seps);	seg.p1.y = strtof(token, nullptr);
		token = strtok (nullptr, seps);	seg.thickness = strtof(token, nullptr);
		token = strtok (nullptr, seps);	seg.sliceThickness = strtof(token, nullptr);
		token = strtok (nullptr, seps);	normal.x = strtof(token, nullptr);
		token = strtok (nullptr, seps);	normal.y = strtof(token, nullptr);
		token = strtok (nullptr, seps);	minParallelDistance = strtof(token, nullptr);
		seg.update(false);
		addGroup(seg, normal, minParallelDistance);
	}
	fclose(fp);

	// Load virtual walls
	Point2f p0, p1, n = Point2f(0,0);
	WallSegment ws;

	path = outPath + "virtuals.txt";
	fp = fopen(path.c_str(), "r");
	while(fgets(line, 512, fp) != nullptr) {
		token = strtok(line, seps);			p0.x = strtof(token, nullptr);
		token = strtok (nullptr, seps);		p0.y = strtof(token, nullptr);
		token = strtok (nullptr, seps);		p1.x = strtof(token, nullptr);
		token = strtok (nullptr, seps);		p1.y = strtof(token, nullptr);
		ws = WallSegment(p0, p1, n);
		virtuals.push_back(ws);
	}
	fclose(fp);
}

/**
 * @brief Writes all rooms, 1 per line, serialized
 * @param path output full path with name
 */
void GroupManager::writeRoomsToFile(vector<vector<Point> > &contours) {
	ofstream fileWalls, fileRooms;
	int nWall = 1, nRoom = 1;
	vector<Point> contour;
	Point p0, p1;

	try {
		fileWalls.open(outPath + "rooms-walls.txt");
		fileRooms.open(outPath + "rooms.txt");

		bool first = true;
		for (size_t idx = 0; idx < contours.size(); idx++) {
			contour = contours.at(idx);

			if (contour.size() < 4) continue; // ignore contours with less than 4 points

			// write contour's walls
			fileRooms << nRoom;

			// add all contour walls to walls file
			for (unsigned long i = 0 ; i < contour.size() - 1 ; i++) {
				p0 = contour.at(i);
				p1 = contour.at(i + 1);
				fileWalls << nWall << "," << p0.x << "," << p0.y << "," << p1.x << "," << p1.y << "\n";
				fileRooms << "," << nWall;
				nWall++;
			}
			fileRooms << "\n";
			nRoom++;
		}

		fileWalls.close();
		fileRooms.close();
	}
	catch (const ifstream::failure& e) {
		cout << "Exception opening/reading file";
	}
}

/**
 * @brief Helper: Runs the function f (e.g. lambda) for all possible pair of groups
 * @param f lamda function in the form: (SliceGroup2 *gi, SliceGroup2 *gj)-> bool. If returns false, current gi is skipped
 * @param grs Groups vector to run the function on
 */
template<typename Func>
void GroupManager::runForAllGroupPairs(list<SliceGroup> &grs, Func f) {
	bool loop;
	std::list<SliceGroup>::iterator it_i, it_j;
	for (it_i = grs.begin(); it_i != grs.end(); it_i++) {
		it_j = it_i;
		it_j++;
		if (it_j == grs.end()) break;
		for (it_j; it_j != grs.end(); it_j++) {
			loop = f(&(*it_i), &(*it_j));
			if (!loop) break;
		}
	}
}
template<typename Func>
void GroupManager::runForAllGroupPairs(vector<SliceGroup> &grs, Func f) {
	bool loop;
	std::vector<SliceGroup>::iterator it_i, it_j;
	for (it_i = grs.begin(); it_i != grs.end(); it_i++) {
		it_j = it_i;
		it_j++;
		if (it_j == grs.end()) break;
		for (it_j; it_j != grs.end(); ++it_j) {
			loop = f(&(*it_i), &(*it_j));
			if (!loop) break;
		}
	}
}

/**
 * Extrude segments with a colinear close endpoint from a different segment
 * @param im CV_8U
 */
void GroupManager::extrudeColinearEndpoints(Mat &im) {
	/*printf("Connecting co-linear endpoints...\n");

	// Search for the best connection candidate
	runForAllGroupPairs(groups, [this, &im](SliceGroup *gi, SliceGroup *gj)-> bool {
		// Only work with wall connected groups that have at least 1 free endpoint
		if (!gi->isLabeledWall()) return false;
		if (!gj->isLabeledWall()) return true;
		bool p0i_connected = (gi->connectedToP0 != nullptr), p0j_connected = (gj->connectedToP0 != nullptr);
		if (p0i_connected && gi->connectedToP1 != nullptr) return false;
		if (p0j_connected && gj->connectedToP1 != nullptr) return true;

		// Prepare variables
		Segment &si = gi->seg, &sj = gj->seg;
		float dist, dist2, li = si.length, lj = sj.length;
		
		// Decide which points can be associated and find their distance
		Point2f pj, pi = (!p0i_connected) ? si.p0 : si.p1;
		if (gj->connectedWall) {
			pj = (!p0j_connected) ? sj.p0 : sj.p1;
			dist = geo::euclideanDistance(pi, pj);
		}
		else {
			dist = geo::euclideanDistance(pi, sj.p0);
			dist2 = geo::euclideanDistance(pi, sj.p1);
			if (dist <= dist2) {
				pj = sj.p0;
			} else {
				pj = sj.p1;
				dist = dist2;
			}
		}
		
		uchar end_i = (!p0i_connected) ? uchar(0) : uchar(1);
		uchar end_j = (!p0j_connected) ? uchar(0) : uchar(1);

		// check colinearity for both gi and gj
		if (dist < li) {
			if (si.isCollinearTo(pj, si.thickness)) {
				if (blackPixelsInLine(im, pi, pj, 0) > 0.4) {
					gi->queueEndpointConnection(gj, end_i, end_j, dist);
					return true;
				}
			}
		}
		if (dist < lj) {
			if (sj.isCollinearTo(pi, sj.thickness)) {
				if (blackPixelsInLine(im, pi, pj, 0) > 0.4) {
					gj->queueEndpointConnection(gi, end_j, end_i, dist);
					return true;
				}
			}
		}

		return true;
	});

	// execute all queued connections as long as they have black pixels between them
	Point2f myP, otherP, cross;
	SliceGroup *g2;
	int n = 0;
	for (SliceGroup &g : groups) {
		if (g.qConn.queued) {
			g2 = g.qConn.other;
			myP = (g.qConn.myEndpoint == 0) ? g.seg.p0 : g.seg.p1;
			otherP = (g.qConn.otherEndpoint == 0) ? g2->seg.p0 : g2->seg.p1;
			n++;
			
			if ((fabs(g.seg.sliceThickness - g2->seg.sliceThickness) < 1.f) && g.seg.isCollinearTo(&g2->seg, 2)) {
				// if segments are collinear and of similar thickness, merge them
				(g.seg.length > g2->seg.length) ? g.absorbGroup(*g2) : g2->absorbGroup(g);
			}
			else {
				// just connect them
				g.extrudeEndpointToPoint(g.qConn.myEndpoint, otherP);
				cross = (g.qConn.myEndpoint == 0) ? g.seg.p0 : g.seg.p1; // update after extruding
				g.registerConnectedEndpoint(g2, cross, g.qConn.myEndpoint);
				g2->registerConnectedEndpoint(&g, cross, g.qConn.otherEndpoint);
				g.connectedWall = true;
			}

			g.qConn.executed = true;
		}
	}*/
	
	// remove absorbed groups (and their references)
	/*for (SliceGroup &g : groups) {
		if (g.parallel && g.parallelGroup->absorbed) {
			g.parallelGroup = nullptr;
			g.parallel = false;
		}
		if (g.connectedToP0 != nullptr && g.connectedToP0->absorbed) {
			g.connectedToP0 = nullptr;
			g.p0ToEndpoint = false;
		}
		if (g.connectedToP1 != nullptr && g.connectedToP1->absorbed) {
			g.connectedToP1 = nullptr;
			g.p1ToEndpoint = false;
		}
	}
	groups.remove_if([](SliceGroup &g){ return (g.absorbed); });*/
	
	//printf("Extruded and connected %d segments with disconected endpoints.\n", n);
}

/**
 * Connects all disconnected wall endpoints that are remarkably close to each other
 */
void GroupManager::connectCloseEndpoints() {
	/*printf("Connecting close disconnected endpoints...\n");

	int n = 0;
	runForAllGroupPairs(groups, [this, &n](SliceGroup *gi, SliceGroup *gj)-> bool {
		// wall connected groups that have at least one free endpoint
		if (!gi->connectedWall || gi->countDisconnectedEndpoints() == 0) return false;
		if (!gj->connectedWall || gj->countDisconnectedEndpoints() == 0) return true;

		Segment &si = gi->seg, &sj = gj->seg;
		float tMax = 0.6f * max<float>(si.thickness, sj.thickness);

		// break if closest endpoints are not disconnected
		uchar endi, endj;
		float dist = closestEndpoints(*gi, *gj, endi, endj);
		if (!gi->isEndDisconnected(endi) && !gj->isEndDisconnected(endj)) return true;

		Point2f &pi = (endi == '0') ? si.p0 : si.p1;
		Point2f &pj = (endj == '0') ? sj.p0 : sj.p1;
		printf("Connecting pi%c (%.1f, %.1f) to pi%c (%.1f, %.1f)\n", endi, pi.x, pi.y, endj, pj.x, pj.y);

		// If endpoints are close, connect them
		if (dist <= tMax) {
			n++;
			Point2f cross = segmentCrossPoint(si, sj);
			pi = cross;
			pj = cross;
			si.update(false);
			sj.update(false);
			gi->registerConnectedEndpoint(gj, cross, endi);
			gj->registerConnectedEndpoint(gi, cross, endj);
			return false;
		}

		return true;
	});

	printf("Connected %d close endpoints.\n", n);*/
}

/**
 * Returns the point where the projections of s0 and s1 cross each other
 * @param s0
 * @param s1
 * @return Point2f, (0,0) if they are parallel, can be out of the image or negative
 */
Point2f GroupManager::segmentCrossPoint(Segment &s0, Segment &s1) {
	if (s0.p0.x == s0.p1.x) {
		return Point2f(s0.p0.x, s1.getYforX(s0.p0.x));
	} else if (s1.p0.x == s1.p1.x) {
		return Point2f(s1.p0.x, s0.getYforX(s1.p0.x));
	} else {
		return geo::rectCrosspoint(s0.slope, s0.offset, s1.slope, s1.offset);
	}
}

/**
 * Returns a percentage [0.0, 1.0] of the black pixels in a line between p0 and p1
 * @param p0
 * @param p1
 * @return
 */
float GroupManager::blackPixelsInLine(Mat &im, Point2f p0, Point2f p1, uchar bgColor) {
	double dx = 0.f, dy = 0.f;
	float w = fabs(p1.x - p0.x), h = fabs(p1.y - p0.y);
	float l = geo::euclideanDistance(p0, p1);
	int step = 0, maxSteps = int(l);
	Point2f p;

	// get displacement and initial position
	if (p0.y == p1.y) {
		dx = 1.f;
		p = Point2f(int(p0.x) + 0.5f, p0.y);
	}
	else if (p0.x == p1.x) {
		dy = 1.f;
		p = Point2f(p0.x, int(p0.y) + 0.5f);
	}
	else {
		float x0, y0;
		double m, b;
		geo::points2LineEquation(p0, p1, m, b);
		if (w > h) {
			dx = 1.;
			dy = m;
			x0 = int(p0.x) + 0.5f;
			p = Point2f(x0, geo::yForXinRect(m, b, x0));
		}
		else {
			dy = 1.;
			dx = -1. / m;
			y0 = int(p0.y) + 0.5f;
			p = Point2f(geo::xForYinRect(m, b, y0), y0);
		}
	}

	// scan and count
	int blacks = 0;
	for (step = 0 ; step < maxSteps ; step++) {
		if (im.at<uchar>(int(p.y), int(p.x)) != bgColor) {
			blacks++;
		}
		p.x += dx;
		p.y += dy;
	}

	return float(blacks) / l;
}

/**
 * Predicts normals for all connected segments
 */
void GroupManager::updateWallNormals() {
	printf("Finding the normal direction for every wall line...\n");

	/*Point2f n, nSeg, diff;
	double mag;
	bool normalRightOrdered;
	float overlap, distance;
	Point2f direction;
	SliceGroup *other;

	// Correct normals for line segments that lost their parallel
	for (SliceGroup &g : groups) {
		if (g.parallel && g.parallelGroup->parallel) {
			other = g.parallelGroup;
			if (other->parallelGroup != &g) {
				if (!other->parallelGroup->parallel) {
					// re-register parallel
					other->parallelGroup = nullptr;
					other->parallel = false;
					overlap = geo::distanceBetweenParallelRects(g.seg.p0, g.seg.p1, other->seg.p0, other->seg.p1, distance, direction);
					other->registerParallelGroup(&g, distance, overlap, -direction);

					//printf("weird case parallel with other parallel=%d, absorbed=%d, connectedWall=%d\n", other->parallelGroup->parallel, other->parallelGroup->absorbed, other->parallelGroup->connectedWall);
					//other->seg.print();
				}
			}
		}
	}

	// Parallel lines carry trustable normals so we propagate them
	for (SliceGroup &g : groups) {
		if (g.parallel && (g.p0ToEndpoint || g.p1ToEndpoint)) {
			n = g.normal;
			nSeg = g.seg.getNormal(true);

			// find order by comparing n to the forward-order p0->p1 normal of the segment
			//printf("g.normal = (%.2f, %.2f), g.seg.normal = (%.2f, %.2f)\n", n.x, n.y, nSeg.x, nSeg.y);
			diff = n - g.seg.getNormal(true);
			mag = geo::vectorMagnitude(diff);
			normalRightOrdered = (mag < 0.1); // true = forward (p0->p1)
			//printf("normalRightOrdered: %d\n", normalRightOrdered);

			// propagate order through both endpoints
			if (g.p1ToEndpoint) propagateNormals(&g, g.connectedToP1, normalRightOrdered);
			if (g.p0ToEndpoint) propagateNormals(&g, g.connectedToP0, normalRightOrdered);
		}
	}*/
}

// removes all non-wall segments from the list
void GroupManager::purgeGroups() {
	groups.remove_if([](SliceGroup &g) {
		return !g.enabled;
	});

	// Reset ids (base 0)
	ushort idx = 0;
	for (SliceGroup &g : groups) {
		g.id = idx;
		idx++;
	}

}

void GroupManager::propagateNormals(SliceGroup *last, SliceGroup *next, bool prevIsOrdered) {
	/*bool nextOrder, sameNodeConnected, nextIsP0;
	float parallelDist = last->minParallelDist;

	while (next != nullptr && !next->parallel && geo::isZero(next->normal)) {
		// Find the propagated order
		sameNodeConnected = ((last->connectedToP1 == next && next->connectedToP1 == last) || (last->connectedToP0 == next && next->connectedToP0 == last));
		nextOrder = (sameNodeConnected) ? !prevIsOrdered : prevIsOrdered;

		// Set the propagated normal and related properties
		next->normal = next->seg.getNormal(nextOrder);
		next->minParallelDist = parallelDist;

		// advance
		nextIsP0 = (next->connectedToP1 == last);
		last = next;
		prevIsOrdered = nextOrder;
		next = (nextIsP0) ? next->connectedToP0 : next->connectedToP1;
	}*/
}


void GroupManager::paintWalls(Mat &imgOut) {
	imgOut = Mat(img->rows, img->cols, CV_8U, Scalar(0));
	for (SliceGroup &g : groups) {
		if (!geo::isZero(g.normal)) {

		}
	}
}

// connect candidate parallels using virtual walls
void GroupManager::virtualConnectParallels(Mat &imgWalls) {
	printf("Creating virtual walls...\n");
	Mat imd = imgWalls.clone();

	// Find the longest wall segment found and use it as length threshold
	maxSegmentLength = 0;
	for (SliceGroup &g : groups) {
		if (g.isLabeledWall() && g.seg.length > maxSegmentLength) maxSegmentLength = g.seg.length;
	}

	// Search for the best connection candidate pairs and store the best ones
	runForAllGroupPairs(groups, [this, &imd, &imgWalls](SliceGroup *gi, SliceGroup *gj)-> bool {
		// groups must be part of a wall
		if (!gi->isLabeledWall()) return false;
		if (!gj->isLabeledWall()) return true;
		// groups must not be connected
		if	(gi->connectedToP0 == gj || gi->connectedToP1 == gj || gj->connectedToP0 == gi || gj->connectedToP1 == gi) return true;

		virtualWallIfColinear(*gi, *gj, maxSegmentLength, imgWalls, imd);
		return true;
	});

	// Store the virtual segments in a different list
	/*WallSegment gv;
	for (SliceGroup &g : groups) {
		if (g.parallel) {
			if (g.minColinearDistP0 > 0) {
				gv = WallSegment(g.seg.p0, g.colinearP0, g.normal);
			}
		}
	}*/
}

// connect small gaps
void GroupManager::virtualConnectSmallGaps(Mat &imgWalls) {
	printf("Virtual walls for small gaps...\n");
	// DONT
}

// If g and p are colinear, nearby, uninterrupted, good candidates, try to link them with a virtual wall
// Return true if a virtual wall was created
bool GroupManager::virtualWallIfColinear(SliceGroup &gi, SliceGroup &gj, float maxLength, Mat &imgWalls, Mat &imd) {
	/*Segment &si = gi.seg, &sj = gj.seg;

	// Find the closest endpoints and distance
	uchar endi, endj;
	float d, tolerance;
	Point2f pi, pj;
	d = closestEndpoints(gi, gj, endi, endj);

	// guard against endpoints connected to other segments' bodies
	if (gi.isEndConnectedToBody(endi)) return false;

	gi.getPresentationEndpoint(endi, pi);
	gj.getPresentationEndpoint(endj, pj);

	if (d >= maxLength || d >= 10 * max<float>(si.length, sj.length)) return false; // too far
	if (gi.normal.dot(gj.normal) < 0) return false; // angle between normals should be <= 90 degrees

	// try connecting from gi
	if (gi.parallel && gi.isCloserColinearPoint(pj, d, endi)) {
		tolerance = si.thickness;
		if (gi.isEndDisconnected(endi) || gi.isEndToEnd(endi)) {
			if (si.isCollinearTo(pj, tolerance)) {
				if (uninterruptedPath(imgWalls, pi, pj, tolerance)) return gi.registerColinearEndpoint(pj);
			}
		}
	}

	// try connecting from gj
	if (gj.parallel && gj.isCloserColinearPoint(pi, d, endj)) {
		tolerance = sj.thickness;
		if (gj.isEndDisconnected(endj) || gj.isEndToEnd(endj)) {
			if (sj.isCollinearTo(pi, tolerance)) {
				if (uninterruptedPath(imgWalls, pi, pj, tolerance)) return gj.registerColinearEndpoint(pi);
			}
		}
	}

	return false;*/
}

// force connect with a wall segment to closest free endpoint
void GroupManager::wallConnectFreeEndpoints(Mat &imgExtrusion) {
	/*runForAllGroupPairs(groups, [this, &imgExtrusion](SliceGroup *gi, SliceGroup *gj)-> bool {
		// groups must both have a free endpoint
		if (gi->connectedToP0 != nullptr && gi->connectedToP1 != nullptr) return false;
		if (gj->connectedToP0 != nullptr && gj->connectedToP1 != nullptr) return true;

		Point2f pi, pj;
		// if groups are parallel, join and we're done
		if (gi->parallelGroup == gj || gj->parallelGroup == gi) {
			//SliceGroup gNew = SliceGroup();
			//this->groups.push_back();
		}

		//virtualWallIfColinear(*gi, *gj, maxLength, imgWalls, imd);
		return true;
	});*/
}


// search for closed loops filled with text and remove them
void GroupManager::filterTextContainers(Mat &imgText) {
	printf("Removing geometrical shapes with text inside them...\n");

	SliceGroup *gi, *g0, *g1, *g0next, *g1next;
	vector<Point2f> points;
	Point2f pMin, pMax;
	Mat roi;
	float textPxCount, textRatio;

	for (SliceGroup &g : groups) {
		// double connected wall component
		if (!g.closedLoop && g.p0ToEndpoint && g.p1ToEndpoint && g.isLabeledWall()) {
			//printf("\nChecking group:\n");
			//g.seg.print();

			gi = &g;
			g0 = g.connectedToP0;
			g1 = g.connectedToP1;
			g0next = (g0->connectedToP0 != gi) ? g0->connectedToP0 : g0->connectedToP1;
			g1next = (g1->connectedToP0 != gi) ? g1->connectedToP0 : g1->connectedToP1;

			if (g0next == g1next && g0next != nullptr) {
				// g0next is endpoint connected?
				if (g0next->isEndConnectedTo(g0) && g0next->isEndConnectedTo(g1)) {
					// Found a 4 vertex loop
					gi->closedLoop = true;
					g0->closedLoop = true;
					g1->closedLoop = true;
					g0next->closedLoop = true;

					// build ROI from segments
					points.clear();
					gatherConnectedPoints(gi, points, 4);
					pMin.x = 9999.f; pMin.y = 9999.f;
					pMax.x = 0; pMax.y = 0;
					for (Point2f &p : points) {
						if (p.x < pMin.x) pMin.x = p.x;
						if (p.y < pMin.y) pMin.y = p.y;
						if (p.x > pMax.x) pMax.x = p.x;
						if (p.y > pMax.y) pMax.y = p.y;
					}
					/*printf("Found loop: ");
					for (Point2f &p : points) { printf("(%.2f, %.2f) ", p.x, p.y); }
					printf("\n");*/

					// Avoid empty MBRs
					if (pMin.x != pMax.x || pMin.y != pMax.y) {
						roi = imgText(Rect(pMin, pMax));

						// count text pixel ratio
						textPxCount = px::countPixels(roi, uchar(1));
						textRatio = textPxCount / float(roi.rows * roi.cols);
						/*printf("roi size %d x %d\n", roi.cols, roi.rows);
						printf("Text count: %.3f\n", textPxCount);
						printf("Text ratio: %.3f\n", textRatio);*/

						// threshold pixel ratio on text
						if (textRatio > 0.05f) {
							gi->disable();
							g0->disable();
							g1->disable();
							g0next->disable();
						}
					}
				}
			}
		}
	}
}

void GroupManager::gatherConnectedPoints(SliceGroup *gi, vector<Point2f> &points, int maxPoints) {
	SliceGroup *gNext, *gLast;
	Point2f p;
	uchar end;

	gi->getPresentationEndpoint('1', p);
	points.push_back(p);
	gi->getPresentationEndpoint('0', p);
	points.push_back(p);

	// Gather points towards g.p0
	gLast = gi;
	gNext = gi->connectedToP0;
	int count = 2; // point count

	if (gi->p0ToEndpoint) {
		while (gNext != nullptr && gNext != gi && count < maxPoints) {
			// store next point
			end = (gNext->connectedToP0 != gLast) ? '0' : '1';
			gNext->getPresentationEndpoint(end, p);
			points.push_back(p);
			count++;

			if (!gNext->isEndToEnd(end)) break;

			gLast = gNext;
			gNext = (end == '0') ? gNext->connectedToP0 : gNext->connectedToP1;
		}
	}

	if (gNext == gi) return;
}

// Remove connected loops that don't look like a wall
void GroupManager::filterWallConnectedShapes() {
	/*printf("Filtering lines connected to walls that don't behave like walls...\n");

	for (SliceGroup &g : groups) {
		// non-parallel connected walls
		if (g.connectedWall && !g.parallel && !g.parallelSeriesConnected) {
			// with 1 free endpoint, not many groups connected to it, not endpoint-connected to a parallel
			if ( g.connectedGroups.size() < 4) {
				g.disable();
				g.type = 9;
			}
		}
	}*/
}

// Updates wall segment labels to reflect current state
void GroupManager::updateWallLabels() {
	/*printf("Updating wall properties...\n");

	for (SliceGroup &g : groups) {
		if (g.parallel) {
			// make sure connected segments still exist, update otherwise

			// propagate connection labels to non-parallels
			if (g.isEndToEnd('0') && !g.connectedToP0->parallel) {
				propagateWallConnectedLabel(&g, g.connectedToP0, true, true);
			}
			if (g.isEndToEnd('1') && !g.connectedToP1->parallel) {
				propagateWallConnectedLabel(&g, g.connectedToP1, true, true);
			}
		}
	}*/
}

/**
 * Returns in parameters endi, endj the closest endpoints {'0', '1'} between gi and gj; returns from function the shortest distance found
 * @param gi
 * @param gj
 * @param endi out {'0', '1'}
 * @param endj out {'0', '1'}
 * @return distance between endi and endj
 */
float GroupManager::closestEndpoints(SliceGroup &gi, SliceGroup &gj, uchar &endi, uchar &endj) {
	float d, d00, d01, d10, d11;
	d00 = geo::euclideanDistance(gi.seg.p0, gj.seg.p0);
	d01 = geo::euclideanDistance(gi.seg.p0, gj.seg.p1);
	d10 = geo::euclideanDistance(gi.seg.p1, gj.seg.p0);
	d11 = geo::euclideanDistance(gi.seg.p1, gj.seg.p1);
	if (d00 <= d01 && d00 <= d10 && d00 <= d11)      { d = d00; endi = '0'; endj = '0'; }
	else if (d01 <= d00 && d01 <= d10 && d01 <= d11) { d = d01; endi = '0'; endj = '1'; }
	else if (d10 <= d00 && d10 <= d01 && d10 <= d11) { d = d10; endi = '1'; endj = '0'; }
	else  { d = d11; endi = '1'; endj = '1'; }
	return d;
}

// extrudes channel ch image pixels with direction dx, dy and backwards
void GroupManager::extrudeChannel(Mat &imgChan, Mat &imgExtrusion, uchar orientation, uchar minNeighs) {
	// Get encoded version
	Mat imgGreen;
	green::encodeMatrixForeground(imgChan, imgGreen, 0);

	// select directions
	uchar dirFront, dirBack;
	int dx, dy;

	switch (orientation) {
		case DIR_HORIZONTAL:    dirFront = GL_E;  dirBack = GL_W;  dx = 1; dy = 0; break;
		case DIR_VERTICAL:      dirFront = GL_S;  dirBack = GL_N;  dx = 0; dy = 1; break;
		case DIR_DIAGONAL_INCR: dirFront = GL_NE; dirBack = GL_SW; dx = 1; dy = -1; break;
		case DIR_DIAGONAL_DECR: dirFront = GL_SE; dirBack = GL_NW; dx = 1; dy = 1; break;
	}

	// extrude
	std::mutex mtx;
	imgGreen.forEach<uchar>([orientation, &mtx, &imgChan, &imgExtrusion, dirFront, minNeighs, dirBack, dx, dy, this](uchar &code, const int* pos) -> void {
		if (code > 0) {
			uchar neighCount = green::neighborCount(code);
			//printf("(%d, %d) = %d, neighs %d\n", pos[1], pos[0], code, neighCount);
			if (neighCount >= minNeighs && neighCount < 8) {
				int x = pos[1], y = pos[0];
				bool front = green::getNeighbor(code, dirFront);
				bool back = green::getNeighbor(code, dirBack);
				//printf("(%d,%d) %d neighs for code %d with front(%d) %d and back(%d) %d", x, y, neighCount, code, dirFront, front, dirBack, back);

				lock_guard<mutex> guard(mtx);
				if (back && !front) {
					this->projectPixelInsideLine(imgChan, imgExtrusion, x, y, dx, dy); // project forward
				}
				else if (front && !back) {
					this->projectPixelInsideLine(imgChan, imgExtrusion, x, y, -dx, -dy); // project back
				}
			}
		}
	});
}

// projects pixel in (x,y) in im with increment (dx, dy) as long as the next pixel is black in the original img
// im should be a CV_8U B&W (intended to be used with the channel mats)
void GroupManager::projectPixelInsideLine(Mat &imgCh, Mat &imgExtrusion, int x, int y, int dx, int dy) {
	//imgCh.at<uchar>(y, x) = 2;
	x += dx; y += dy;
	while (px::isPixelInsideMat(x, y, imgCh)) {
		if (imgExtrusion.at<uchar>(y, x) == 0) break; // extrusion space must not be empty here
		if (imgCh.at<uchar>(y, x) != 0) break; // channel must be empty (0) here

		imgCh.at<uchar>(y, x) = 1;
		x += dx;
		y += dy;
	}
}


void GroupManager::subdivideLines() {
	printf("Subdividing line bodies with connections...\n");

	int n = 0;
	for (SliceGroup &g : groups) {
		if (g.enabled && g.isParallel()) {
			for (ConnectRelation &r : g.connP0) {
				if (!r.toEndpoint) {
					SliceGroup sg;
					if (r.other->subdivideAt(r.cross, sg)) { addGroup(sg); n++; }
				}
			}

			for (ConnectRelation &r : g.connP1) {
				if (!r.toEndpoint) {
					SliceGroup sg;
					if (r.other->subdivideAt(r.cross, sg)) { addGroup(sg); n++; }
				}
			}
		}
	}
	printf("...%d subdivisions.\n", n);
}
