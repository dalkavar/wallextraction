#ifndef SLICE_H
#define SLICE_H

#define DIR_HORIZONTAL    0 // horizontal direction
#define DIR_VERTICAL      1 // vertical
#define DIR_DIAGONAL_INCR 2 // diagonal increasing slope
#define DIR_DIAGONAL_DECR 3 // diagonal decreasing slope
#define SLICE_HORIZONTAL 0
#define SLICE_VERTICAL 1

#include <opencv2/core.hpp>
#include <vector>
#include "util/segment.h"

using namespace cv;
using std::vector;

// representation of a slice for Extractor
// can scan itself
class Slice {
	void updateCenter();
	void getRange(float cx, float cy, uchar l, Point &first, Point &last);

public:
	Point start, end;
	Point2f center; // sub-pixel precision center
	uchar length = 0;
	char sdx = 0, sdy = 0;// slice direction
	uchar type; // 0=x, 1=y, 2=d1, 3=d2
	bool stableLength = false; // true if length is common in the slice group

	Slice();
	Slice(Mat &img, int x, int y, uchar type);

	void setType(uchar type);
	bool forceAdvance(Mat &img, float dx, float dy);
	void advanceAndScan(Mat &img, char dx, char dy);
	void scan(Mat &img, int x, int y);
	int scanCentered(Mat &img, float x, float y, uchar l);
	float forceScan(Mat &img, float x, float y, int forceLength);

	template <class T> void paint(Mat &img, ushort color);
	void stabilizeWithNeighbors(Slice &prev, Slice &next);
	void stabilize(Mat *imgExtrude, uchar mainLength, Segment &seg);
	Point2f getFloatStart();
	Point2f getFloatEnd();

	void print();
};

#endif // SLICE_H
