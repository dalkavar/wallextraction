#ifndef GROUPMANAGER_H
#define GROUPMANAGER_H

#include <list>
#include <iostream>
#include <fstream>
#include "opencv2/imgproc.hpp"
#include "slicegroup.h"
#include "slice.h"
#include <util/display.h>
#include <util/plothist.h>
#include <util/stats.h>
#include <util/greenlee.h>
#include <common/WallSegment.h>

using namespace cv;
using std::vector;


class GroupManager {
	const int MIN_GROUP_SLICES = 3; // Minimum slice number a group needs to be added
	const float MAX_DIF_PARALLEL_ANGLES = 3; // Maximum angle (degrees) difference to consider 2 angles parallel
	const float MIN_DIST_ENDPOINT = 3.f; // Minimum distance between connected endpoints
	const float MIN_LINE_LENGTH = 3.f; // Minimum line length to be considered a possible wall segment

	Mat *img; // original binary {0, 1} image

	float maxParallelDistance = 0; // Hard limit on parallel segment distance

	bool groupsHaveSimilarAngle(SliceGroup &g1, SliceGroup &g2, float tolerance);
	void propagateWallConnectedLabel(SliceGroup *prev, SliceGroup *next, bool connectedLabel, bool parallelSeriesLabel);
	Point2f segmentCrossPoint(Segment &s0, Segment &s1);
	float blackPixelsInLine(Mat &im, Point2f p0, Point2f p1, uchar bgColor);
	void propagateNormals(SliceGroup *last, SliceGroup *next, bool prevIsOrdered);
	bool virtualWallIfColinear(SliceGroup &gi, SliceGroup &gj, float maxLength, Mat &imgWalls, Mat &imd);
	float closestEndpoints(SliceGroup &gi, SliceGroup &gj, uchar &endi, uchar &endj);


public:
	list<SliceGroup> groups; // slice groups for all channels
	list<WallSegment> virtuals; // slice groups for all virtual walls
	float medianParallelDistance = 0;
	float maxSegmentLength = 0; // max segment length found
	string outPath;

	void extrudeChannel(Mat &imgChan, Mat &imgExtrusion, uchar orientation, uchar minNeighs);
	void projectPixelInsideLine(Mat &im, Mat &imgExtrusion, int x, int y, int dx, int dy);

	GroupManager(Mat *img);
	void addGroup(SliceGroup &g);
	void addGroup(Segment seg, Point2f normal, float minParallelDist);
	void addGroupsVector(vector<SliceGroup> &grs);

	// Geometrical filters
	void detectParallels(bool onlyWalls);

	void filterParallelsByDistance();
	void filterIsolatedParallels();
	void filterOverlappedParallels();

	void detectSegmentConnections();
	void labelWallConnectedEndpoints();
	void subdivideLines();

	void mergeGroups(SliceGroup* g0, SliceGroup* g1);
	void mergeAllCloseColinearGroups(vector<SliceGroup> &grs);
	void extrudeColinearEndpoints(Mat &im);
	void connectCloseEndpoints();
	void filterTextContainers(Mat &imgText);

	void updateWallNormals();
	void updateWallLabels();
	void virtualConnectParallels(Mat &imgWalls);
	void virtualConnectSmallGaps(Mat &imgWalls);

	void paintColorGroups(Mat &imgOut, list<SliceGroup> &grs);
	void paintWalls(Mat &imgOut);
	void writeSegmentsToFile();
	void readSegmentsFromFile();
	void writeRoomsToFile(vector<vector<Point> > &contours);

	template<typename Func> void runForAllGroupPairs(vector<SliceGroup> &grs, Func f);
	template<typename Func> void runForAllGroupPairs(list<SliceGroup> &grs, Func f);

	void purgeGroups();

	bool uninterruptedPath(Mat &img, Point2f &p0, Point2f &p1, float tolerance);
	void gatherConnectedPoints(SliceGroup *gi, vector<Point2f> &points, int maxPoints);

	void wallConnectFreeEndpoints(Mat &imgExtrusion);

	void filterWallConnectedShapes();
};

#endif // GROUPMANAGER_H
