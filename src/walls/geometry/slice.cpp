#include "slice.h"

// updates center
void Slice::updateCenter() {
	if (length == 1) {
		center = getFloatStart();
	} else if (length > 1) {
		center.x = 0.5 + float(start.x + end.x) / 2.0;
		center.y = 0.5 + float(start.y + end.y) / 2.0;
	}
}

// Returns in first and last the pixels where a slice centered in (cx, cy) of l length would be scanned
// Does not modify the slice object
void Slice::getRange(float cx, float cy, uchar l, Point &first, Point &last) {
	// find integer start and end coords for slice
	float radius = float(l) / 2.0;
	float x0 = cx - float(sdx) * radius;
	float y0 = cy - float(sdy) * radius;
	float x1 = cx + float(sdx) * radius;
	float y1 = cy + float(sdy) * radius;

	if (type == 0) { // horizontal
		first.x = ((x0 - int(x0)) > 0.5) ? ceil(x0) : floor(x0);
		first.y = floor(y0);
		last.x   = ((x1 - int(x1)) > 0.5) ? floor(x1) : floor(x1) - 1;
		last.y   = first.y;
	}
	else { // vertical
		first.x = floor(x0);
		first.y = ((y0 - int(y0)) > 0.5) ? ceil(y0) : floor(y0);
		last.x   = first.x;
		last.y   = ((y1 - int(y1)) > 0.5) ? floor(y1) : floor(y1) - 1;
	}
}

Slice::Slice() {
}

// on creation, the slice scans itself
// type is: 0=horizontal, 1=vertical, 2=D1, 3=D2
// img should have a 1px empty border, and 1s for unexplored pixels
Slice::Slice(Mat &img, int x, int y, uchar type) {
	setType(type);
	scan(img, x, y);
}

// stores slice type and computes sdx sdy (slice parallel displacements)
void Slice::setType(uchar type) {
	sdx = 0, sdy = 0;
	this->type = type;
	switch (type) {
		case 0: sdx = 1; break;
		case 1: sdy = 1; break;
		case 2: sdx = 1; sdy = -1; break;
		case 3: sdx = 1; sdy = 1; break;
	}
}

// Displaces current slice with step (dx, dy)
// Returns true if a valid slice was found
bool Slice::forceAdvance(Mat &img, float dx, float dy) {
	return (forceScan(img, center.x + dx, center.y + dy, length));
}

// scans slice at (x, y), must not be empty
// sdx, sdy must be already initialized
// scanned pixels are labeled "2"
void Slice::scan(Mat &img, int x, int y) {
	length = 1;

	//printf("Scanning slice from (%d,%d)\n", x, y);
	img.at<uchar>(y, x) = 2;

	// move start backwards
	start.x = x; start.y = y;
	while (img.at<uchar>(start.y - sdy, start.x - sdx) == 1) {
		start.x -= sdx;
		start.y -= sdy;
		img.at<uchar>(start) = 2;
		//printf("BW Scanned pixel (%d,%d)\n", start.x, start.y);
		length++;
	}

	// move end forward
	end.x = x; end.y = y;
	while (img.at<uchar>(end.y + sdy, end.x + sdx) == 1) {
		end.x += sdx;
		end.y += sdy;
		img.at<uchar>(end) = 2;
		//printf("FW Scanned pixel (%d,%d)\n", end.x, end.y);
		length++;
	}

	// make sure start goes before end
	if (start.x > end.x || start.y > end.y) {
		cv::swap<Point>(this->start, this->end);
		//printf("after swap: (%d,%d)-(%d,%d)\n", start.x, start.y, end.x, end.y);
	}

	updateCenter();
}

// Scans this slice of length at img around center (x,y)
// type should be set before
int Slice::scanCentered(Mat &img, float x, float y, uchar l) {
	//printf("Re-scanning slice centered at (%.1f, %.1f) length:%d sdx:%d sdy:%d\n", x, y, l, sdx, sdy);
	center.x = x; center.y = y;

	// get maximum first and last pixels
	Point first, last;
	getRange(x, y, l, first, last);
	//printf("Range is (%d,%d) and (%d, %d)\n", first.x, first.y, last.x, last.y);

	// start at center point
	start = Point(floor(x), floor(y));
	end = start;
	length = 0;
	if (img.at<uchar>(start) == 0) return 0; // center empty means no slice

	length++;

	// scan backwards until first or empty
	//printf("start(%d, %d)\n", start.x, start.y);
	while ((start != first) && (img.at<uchar>(start.y - sdy, start.x - sdx) != 0)) {
		start.x -= sdx;
		start.y -= sdy;
		length++;
		//printf("start moved to (%d, %d)\n", start.x, start.y);
	};

	// scan forward
	while ((end != last) && (img.at<uchar>(end.y + sdy, end.x + sdx) != 0) && (length < l)) {
		end.x += sdx;
		end.y += sdy;
		length++;
		//printf("end moved to (%d, %d)\n", end.x, end.y);
	};

	//printf("got %d pixels\n", length);
	updateCenter();

	return length;
}

// forces slice to be scanned around center (x,y) with forceLength
// doesn't modify img (intended for extrusion space)
// returns percentage [0.0, 1.0] of non-empty pixels found
float Slice::forceScan(Mat &img, float x, float y, int forceLength) {
	//printf("forceScan at (%.2f, %.2f) with length %d...\n", x, y, forceLength);
	float pixelCount = 0.f;
	center.x = x; center.y = y;
	getRange(x, y, forceLength, start, end);
	//printf("Slice between (%d,%d) and (%d, %d)\n", start.x, start.y, end.x, end.y);
	length = forceLength;

	// Guard against invalid slice range
	if (start.x < 0 || start.y < 0 || end.x < 0 || end.y < 0) return 0.f;
	if (start.x >= img.cols || start.y >= img.rows || end.x >= img.cols || end.y >= img.rows) return 0.f;

	// Force full length scan slice and return percentage of non-empty pixels found
	for (int i = 0 ; i < length ; i++) {
		if (img.at<uchar>(start.y + i*sdy, start.x +i*sdx) > 0) pixelCount++;
	}
	//printf("got %.0f pixels.\n", pixelCount);

	return pixelCount / length;
}

// stabilizes a slice to match it's immediate neighbors
void Slice::stabilizeWithNeighbors(Slice &prev, Slice &next) {
	int psx = prev.start.x, psy = prev.start.y, pex = prev.end.x, pey = prev.end.y;
	int nsx = next.start.x, nsy = next.start.y, nex = next.end.x, ney = next.end.y;

	if (prev.start.x > prev.end.x || prev.start.y > prev.end.y) {
		printf("PREV FKIN BROKEN\n");
	}

	switch (type) {
	case 0: // x
		start.x = (psx == nsx) ? psx : max<int>(psx, nsx);
		end.x = (pex == nex) ? pex : min<int>(pex, nex);
		length = uchar(end.x - start.x + 1);
		break;
	case 1: // y
		start.y = (psy == nsy) ? psy : max<int>(psy, nsy);
		end.y = (pey == ney) ? pey : min<int>(pey, ney);
		length = uchar(end.y - start.y + 1);
		break;
	default:
		// decide what to do with diagonals
		break;
	}

	updateCenter();
	//printf("Stabilized slice betwen (%d,%d)-(%d,%d) and (%d,%d)-(%d,%d) to (%d,%d)-(%d,%d)\n", psx, psy, pex, pey, nsx, nsy, nex, ney, start.x, start.y, end.x, end.y);
	//printf("New length: %d\n", length);
}

// Paints this slice into Binary {0, 1} img as pixels with color
template <class T>
void Slice::paint(Mat &img, ushort color) {
	Point p = start;
	int n = 0;
	while (n < length) {
		img.at<T>(p) = color;
		p.x += sdx;
		p.y += sdy;
		n++;
	}
}

// Return sub-pixel precision points for the center point of start and end pixel coords
Point2f Slice::getFloatStart() {
	return Point2f(start.x + 0.5, start.y + 0.5);
}

Point2f Slice::getFloatEnd() {
	return Point2f(end.x + 0.5, end.y + 0.5);
}

void Slice::print() {
	printf("Slice { (%d,%d)-(%d,%d) center(%.1f,%.1f) length:%d, sd:%d|%d, type:%d, stable:%d }\n", start.x, start.y, end.x, end.y, center.x, center.y, length, sdx, sdy, type, stableLength);
}


// tries to advance and scan the slice after offset dx, dy
// count is 0 if it fails
void Slice::advanceAndScan(Mat &img, char dx, char dy) {
	int x2, y2;
	//printf("advancing slice with d (%d, %d)\n", dx, dy);

	// try to find a center pixel from the next slice
	if (length == 1) {
		x2 = start.x + dx; y2 = start.y + dy;
	} else {
		if (start.x == end.x) {
			x2 = start.x + dx;
		} else {
			x2 = floor(0.5 * float(start.x + end.x) + dx);
		}

		if (start.y == end.y) {
			y2 = start.y + dy;
		} else {
			y2 = floor(0.5 * float(start.y + end.y) + dy);
		}
	}

	// if new slice seed is empty, look for a connected pixel
	if (img.at<uchar>(y2, x2) == 0 && length > 1) {
		//printf("advance seed (%d, %d) invalid, looking for a new seed...\n", x2, y2);
		uchar n = 0;
		x2 = start.x + dx;
		y2 = start.y + dy;

		while (n <= length && img.at<uchar>(y2, x2) == 0) {
			x2 += sdx;
			y2 += sdy;
			n++;
		}

		if (n > length) {
			//printf("No valid seed found.\n");
			length = 0;
			return; // no slice found
		}
	}
	//printf("scanning sclice with seed (%d, %d)\n", x2, y2);
	scan(img, x2, y2);
}


// template definitions to avoid compiler errors
template void Slice::paint<uchar>(Mat &img, ushort color);
template void Slice::paint<ushort>(Mat &img, ushort color);
