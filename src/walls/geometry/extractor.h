#ifndef EXTRACTOR_H
#define EXTRACTOR_H

#include <mutex>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "util/display.h"
#include "util/px.h"
#include "slicegroup.h"

using namespace cv;
using std::vector;

// Extracts SliceGroups from a channel {0,1,2} image
// Meant to work on a single channel obtained by SliceTransform
class Extractor {
	const int T_MIN_SLICES_EXTRUSION = 3; // Min number of slices to allow to extrude a group

	Mat img; // CV_8U Mat channel to work on; 0=empty, 1=filled non-scanned, 2=filled scanned
	Mat *imgExtrude; // CV_8U Binary {0, 1} image with the extrusion space

	void getGroupAt(int x, int y, uchar sliceType, char chDx, char chDy);
	void detectAllGroups(uchar direction);
	void stabilizeAllGroups();
	void extrudeAllGroups(Mat &imgExtrude);

public:
	vector<SliceGroup> groups; // slice groups detected

	Extractor(Mat *imgChannel, Mat *imgExtrude);
	void runFullExtraction(uchar direction, Mat *imgExtrusion);
};

#endif // EXTRACTOR_H
