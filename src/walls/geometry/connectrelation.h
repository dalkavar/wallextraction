#ifndef CONNECTRELATION_H
#define CONNECTRELATION_H
#include "slicegroup.h"

class SliceGroup;

/**
 * Stores a relationship from this SliceGroup that connects one endpoint to another SliceGroup
 */
class ConnectRelation {
public:
	SliceGroup *other;
	bool toEndpoint = false;
	char otherEnd = -1;
	Point2f cross; // cross point between both segments

	ConnectRelation(SliceGroup *other, Point2f &cross, char otherEnd) {
		this->other = other;
		this->cross = cross;
		if (otherEnd != -1) {
			this->otherEnd = otherEnd;
			toEndpoint = true;
		}
	}
};

#endif //CONNECTRELATION_H
