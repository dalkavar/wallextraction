#include "extractor.h"

// needs a pointer to the channel image (imgBW), the extrusion space (imgExtrude)
Extractor::Extractor(Mat *imgBW, Mat *imgExtrude) {
	img = imgBW->clone(); // we store a copy to modify
	this->imgExtrude = imgExtrude;
}

// scans group with seed (x, y) and stores it in groups
void Extractor::getGroupAt(int x, int y, uchar sliceType, char chDx, char chDy) {
	//printf("scanning group at pixel (%d, %d)\n", x, y);
	SliceGroup g = SliceGroup(img, *imgExtrude, x, y, sliceType, chDx, chDy);

	// we only store valid stable slice groups
	if (g.isLine && g.slices.size() > 3) {
		if (g.mainLengths.size() <= 2 && g.mainLengths[0] > 1) {
			g.id = groups.size() + 1;
			groups.push_back(g);
		}
	}
}

// detect and store valid connected slice groups in img for direction using slices ofr sliceType
// img should be a CV_8U {0, 1} and 1s will be turned into 2s after scanning
void Extractor::detectAllGroups(uchar direction) {
	int w = img.cols, h = img.rows, x, y;
	uchar *row;
	uchar px;
	char chDx = 0, chDy = 0; // forward displacements for this direction
	uchar sliceType;

	// prepare forward displacements
	chDx = 0, chDy = 0;
	switch (direction) {
		case DIR_VERTICAL:	chDy = 1; sliceType = SLICE_HORIZONTAL; break;
		default:			chDx = 1; sliceType = SLICE_VERTICAL;   break;
	}

	// look for non-scanned filled pixels
	for (y = 0 ; y < h ; y++) {
		row = img.ptr(y);
		for (x = 0 ; x < w ; x++) {
			px = row[x];
			if (px == 1) {
				// scan this position
				getGroupAt(x, y, sliceType, chDx, chDy);
				//disp::showColormap(img, "getGroupAt");
				//waitKey(0);
			}
		}
	}


	//Mat imgGroups = Mat(img.size(), CV_16U, Scalar(0));
	//for (SliceGroup &sg : groups) sg.paintSlices<ushort>(imgGroups, false);

	for (SliceGroup &sg : groups) {
		//sg.sliceStabilize(imgExtrude);
		sg.removeMisplacedSlices();
	}

	//Mat imgGroups2 = Mat(img.size(), CV_16U, Scalar(0));
	//for (SliceGroup &sg : groups) sg.paintSlices<ushort>(imgGroups2, false);

	//disp::showOverlapped<ushort>(imgGroups, imgGroups2, 0, "change", "");
}


// Given the groups of slices, try to stabilize their slice length
// For every group, we try to fix slices of unexpected length based on imgExtrude
void Extractor::stabilizeAllGroups() {
	for (SliceGroup &sg : groups) {
		sg.sliceStabilize(imgExtrude);
	}
}


// extrudes all slice groups on extrusion space
void Extractor::extrudeAllGroups(Mat &imgExt) {
	disp::showColormap(imgExt, "Extrusion space");

	// paint slice groups so we don't overlap
	Mat imgGroups = Mat(img.size(), CV_16U, Scalar(0));
	for (SliceGroup &sg : groups) sg.paintSlices<ushort>(imgGroups, false, true);

	// extrude forward all groups sufficiently long
	for (SliceGroup &sg : groups) {
		if (!sg.absorbed && sg.slices.size() >= T_MIN_SLICES_EXTRUSION) {
			sg.extrudeSlices(imgExt, imgGroups, groups, MOVING_FORWARD);
		}
	}

	// extrude all non-absorbed groups backwards
	for (SliceGroup &sg : groups) {
		if (!sg.absorbed && sg.slices.size() >= T_MIN_SLICES_EXTRUSION) {
			sg.extrudeSlices(imgExt, imgGroups, groups, MOVING_BACKWARD);
		}
	}
}

// streamlined extraction operation
void Extractor::runFullExtraction(uchar lineDirection, Mat *imgExtrusion) {
	detectAllGroups(lineDirection);
	stabilizeAllGroups();
	extrudeAllGroups(*imgExtrusion);
}
