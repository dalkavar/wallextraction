#ifndef PROJECT_PARTICLE_H
#define PROJECT_PARTICLE_H

#include "opencv2/core.hpp"
#include "geometry.h"
#include "../util/Vector2D.h"
using namespace cv;

class Particle {
public:
	ushort eid;
	unsigned long steps = 0; // particle lifetime
	ushort stepsCollision = 0; // steps since last collision; the engine resets this
	Vector2D X, V, A; // position, velocity, acceleration
	float m = 1.0f; // mass
	
	Particle(ushort eid, Point2f &pos);
	void applyImpulse(float magnitude, Point2f &direction);
	void step(float friction);
	void stepBack();
	bool isInBounds(float x0, float y0, float x1, float y1);

	void print();
};


#endif //PROJECT_PARTICLE_H
