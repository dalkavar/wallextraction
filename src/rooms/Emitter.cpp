
#include "Emitter.h"

/**
 * Initializes Emitter with the segment
 * @param imgWalls pointer to binary {0, 1} walls image
 * @param seg
 */
Emitter::Emitter(ushort id, Segment &seg, Point2f &normal, Mat &imgWalls){
	this->id = id;
	this->seg = seg;
	this->normal = normal;
	tangent = Vector2D(geo::directionBetweenPoints(seg.p0, seg.p1)).normalize();
	
	// If normal vector is not defined, use a random one of the 2 line segment normals
	if (normal.x == 0.f && normal.y == 0.f) {
		reliableNormal = false;
		emitParticles = false;
		this->normal = this->seg.getNormal(true);
	}
	

	findSources(imgWalls); // Store sources
	
	// emitPeriod is calculated based on the segment length
	float ep = 100.f - 0.5f * seg.length;
	emitPeriod = ushort((ep > 5.f) ? ep : 5.f);
	
	// Initialize random number engine
	randEng.seed();
	distribution = uniform_int_distribution<int>(0, (int)(sources.size() - 1));
}

/**
 * Finds and stores the nearest empty pixels to the emitter, in the normal direction
 */
void Emitter::findSources(Mat &imgWalls) {
	Point source;
	Vector2D pDisplaced;
	Vector2D p = Vector2D(seg.p0);
	Vector2D displ = tangent * 0.5f;
	int steps = 0, maxSteps = (int)round(2 * seg.length); // We advance 2x steps each time
	float radius = 0.5f * seg.thickness;

	/*printf("Find sources for:\n"); seg.print();
	printf("Tangent: (%.2f, %.2f)\n", tangent.x, tangent.y);
	printf("Normal: (%.2f, %.2f)\n", normal.x, normal.y);
	printf("displ: (%.2f, %.2f)\n", displ.x, displ.y);*/

	while (steps < maxSteps) {
		// get a source candidate
		pDisplaced = p + normal * (radius + 0.6f);
		source = pDisplaced.integerCoords();
		
		// if we didn't hit an empty pixel yet, try the next one
		if (imgWalls.at<uchar>(source) != 0) {
			pDisplaced += normal;
			source = pDisplaced.integerCoords();
		}
		
		// If we got an empty pixel, use this one, otherwise skip
		if (imgWalls.at<uchar>(source) == 0) {
			// store source candidate if not repeated
			if (sources.empty() || sources.back() != source) {
				sources.push_back(source);
			}
		}
		
		p += displ;
		steps++;
	}
}


Particle Emitter::emit() {
	// Find a random source
	Point &source = sources.at((unsigned long)distribution(randEng));
	Point2f pos = Point2f(source.x + 0.5f, source.y + 0.5f);
	steps = 0;
	
	// Generate a particle and apply an impulse to it
	Particle pa = Particle(id, pos);
	pa.V = normal;
	return pa;
}


/**
 * Paints the Emitter's pixels with value of type T using the slices methodology
 * @param value can be uchar or Vec2f
 * @param img can be CV_8U or CV_32F
 */
template<class T> void Emitter::paint(Mat &img, T value) {
	Point2f pc, begin;
	int thick = (int)seg.sliceThickness;
	float r =  float(thick) / 2.f;
	int x0, y0, x1, y1, numSlices;
	
	// vertical slices
	if (seg.horizontalAspect) {
		numSlices = (int)fabs(seg.p1.x - seg.p0.x);
		pc.x = floor(seg.p0.x) + 0.5f;
		pc.y = seg.getYforX(pc.x);
		
		while (numSlices > 0) {
			begin.x = pc.x;
			begin.y = pc.y - r;
			
			x0 = (int)floor(begin.x);
			y0 = (int)floor(begin.y);
			if (begin.y - y0 > 0.5) y0++;
			y1 = y0 + thick - 1;
			
			while (y0 <= y1) {
				img.at<T>(y0, x0) = value;
				y0++;
			}
			
			pc.x += 1.f;
			pc.y = seg.getYforX(pc.x);
			numSlices--;
		}
	}
	// horiz slices
	else {
		float dy = (seg.p0.y < seg.p1.y) ? 1.f : -1.f;
		numSlices = (int) fabs(seg.p1.y - seg.p0.y);
		pc.y = floor(seg.p0.y) + 0.5f;
		pc.x = seg.getXforY(pc.y);
		
		while (numSlices > 0) {
			begin.y = pc.y;
			begin.x = pc.x - r;
			
			y0 = (int) floor(begin.y);
			x0 = (int) floor(begin.x);
			if (begin.x - x0 > 0.5) x0++;
			x1 = x0 + thick - 1;
			
			
			while (x0 <= x1) {
				img.at<T>(y0, x0) = value;
				x0++;
			}
			
			pc.y += dy;
			pc.x = seg.getXforY(pc.y);
			numSlices--;
		}
	}
}


/**
 * Paints the emitter's internal line, source pixels, and an arrow for the normal, for visualization purposes
 * @param img
 * @param color
 */
void Emitter::paintDebug(Mat &img, uchar color) {
	cv::line(img, seg.p0, seg.p1, Scalar(color), 1);
	Point2f center = seg.getCenter();
	cv::arrowedLine(img, center, center + Point2f(normal) * 15, Scalar(color + 1), 1, 8, 0, 0.3);
	
	for (Point &p : sources) {
		img.at<uchar>(p) = uchar(color + 2);
	}
}



void Emitter::step() {
	steps++;
}

// template declarations
template void Emitter::paint(Mat &img, uchar value);
template void Emitter::paint(Mat &img, Vec2f value);