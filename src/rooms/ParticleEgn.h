#ifndef PROJECT_PARTICLEEGN_H
#define PROJECT_PARTICLEEGN_H

#include <fstream>
#include "opencv2/highgui.hpp"
#include "Emitter.h"
#include "Particle.h"
#include "../util/debug.h"

using namespace cv;
using std::vector;
using std::string;
class Emitter;
class Particle;

// Define a scoped ennumeration for world pixel codes
namespace WPx { enum Pixel {
	EMPTY = 0, // Nothing here
	WALL = 1, // Wall line with a correct normal
	LINE = 2 // line where the normal might point outside or inside
}; }

class ParticleEgn {
	Emitter parseEmitter(ushort eid, string text);
	uchar handleCollisions(Particle &pa, Point &pos);
	bool detectCollision(Particle &pa, Point &pos);
	void stepParticle(Particle &pa);
	void registerDirection(Point &p, Vector2D dir);
	void initVisualization();
	
public:
	Mat imgInit; // CV_8U {0, 1} Initial wall image for visualization purposes

	// Physics world
	Mat imgWorld; // CV_8U Semantic description of the physics world, different from imgInit
	Mat imgNormals; // CV_32FC2 Wall normal directions
	float friction = 0.0001f;
	uint steps = 0; // step count
	uint maxSteps = 100000; // simulation duration
	
	// Behavior analysis
	Mat imgFlux; // CV_32FC2  unit vector of flux average orientation
	
	Mat imgBgColor; // CV_8UC3 RGB base for imgColor
	Mat imgColor; // CV_8UC3 RGB visualization mat
	Mat imgHueMap; // CV_8UC3 HSV flux visualization mat
	Mat imgAmplitude;
	
	vector<Emitter> emitters;
	list<Particle> particles;
	
	std::mt19937 randEng; // Mersenne Twister random engine
	std::uniform_real_distribution<float> distribution; // Uniform distribution
	
	ParticleEgn() = default;
	ParticleEgn(string imgPath, string segPath);
	void paintEmitters(Mat &img);
	

	void run();
	void visualize();
};


#endif //PROJECT_PARTICLEEGN_H
