
#include "wallprojector.h"


WallProjector::
WallProjector(Mat *imgWalls) {
	this->imgWalls = imgWalls;
	imgSpace = Mat(imgWalls->rows, imgWalls->cols, CV_16U, Scalar(0));
	imgDraft = Mat(imgWalls->rows, imgWalls->cols, CV_8U, Scalar(0));
}

// Projects all segments
void WallProjector::projectAllGroups(list<SliceGroup> &groups) {
	for (SliceGroup &g : groups) {
		if (g.parallel) {
			projectGroup(g);
		}
	}
}

// Projects a segment in imgSpace until a non-empty pixel in imgWalls is found
void WallProjector::projectGroup(SliceGroup &g) {
	Segment &seg = g.seg;
	Point2f p = Point2f(seg.p0.x, seg.p0.y);

	// Guard against invalid segments
	int w = imgWalls->cols, h = imgWalls->rows;
	if (p.x <= 0 || p.y <= 0 || p.x >= (w - 1) || p.y >= (h - 1)) return;

	// Get forward displacements from p0 to p1
	double dx = 0.f, dy = 0.f, xf = seg.p1.x, yf = seg.p1.y;
	bool lineIsVertical = false;

	if (p.x == xf) {
		dy = 1.f;
		lineIsVertical = true;
	}
	else if (p.y == yf) {
		dx = 1.f;
	}
	else {
		if (abs(seg.slope) <= 1.0) {
			dx = 0.5f;
			dy = 0.5 * seg.slope;
		}
		else {
			if (seg.p0.y < seg.p1.y) {
				dy = 0.5f;
				dx = 0.5f / seg.slope;
			} else {
				dy = -0.5f;
				dx = -0.5f / seg.slope;
			}
		}
	}

	seg.print();

	// project all pixels
	imgDraft = Scalar(0);
	while (true) {
		// Stop conditions
		if (lineIsVertical && p.y > yf)  break;
		if (!lineIsVertical && p.x > xf) break;
		printf("projecting (%.1f,%.1f)\n", p.x, p.y);

		projectPixel(p, g.normal);

		p.x += dx;
		p.y += dy;
	}
}

// Projects pixels from p in direction u until a non-empty pixel in imgWalls is found
void WallProjector::projectPixel(Point2f p, Point2f u) {
	int w = imgWalls->cols, h = imgWalls->rows;

	// advance until we find the first non-empty pixel
	while (imgWalls->at<uchar>(p) == 1) {
		p.x += u.x;
		p.y += u.y;
	}

	// project until a non-empty pixel is found
	while (imgWalls->at<uchar>(p) == 0) {
		// guard against image borders
		if (p.x <= 0 || p.y <= 0 || p.x >= (w - 1) || p.y >= (h - 1)) break;

		if (imgDraft.at<uchar>(p) == 0) {
			imgDraft.at<uchar>(p) = 1;
			imgSpace.at<ushort>(p) += 1;
		}

		p.x += u.x;
		p.y += u.y;
	}

}
