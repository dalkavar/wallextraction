#ifndef WALLPROJECTOR_H
#define WALLPROJECTOR_H

#include <mutex>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include <util/display.h>
#include <util/px.h>
#include <util/segment.h>
#include <extractor/transform/slicegroup.h>

using namespace cv;
using std::vector;

class WallProjector {

public:
	Mat *imgWalls;
	Mat imgSpace;
	Mat imgDraft;

	WallProjector(Mat *imgWalls);
	void projectAllGroups(list<SliceGroup> &groups);
	void projectGroup(SliceGroup &g);
	void projectPixel(Point2f p, Point2f u);
};

#endif // WALLPROJECTOR_H
