#include "WallPainter.h"

WallPainter::WallPainter(Mat *imgInit) {
	imgLines = Mat(imgInit->rows, imgInit->cols, CV_8U, Scalar(0));
}

// Draws slicegroups into imgLines with color
void WallPainter::drawLineSegments(list<SliceGroup> &groups, uchar color) {
	printf("Drawing line segments...\n");
	Point2f p0, p1;
	for (SliceGroup &g : groups) {
		if (g.isLabeledWall()) {
			g.getPresentationEndpoint('0', p0);
			g.getPresentationEndpoint('1', p1);
			//line(imgLines, p0, p1, Scalar(color));
			drawGroupSlices<uchar>(imgLines, g, color, false);
		}
	}
}

template <typename T>
void WallPainter::drawGroupSlices(Mat &img, SliceGroup &g, T color, bool innerEndpoints) {
	Segment &seg = g.seg;
	Point2f p0, p1;

	// get the endpoints
	if (innerEndpoints) {
		g.getPresentationEndpoint('0', p0);
		g.getPresentationEndpoint('1', p1);
	} else {
		p0 = seg.p0;
		p1 = seg.p1;
	}

	drawLine(img, p0, p1, g.seg.thickness, uchar(color));
}



void WallPainter::projectWalls(list<SliceGroup> &groups, uchar color) {
	Point2f dir;

	/*auto it = groups.begin();
	for (int i = 0 ; i < 10 ; i++) it++;
	SliceGroup &g = *it;*/

	for (SliceGroup &g : groups) {
		if (g.isLabeledWall()) {
			dir = -g.normal;
			//printf("is wall, project with dir (%.2f, %.2f)\n", dir.x, dir.y);
			if (!geo::isZero(dir)) projectGroup(imgLines, g, dir, g.minParallelDist - 1, color);
			//return;
		}
	}
}

void WallPainter::projectGroup(Mat &img, SliceGroup &g, Point2f &direction, float maxLength, uchar color) {
	Segment &seg = g.seg;
	Point2f p0, p1;
	//p0 = seg.p0, p1 = seg.p1;
	g.getPresentationEndpoints(p0, p1);

	Point2f p = Point2f(p0.x, p0.y);

	// Guard against invalid segments
	int w = imgLines.cols, h = imgLines.rows;
	if (p.x <= 0 || p.y <= 0 || p.x >= (w - 1) || p.y >= (h - 1)) return;

	// Get forward displacements from p0 to p1
	double dx = 0.f, dy = 0.f;

	// Horizontal and vertical cases need integer coordinates
	if (seg.isPerfectHorizontal()) {
		p.x = floor(p.x) + 1;
		p.y = floor(p.y);
		dx = 1.f;
	}
	else if (seg.isPerfectVertical()) {
		p.x = floor(p.x);
		p.y = floor(p.y) + 1;
		dy = 1.f;
	}
	else {
		if (abs(seg.slope) <= 1.0) {
			dx = 0.5f;
			dy = 0.5 * seg.slope;
		}
		else {
			if (p0.y < p1.y) {
				dy = 0.5f;
				dx = 0.5f / seg.slope;
			} else {
				dy = -0.5f;
				dx = -0.5f / seg.slope;
			}
		}
	}

	// find a suitable MBR
	float xMin = min<float>(p0.x, p1.x);
	float yMin = min<float>(p0.y, p1.y);
	float xMax = max<float>(p0.x, p1.x);
	float yMax = max<float>(p0.y, p1.y);
	if (xMin == xMax) {
		xMin = floor(p.x);
		xMax = ceil(p.x);
	}
	if (yMin == yMax) {
		yMin = floor(p.y);
		yMax = ceil(p.y);
	}

	// Guard against segments touching image borders
	if (xMin < 1) xMin = 1;
	if (yMin < 1) yMin = 1;
	if (xMax > (w - 1)) xMax = (w - 1);
	if (yMax > (h - 1)) yMax = (h - 1);

	//seg.print();
	//printf("MBR: (%.2f, %.2f)-(%.2f, %.2f)\n", xMin, yMin, xMax, yMax);
	//printf("starting at (%.2f, %.2f)\n", p.x, p.y);

	// project all pixels
	while (p.x >= xMin && p.x <= xMax && p.y >= yMin && p.y <= yMax) {
		//printf("\nprojecting (%.2f, %.2f)\n", p.x, p.y);
		projectPixel(img, p, direction, seg.sliceThickness, maxLength, color);
		p.x += dx;
		p.y += dy;

	}
	img.at<uchar>(p0) = 4;
	img.at<uchar>(p1) = 5;
}

void WallPainter::projectPixel(Mat &img, Point2f origin, Point2f &direction, float lineThickness, float maxLength, uchar color) {
	Point2f &p = origin;
	int w = img.cols, h = img.rows;
	float steps = 1;

	// first mandatory step
	p.x += direction.x;
	p.y += direction.y;

	// advance until we find the first non-empty pixel
	while (steps <= lineThickness && img.at<uchar>(p) == 1) {
		p.x += direction.x;
		p.y += direction.y;
		steps++;
		//printf("pre-advancing (%.2f, %.2f)\n", p.x, p.y);

		if (p.x <= 0 || p.y <= 0 || p.x >= (w - 1) || p.y >= (h - 1)) return;
	}

	// project until a non-empty pixel is found
	while (steps <= maxLength && img.at<uchar>(p) != 1) {
		//printf("painting (%.2f, %.2f)\n", p.x, p.y);
		img.at<uchar>(p) = color;
		// guard against image borders
		if (p.x <= 0 || p.y <= 0 || p.x >= (w - 1) || p.y >= (h - 1)) break;

		p.x += direction.x;
		p.y += direction.y;
		steps += 1.f;
	}
}

// returns in img a binary {0, 1} mask where 1 is a wall pixel body
void WallPainter::getWallsMask(Mat &img) {

	img = Mat(imgLines.rows, imgLines.cols, CV_8U, Scalar(0));
	std::mutex mtx;
	imgLines.forEach<uchar>([&img, &mtx](uchar &val, const int pos[]) -> void {
		if (val != 0) {
			lock_guard<mutex> guard(mtx);
			img.at<uchar>(pos) = 1;
		}
	});
}

// template declarations
template void WallPainter::drawGroupSlices(Mat &img, SliceGroup &group, uchar color, bool innerEndpoints);