#include "WallSegmentManager.h"

// Reloads all wallsegments and virtuals
void WallSegmentManager::loadSegments(string outPath) {
	this->outPath = outPath;
	WallSegment ws;
	vector<string> tokens;

	// Load wall segments
	io::CSVReader<10> in(outPath + "segments.txt");
	while(in.read_row(ws.id, ws.p0.x, ws.p0.y, ws.p1.x, ws.p1.y, ws.thickness, ws.sliceThickness, ws.normal.x, ws.normal.y, ws.minParallelDist)) {
		ws.update(false);
		wallSegments.push_back(ws);
		ws = WallSegment();
	}
}

void WallSegmentManager::writeRoomsToFile(vector<vector<Point> > &contours) {
	ofstream fileWalls, fileRooms;
	int nWall = 1, nRoom = 1;
	vector<Point> contour;
	Point p0, p1;

	try {
		fileWalls.open(outPath + "rooms-walls.txt");
		fileRooms.open(outPath + "rooms.txt");

		bool first = true;
		for (size_t idx = 0; idx < contours.size(); idx++) {
			contour = contours.at(idx);

			if (contour.size() < 4) continue; // ignore contours with less than 4 points

			// write contour's walls
			fileRooms << nRoom;

			// add all contour walls to walls file
			for (unsigned long i = 0 ; i < contour.size() - 1 ; i++) {
				p0 = contour.at(i);
				p1 = contour.at(i + 1);
				fileWalls << nWall << "," << p0.x << "," << p0.y << "," << p1.x << "," << p1.y << "\n";
				fileRooms << "," << nWall;
				nWall++;
			}
			fileRooms << "\n";
			nRoom++;
		}

		fileWalls.close();
		fileRooms.close();
	}
	catch (const ifstream::failure& e) {
		cout << "Exception opening/reading file";
	}
}
