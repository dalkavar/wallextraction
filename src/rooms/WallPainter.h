#ifndef PROJECT_WALLPAINTER_H
#define PROJECT_WALLPAINTER_H


#include <mutex>
#include "opencv2/core.hpp"

#include <util/display.h>
#include <util/px.h>
#include <util/segment.h>
#include <walls/geometry/slicegroup.h>

using namespace cv;
using std::vector;
using std::list;

/* Paints wall SliceGroups */
class WallPainter {
public:
	Mat imgLines;

	WallPainter() = default;
	WallPainter(Mat *imgInit);

	void drawLineSegments(list<SliceGroup> &groups, uchar color);
	template <typename T> void drawGroupSlices(Mat &img, SliceGroup &g, T color, bool innerEndpoints);

	void projectWalls(list<SliceGroup> &groups, uchar color);
	void projectGroup(Mat &img, SliceGroup &g, Point2f &direction, float maxLength, uchar color);
	void projectPixel(Mat &img, Point2f origin, Point2f &direction, float lineThickness, float maxLength, uchar color);

	void getWallsMask(Mat &img);
};


#endif //PROJECT_WALLPAINTER_H