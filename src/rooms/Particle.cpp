#include "Particle.h"

Particle::Particle(ushort eid, Point2f &pos) {
	this->eid = eid;
	this->X.set(pos.x, pos.y);
}

/**
 * Impulsive force affects acceleration and vanishes
 * @param magnitude
 * @param direction unit vector
 */
void Particle::applyImpulse(float magnitude, Point2f &direction) {
	//A += (magnitude * direction) / m;
}

/**
 * Advances 1 time unit all properties
 */
void Particle::step(float friction) {
	//printf("\nBefore step:\n"); print();
	// Advance
	V += A;
	X += V;
	
	// Apply max speed
	if (V.magnitude() > 2.f) V.magnitude(2.f);

	// Apply friction
	//V.reduceMagnitude(friction);
	
	steps++;
	stepsCollision++;
	//printf("After step:\n"); print();
}

/**
 * True if position x between x0 and x1 and y between y0 and y1
 * @param x0
 * @param y0
 * @param x1
 * @param y1
 * @return bool
 */
bool Particle::isInBounds(float x0, float y0, float x1, float y1) {
	return !(X.x < x0 || X.x > x1 || X.y < y0 || X.y > y1);
}

/**
 * Moves backwards 1 step with current velocity. No physical variables are updated
 */
void Particle::stepBack() {
	X -= V;
}

void Particle::print() {
	printf("Particle { X(%.2f, %.2f) V(%.2f, %.2f) A(%.2f, %.2f) steps=%lu}\n", X.x, X.y, V.x, V.y, A.x, A.y, steps);
}
