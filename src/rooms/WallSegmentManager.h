#ifndef WALLSEGMENTMANAGER_H
#define WALLSEGMENTMANAGER_H

#include <opencv2/core.hpp>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <util/segment.h>
#include "geometry.h"
#include "common/WallSegment.h"
#include <external/csv.h>


using namespace std;
using namespace cv;

/**
 * Responsible for performing general reusable operations on wall segments
 */
class WallSegmentManager {
public:
	vector<WallSegment> wallSegments;
	vector<WallSegment> virtuals;

	string outPath;

	void loadSegments(string outPath);
	void writeRoomsToFile(vector<vector<Point> > &contours);
};


#endif //WALLSEGMENTMANAGER_H
