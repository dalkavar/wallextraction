
#include "ParticleEgn.h"

ParticleEgn::ParticleEgn(string imgPath, string segPath) {
	// load image and init matrices
	imgInit = imread(imgPath, IMREAD_GRAYSCALE);
	if (imgInit.empty()) {
		printf("ParticleEgn FATAL: couldn't load image in %s\n", imgPath.c_str());
		exit(-1);
	}
	threshold(imgInit, imgInit, 0, 5,  THRESH_OTSU | THRESH_BINARY_INV);
	imgWorld = Mat(imgInit.rows, imgInit.cols, CV_8U, Scalar(WPx::EMPTY));
	imgNormals = Mat(imgInit.rows, imgInit.cols, CV_32FC2, Scalar(0.f, 0.f));
	imgFlux = Mat(imgInit.rows, imgInit.cols, CV_32FC2, Scalar(0.f, 0.f));
	imgColor = Mat(imgInit.rows, imgInit.cols, CV_8UC3, Scalar(0, 0, 0));
	imgBgColor = Mat(imgInit.rows, imgInit.cols, CV_8UC3, Scalar(0, 0, 0));
	imgHueMap = Mat(imgInit.rows, imgInit.cols, CV_8UC3, Scalar(0, 0, 0));
	imgAmplitude = Mat(imgInit.rows, imgInit.cols, CV_32F, Scalar(0.f));
	
	// Init random engine
	distribution = uniform_real_distribution<float>(0.0f, 0.3f);
	
	// load segments and create emitters
	string line;
	ifstream f(segPath);
	Emitter em;
	ushort eid = 1;
	if (f.is_open()) {
		while (getline(f, line)) {
			// create and store Emitter
			em = parseEmitter(eid, line);
			emitters.push_back(em);
			eid++;

			em.seg.print();
			printf("emitter with normal (%.2f, %.2f)\n", em.normal.x, em.normal.y);
			if(em.normal.isZero()) exit(-1);
			// register emitter on imgWorld and imgNormals
			em.paint<uchar>(imgWorld, (em.reliableNormal) ? WPx::WALL : WPx::LINE);
			em.paint<Vec2f>(imgNormals, Vec2f(em.normal.x, em.normal.y));
		}
		f.close();
	}
	else {
		printf("ParticleEgn FATAL: couldn't load wall segment data in %s\n", segPath.c_str());
		exit(-1);
	}
	
	disp::showColormap(imgWorld, "World");
}

/**
 * Builds a Emitter object from parsing the serialized text
 * @param text text line in the format described by SliceGroup2::serialize
 * @return Emitter
 */
Emitter ParticleEgn::parseEmitter(ushort eid, string text) {
	// Parse segment
	Point2f normal;
	Segment seg;
	std::stringstream ss(text);
	std::string item;
	getline(ss, item, ',');		seg.p0.x = stof(item);
	getline(ss, item, ',');		seg.p0.y = stof(item);
	getline(ss, item, ',');		seg.p1.x = stof(item);
	getline(ss, item, ',');		seg.p1.y = stof(item);
	getline(ss, item, ',');		seg.thickness = stof(item);
	getline(ss, item, ',');		seg.sliceThickness = stof(item);
	getline(ss, item, ',');		normal.x =  stof(item);
	getline(ss, item, ',');		normal.y =  stof(item);
	
	// Create and return object
	seg.update(false);
	return Emitter(eid, seg, normal, imgInit);
}

/**
 * Paints all emitters in CV_8U img for visualization purposes
 * @param img
 */
void ParticleEgn::paintEmitters(Mat &img) {
	for (Emitter &em : emitters) em.paintDebug(img, 2);
}

/**
 * Steps particle and handle collisions and other raections
 * @param pa
 */
void ParticleEgn::stepParticle(Particle &pa) {
	pa.step(friction);
}


/**
 * Run simulation
 */
void ParticleEgn::run() {
	float maxX = imgInit.cols - 1, maxY = imgInit.rows - 1;
	Point pos;
	list<Particle>::iterator it;
	initVisualization();
	//waitKey(0);
	
	// Simulation step loop
	for (steps = 0; steps < maxSteps; steps++) {
		debug::log(1, "\nStep %d\n", steps);
		
		// step all emitters
		//Emitter &em = emitters.at(13);
		for (Emitter &em : emitters) {
			if (em.emitParticles) {
				em.step();
				if (em.steps == em.emitPeriod) {
					particles.push_back(em.emit());
				}
			}
		}

			
		// Update and draw all particles
		for (it = particles.begin(); it != particles.end(); ++it) {
			it->step(friction);
			
			if ((it->V.x == 0.f && it->V.y == 0.f)) {
				it->print();
				printf("FATAL: Weird Particle destroyed the universe OMG.\n");
				exit(-1);
			}

			// Destroy particle if out of image
			if (!it->isInBounds(0.f, 0.f, maxX, maxY)) {
				//printf("particle died at (%.2f, %.2f)\n", it->X.x, it->X.y);
				particles.erase(it);
				it--;
			}
			else {
				pos = it->X.integerCoords();
				if (detectCollision(*it, pos)) {
					// destroy particles that crash after 1 step or that get stuck
					if (it->steps < 2 || (it->stepsCollision < 2)) {
						particles.erase(it);
						it--;
					}
					else {
						handleCollisions(*it, pos);
					}
				}
				
				registerDirection(pos, it->V.direction()); // register flux
			}
		}
		
		visualize();
	}
	
}

/**
 * Detects and reacts to collisions in particle
 * @param pa Particle. Must be in the image.
 * @param pos current position in 2D pixel space
 */
uchar ParticleEgn::handleCollisions(Particle &pa, Point &pos) {
	uchar code = imgWorld.at<uchar>(pos);
	
	// reflect particle
	Vec2f normals = imgNormals.at<Vec2f>(pos);
	Vector2D n(normals[0], normals[1]);
	//printf("\nColission at (%d, %d) type %d with normal (%.2f, %.2f)\n", pos.x, pos.y, code, n.x, n.y);
	//pa.print();

	// if this normal is unreliable and makes the particle crash, invert it
	Point2f next = (Vector2D(pos.x + 0.5f, pos.y + 0.5f) + n);
	if (imgWorld.at<uchar>((int)next.y, (int)next.x) != WPx::EMPTY) {
		n.invert();
		next = (Vector2D(pos.x + 0.5f, pos.y + 0.5f) + n);
	}
	
	pa.stepBack();
	
	// Get directions' dot product
	Vector2D dir = pa.V.direction();
	float mag = pa.V.magnitude();
	float dot = dir.dotProduct(n);

	//printf("Dot product: %.4f\n", dot);

	// orthogonal vectors: particle V close and tangential to wall -> get away
	if (fabs(dot) < 0.0001f) {
		dir += 0.5f * n;
		dir.normalize();
		pa.V = mag * dir;
	}
	// parallel vectors: perfect reflection
	else if (fabs(fabs(dot) - 1.f) < 0.0001f) {
		pa.V.x += distribution(randEng);
		pa.V.y += distribution(randEng);
		pa.V.invert();
	}
	else {
		// normal collision
		pa.V = dir - n * 2.f * dot;
		
		// add a small random angle variation
		pa.V.x += distribution(randEng);
		pa.V.y += distribution(randEng);
		
		pa.V.normalize();
		pa.V.magnitude(mag);
	}
	
	if ((pa.V.x == 0.f && pa.V.y == 0.f) || !pa.isInBounds(0.f, 0.f, imgWorld.cols - 1, imgWorld.rows - 1)) {
		printf("FATAL: Weird Colission destroyed the universe.\n");
		exit(1);
	}
	pa.stepsCollision = 0;
	return 0;
}

// true if the particle collided with something
bool ParticleEgn::detectCollision(Particle &pa, Point &pos) {
	return (imgWorld.at<uchar>(pos) != WPx::EMPTY);
}

// add V to the flux matrix
void ParticleEgn::registerDirection(Point &p, Vector2D dir) {
	Vec2f& fl = imgFlux.at<Vec2f>(p);
	//Vector2D vfl(fl[0], fl[1]);
	fl[0] += dir.x;
	fl[1] += dir.y;
}

// Show everything
void ParticleEgn::visualize() {
	imgColor = imgBgColor.clone(); // get walls bg
	
	// Draw flux
	imgHueMap = Scalar(0,0,0);
	imgAmplitude = Scalar(0);
	std::mutex mtx;
	imgFlux.forEach<Vec2f>([this, &mtx](Vec2f &dir, const int pos[]) -> void {
		if (dir[0] != 0.f || dir[1] != 0.f) {
			lock_guard<mutex> guard(mtx);
			float x = dir[0], y = dir[1];
			float a = geo::angleFromUnitVector(x, y);
			if (a < 0) a += 180; // orientation, not angle!
			this->imgHueMap.at<Vec3b>(pos) = Vec3b((uchar)a, 255, 255);
			//this->imgAmplitude.at<float>(pos) = sqrt(x*x + y*y);
		}
	});
	
	// Draw particles
	for (Particle &pa : particles) {
		imgColor.at<Vec3b>(pa.X.integerCoords()) = Vec3b(255, 255, 255);
	}
	
	imshow("Visualization", imgColor);
	
	cvtColor(imgHueMap, imgHueMap, cv::COLOR_HSV2BGR);
	imshow("Flux", imgHueMap);
	//disp::showNormalized(imgAmplitude, "Amplitude");
	waitKey(0);
}

// Initialize visualization imgBgColor
void ParticleEgn::initVisualization() {
	// Copy walls
	std::mutex mtx;
	imgWorld.forEach<uchar>([this, &mtx](uchar &code, const int pos[]) -> void {
		if (code == WPx::WALL) {
			lock_guard<mutex> guard(mtx);
			this->imgBgColor.at<Vec3b>(pos) = Vec3b(180, 240, 180);
		}
		else if (code == WPx::LINE) {
			lock_guard<mutex> guard(mtx);
			this->imgBgColor.at<Vec3b>(pos) = Vec3b(180, 180, 240);
		}
	});
}
