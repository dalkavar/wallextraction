#ifndef PROJECT_EMITTER_H
#define PROJECT_EMITTER_H

#include <random>
#include <mutex>
#include "opencv2/core.hpp"
#include "Particle.h"
#include "../util/segment.h"

using namespace cv;
using std::vector;
using std::string;

class Emitter {
public:
	ushort id;
	Segment seg;
	Vector2D normal, tangent; // Unit direction vectors; tangent from p0 to p1
	vector<Point> sources;
	
	bool reliableNormal = true; // True if we are sure the normal points outside the wall
	bool emitParticles = true; // False if emitter doesn't emit particles
	
	ushort emitPeriod = 25; // steps for a new particle emission
	ushort steps = 0; // step number; reset after reaching emitPeriod
	
	std::mt19937 randEng; // Mersenne Twister random engine
	std::uniform_int_distribution<int> distribution; // Uniform distribution
	
	Emitter() = default;
	Emitter(ushort id, Segment &seg, Point2f &normal, Mat &imgWalls);
	void findSources(Mat &imgWalls);
	Particle emit();
	void step();
	template<class T> void paint(Mat &img, T value);
	void paintDebug(Mat &img, uchar color);
};


#endif //PROJECT_EMITTER_H
